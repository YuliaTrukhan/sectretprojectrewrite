﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerAnimationController : MonoBehaviour
{
    private Animator animationController;
    private Rigidbody playerRigidbody;
    private Vector3 prevPosition;
    private float prevRotation;
    private float vSpeed;
    private float hSpeed;
    private float aSpeed;
    private LookDirection lookDirection;
    private bool isShooting;
    private bool isHeavyGun;
    private bool isKeyboardControl;

    public enum LookDirection
    {
        RIGHT,
        LEFT,
        TOP,
        DOWN
    }

    // Use this for initialization
    void Start ()
    {
        playerRigidbody = GetComponent<Rigidbody>();
        animationController = GetComponentInChildren<Animator>();
        prevPosition = transform.position;
        prevRotation = transform.rotation.eulerAngles.y;
        isKeyboardControl = false;
    }
	
	// Update is called once per frame
	void Update ()
    {
        ReadInput();
        MeasureVelocity();
        ReadOrientation();
        UpdateAnimationController();
	}

    private void ReadInput()
    {
        ReadKeys();
        ReadMouse();
    }

    private void ReadMouse()
    {
        if (Input.GetMouseButtonDown(0))
        {
            if(isKeyboardControl)
            {
                isShooting = true;
            }
        }
        if (Input.GetMouseButtonUp(0))
        {
            if(isKeyboardControl)
            {
                isShooting = false;
            }
        }
    }

    private void ReadKeys()
    {
        if(Input.GetKeyDown(KeyCode.Tab))
        {
            isKeyboardControl = !isKeyboardControl;
        }   

        if(Input.GetKeyUp(KeyCode.Alpha1))
        {
            isHeavyGun = false;
        }
        if (Input.GetKeyUp(KeyCode.Alpha2) || Input.GetKeyUp(KeyCode.Alpha3) || Input.GetKeyUp(KeyCode.Alpha4) || Input.GetKeyUp(KeyCode.Alpha5) || Input.GetKeyUp(KeyCode.Alpha6))
        {
            isHeavyGun = true;
        }
    }

    private void MeasureVelocity()
    {
        hSpeed = (transform.position.x - prevPosition.x)/Time.deltaTime;
        vSpeed = (transform.position.z - prevPosition.z) / Time.deltaTime;
        aSpeed = (transform.rotation.eulerAngles.y - prevRotation) / Time.deltaTime;
        prevPosition = transform.position;
        prevRotation = transform.rotation.eulerAngles.y;
    }

    private void ReadOrientation()
    {
        float angle = transform.rotation.eulerAngles.y;

        if (angle >= 0f && angle <= 45f)
        {
            lookDirection = LookDirection.TOP;
        }
        else if (angle > 45f && angle <= 135f)
        {
            lookDirection = LookDirection.RIGHT;
        }
        else if (angle > 135f && angle <= 225f)
        {
            lookDirection = LookDirection.DOWN;
        }
        else if (angle > 225f && angle <= 315f)
        {
            lookDirection = LookDirection.LEFT;
        }
        else if (angle > 315f && angle <= 360f)
        {
            lookDirection = LookDirection.TOP;
        }
    }

    private void UpdateAnimationController()
    {
        switch (lookDirection)
        {
            case LookDirection.TOP:
                animationController.SetFloat("fSpeed", vSpeed);
                animationController.SetFloat("sSpeed", hSpeed);
                break;
            case LookDirection.DOWN:
                animationController.SetFloat("fSpeed", -vSpeed);
                animationController.SetFloat("sSpeed", -hSpeed);
                break;
            case LookDirection.RIGHT:
                animationController.SetFloat("fSpeed", hSpeed);
                animationController.SetFloat("sSpeed", vSpeed);
                break;
            case LookDirection.LEFT:
                animationController.SetFloat("fSpeed", -hSpeed);
                animationController.SetFloat("sSpeed", -vSpeed);
                break;
        }

        animationController.SetFloat("aSpeed", aSpeed);
        animationController.SetBool("Shoot", isShooting);
        animationController.SetBool("KeyboardKontrol", isKeyboardControl);
        animationController.SetBool("isHeavyGun", isHeavyGun);
    }

    private void SwitchGun()
    {
        isHeavyGun = !isHeavyGun;
        animationController.SetBool("isHeavyGun", isHeavyGun);
    }
}
