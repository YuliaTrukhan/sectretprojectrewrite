﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System.Collections.Generic;

public class UIManager : MonoBehaviour
{
    public GameOverUI GameOverCanvas;
    public Text BulletsText;
    public RectTransform ShakeUI;
    private Vector3 OriginalUIPos;
   
    private bool selectInterface = false;
   
    [Header("Cursor")]
    public Texture2D KeyboardСursor;
    public Texture2D AutomaticCursor;
    public GameObject MouseСursor;
    public Animator CursorAnim;

    [Header("PlayerInfo")]
    public HealthBar PlayerHealth;
    public Text StartingHealth;
    public Text CurrentHealth;
    public Text Coins;
    public Vignette PlayerVignette;
    public Vignette DieVignette;
    public HealthBar PlayerArmor;

    [Header ("WeaponInfo")]
    public Text WeaponName;
    public Text CurrentBullets;
    public Text OtherBullets;
    public GameObject ReloadText;
    public Transform WeaponIconSpacer;
    public UIWeaponInfo UIWeaponPrefab;
    public WeaponIconUI WeaponIcon;
    public Image WeaponImage;
    public List<UIWeaponInfo> WeaponUIList = new List<UIWeaponInfo>();

    [Header("EnemyInfo")]
    public HealthBar EnemyHealth;
    public Slider EnemyHealthBar;
    public Text EnemyName;
    public GameObject AlarmText;

    [Header("Grenade")]
    public GameObject GrenadeUI;
    public GameObject GrenadeCursor;

    [Header("HoverInfo")]
    public GameObject HoverInformation;
    public Text HoverInformationText;

    private CameraControl cameraControl;
    public static UIManager UIManagerInstance;
    
    void Awake()
    {
        if (UIManagerInstance == null)
        {
            UIManagerInstance = this;
        }
        else if (UIManagerInstance != this)
        {
            Destroy(gameObject);
        }
        DontDestroyOnLoad(gameObject);
    }

    private void Start()
    {
        DeactivateEnemyHealthBar();
        SetRealodProgress(0f, 0f);
        SwitchСursor();
        OriginalUIPos = ShakeUI.localPosition;
        GrenadeUI.SetActive(false);
        GrenadeCursor.SetActive(false);
        DeactivateHoverInformation();
        SetArmorSlider(1f, 0f);
    }

    public void SetShakeUI(float _amount)
    {
        ShakeUI.localPosition = OriginalUIPos - Random.insideUnitSphere * _amount *10f;
    }
    public void SetShakeOriginalPos()
    {
        ShakeUI.localPosition = OriginalUIPos;
    }

    public void SetArmorSlider(float _startingAmount, float _currentAmount)
    {
        PlayerArmor.SetValue(_currentAmount, _startingAmount);
    }

    public void SetRealodProgress(float _startingAmount, float _currentAmount)
    {
        WeaponIcon.SetWeaponProgress(_startingAmount, _currentAmount);
    }

    public void SetGrenadeRadius(Vector3 _position)
    {
        _position.y += 0.03f;
        GrenadeCursor.transform.position = _position;
    }

    public void SetCursorAnimation(Vector3 _position)
    {
        _position.y += 0.03f;
        CursorAnim.gameObject.transform.position = _position;
        CursorAnim.gameObject.SetActive(true);
        CursorAnim.SetTrigger("animate");
    }

    public void SetWeaponImage(Sprite _weaponIcon)
    {
        WeaponImage.sprite = _weaponIcon;
    }

    public void SwitchСursor()
    {
        if(InputManager.InputManagerInstance.PlayerControlType == InputManager.TypeControl.KeyboardControl)
        {
            Cursor.SetCursor(KeyboardСursor, Vector2.zero, CursorMode.Auto);
            CursorAnim.gameObject.SetActive(false);
            MouseСursor.SetActive(false);
        }
        else
        {
            Cursor.SetCursor(AutomaticCursor, Vector2.zero, CursorMode.Auto);
        }
    }

    private CameraControl GetCameraControl()
    {
        if (cameraControl == null)
            cameraControl = FindObjectOfType<CameraControl>();
        return cameraControl;
    }

    public void SetCameraDamageShake()
    {
        GetCameraControl().SetShakeDamageCamera();
    }

    public void SetCameraShootShake()
    {
        GetCameraControl().SetShakeShootCamera();
    }

    public void DeactivateEnemyHealthBar()
    {
        EnemyName.text = "";
        EnemyHealth.ResetParametres();
        EnemyHealth.gameObject.SetActive(false);
    }

    public void SetStartEnemyHealthValues(string _enemyName, float _startingHealth, float _currentHealth)
    {
        DeactivateEnemyHealthBar();
        EnemyName.text = _enemyName;
        EnemyHealth.SetStartValues(_currentHealth, _startingHealth);
        EnemyHealth.gameObject.SetActive(true);
    }

    public void SetDamagedEnemyHealthBar(string _enemyName, float _startingHealth, float _currentHealth)
    {
        EnemyHealth.gameObject.SetActive(true);
        EnemyName.text = _enemyName;
        EnemyHealth.SetValue(_currentHealth, _startingHealth);
    }


    public void SetCoins(int _coins)
    {
        Coins.text = _coins.ToString();
    }
    

    public void SetAlarmText(bool _isActive)
    {
        AlarmText.SetActive(_isActive);
    }

    public void SetWeaponName(string _name)
    {
        WeaponName.text = _name.ToUpper();
    }

    //установка экрана конца игры
    public void SetGameOverCanvas(bool _isActive)
    {
        SelectInterface = _isActive;
        GameOverCanvas.SetGameOverPanel();
       // GameOverCanvas.SetActive(_isActive);
    }

    public void SetPlayerDamagedVignette()
    {
        PlayerVignette.SetDamage();
    }

    //полоска жизни игрока
    public void SetPlayerHealthBar(float _currentHealth, float _startingHealth)
    {
        PlayerHealth.SetValue(_currentHealth, _startingHealth);
        CurrentHealth.text = _currentHealth.ToString();
        StartingHealth.text = "/" + _startingHealth;
    }


    //установка текста патронов
    public void SetBulletText(int _currentOboimaAmount, int _startingOboimaAmount, int _currentBullets)
    {
        CurrentBullets.text = _currentOboimaAmount.ToString();
        OtherBullets.text = "/"+_currentBullets;
    }

    //public void SetWeaponTextActive()
    //{
    //    BulletsText.text = "";
    //    WeaponName.text = "";
    //}

    //установка текста перезарядки
    public void SetReloadTextActve(bool _isActive)
    {
        ReloadText.SetActive(_isActive);
    }

    //перезапуск текущего уровня
    public void ReloadCurrentLevel()
    {
        SceneManager.LoadScene("MainScene");
        SetGameOverCanvas(false);
        GameManager.GameInstance.IsGameOver = false;
    }

    public void PauseGame()
    {
        GameManager.GameInstance.Pause = !GameManager.GameInstance.Pause;
        if (GameManager.GameInstance.Pause)
        {
            Time.timeScale = 0;
        }
        else
        {
            Time.timeScale = 1;
        } 
    }


    public void SetWeaponUIIcon(int _weaponUnicID, string _buttonName)
    {
        UIWeaponInfo temp = GetWeaponUI(_buttonName);
        if (temp != null)
        {
            temp.SetParametres(_weaponUnicID, _buttonName);
            temp.transform.SetParent(WeaponIconSpacer, false);
            temp.gameObject.SetActive(true);
        }
        
    }

    public void RemoveWeaponUIIcon(int _weaponUnicID)
    {
        UIWeaponInfo temp = GetWeaponUI(_weaponUnicID);
        temp.WeaponUnicID = -1;
        temp.gameObject.SetActive(false);
    }
    
    public UIWeaponInfo GetWeaponUI(string _buttonName)
    {
        for (int i = 0; i < WeaponUIList.Count; i++)
        {
            if (WeaponUIList[i].ButtonName == _buttonName)
                return WeaponUIList[i];
        }
        return null;
    }

    public UIWeaponInfo GetWeaponUI(int _unicID)
    {
        for(int i=0; i<WeaponUIList.Count; i++)
        {
            if (WeaponUIList[i].WeaponUnicID == _unicID)
                return WeaponUIList[i];
        }
        return null;
    }

    public void SetHoverInformation(string _info)
    {
        HoverInformation.SetActive(true);
        HoverInformationText.text = _info.ToUpper();
    }

    public void DeactivateHoverInformation()
    {
        HoverInformation.SetActive(false);
    }

    public bool SelectInterface
    {
        get
        {
            return selectInterface;
        }
        set
        {
            selectInterface = value;
            if (selectInterface)
            {
                Cursor.SetCursor(AutomaticCursor, Vector2.zero, CursorMode.Auto);
            }
            else
            {
                SwitchСursor();
            }
        }
    }
}
