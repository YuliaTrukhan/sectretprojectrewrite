﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InputManager : MonoBehaviour {

    public static InputManager InputManagerInstance;

    //тип управления: клавиатура или мышь
    public enum TypeControl { KeyboardControl, MouseControl};

    public bool SelectUI { get; set; }

    public TypeControl PlayerControlType;
    private Player player;
    private RaycastHit hitToPlayerMove;
    public bool InteructNpcClick = false;  //проверка нажатия мыши на объекте (для автоматического управления)
    public bool InteructPortalClick = false;


    private Portal portalObject;



    public Portal PortalObject
    {
        get
        {
            return portalObject;
        }
        set
        {
            portalObject = value;
        }
    }

    private void Awake()
    {
        if (InputManagerInstance == null)
        {
            InputManagerInstance = this;
        }
        else if (InputManagerInstance != this)
        {
            Destroy(gameObject);
        }
        DontDestroyOnLoad(gameObject);
        player = FindObjectOfType<Player>();
        SelectUI = false;
    }

    private RaycastHit checkNPC;
    public NPC tempNPC;
    private void Update()
    {
        if (Input.GetKeyUp(KeyCode.F))
        {
            player.TakeDamage(20f);
        }
        if (!GameManager.GameInstance.IsGameOver)
        {
            //взаимодействие с нпс
            if (Input.GetKeyDown(KeyCode.E))
            {
                if(tempNPC!=null)
                    tempNPC.OpenWindow();
            }

            //если не открыты окна или если не пауза
            if (!UIManager.UIManagerInstance.SelectInterface && !GameManager.GameInstance.Pause && !SelectUI)
            {
                //смена контроля
                if (Input.GetKeyUp(KeyCode.Tab))
                {
                    SwitchControl();
                }

                //если управление - клавиатура
                if (PlayerControlType == TypeControl.KeyboardControl)
                {
                    //стрельба по нажатию мыши
                    if (Input.GetMouseButton(0) && !InvertoryController.invertoryController.GrenadeMode)
                        player.Shoot();
                    //управление wasd
                    if (Input.GetKey(KeyCode.W))
                        player.PlayerKeyBoardMove(Vector3.right);
                    if(Input.GetKeyUp(KeyCode.W) || Input.GetKeyUp(KeyCode.S) 
                        || Input.GetKeyUp(KeyCode.A) || Input.GetKeyUp(KeyCode.D))
                        player.ResetWalking();
                    if (Input.GetKey(KeyCode.S))
                        player.PlayerKeyBoardMove(Vector3.left);
                    if (Input.GetKey(KeyCode.A))
                        player.PlayerKeyBoardMove(Vector3.forward);
                    if (Input.GetKey(KeyCode.D))
                        player.PlayerKeyBoardMove(Vector3.back);
                }
                //если управление мышью
                else if (PlayerControlType == TypeControl.MouseControl && Input.GetMouseButton(0) && !InvertoryController.invertoryController.GrenadeMode)
                {
                    PlayerMouseMove();
                }
                

                //принудительная перезарядка
                if (Input.GetKeyDown(KeyCode.R) && InvertoryController.invertoryController.CurrentWeapon.CheckBullets())
                {
                    InvertoryController.invertoryController.CurrentWeapon.ReloadHolder = true;
                }


                //взаимодействие с порталом
                if (Input.GetKeyDown(KeyCode.E) && portalObject != null)
                {
                    portalObject.Teleport();
                }


                //бросок гранаты
                if (InvertoryController.invertoryController.GrenadeMode && Input.GetMouseButtonUp(0) && !UIManager.UIManagerInstance.SelectInterface)
                {
                    if (Physics.Raycast(Camera.main.ScreenPointToRay(Input.mousePosition), out hitToPlayerMove, Mathf.Infinity, 1))
                    {
                        Vector3 temp = hitToPlayerMove.point;
                        InvertoryController.invertoryController.GrenadeShootPosition = hitToPlayerMove.point;
                        temp.y += 0.03f;
                        UIManager.UIManagerInstance.GrenadeUI.transform.position = temp;
                        UIManager.UIManagerInstance.GrenadeUI.SetActive(true);
                        InvertoryController.invertoryController.ShootGrenades(hitToPlayerMove.point);
                    }
                }


                //активировать слот по нажатию 1 2 3 4 5 6 7
                if (Input.GetKeyUp(KeyCode.Alpha1))
                {
                    SlotsController.SlotsInstanse.ActiveWeaponSlot(0);
                }
                if (Input.GetKeyUp(KeyCode.Alpha2))
                {
                    SlotsController.SlotsInstanse.ActiveWeaponSlot(1);
                }
                if (Input.GetKeyUp(KeyCode.Alpha3))
                {
                    SlotsController.SlotsInstanse.ActiveWeaponSlot(2);
                }
                if (Input.GetKeyUp(KeyCode.Alpha4))
                {
                    SlotsController.SlotsInstanse.ActiveWeaponSlot(3);
                }
                if (Input.GetKeyUp(KeyCode.Alpha5))
                {
                    SlotsController.SlotsInstanse.ActiveWeaponSlot(4);
                }
                if (Input.GetKeyUp(KeyCode.Alpha6))
                {
                    SlotsController.SlotsInstanse.ActiveWeaponSlot(5);
                }

                if (Input.GetKeyUp(KeyCode.H))
                {
                    UseHeal();
                }

                if (Input.GetKeyUp(KeyCode.G))
                {
                    UseGrenade();
                }
            }
            //при нажатии на esc закрывает верхнее окно
            if (Input.GetKeyDown(KeyCode.Escape) && CanavasController.canvasController.SortOrder > 0)
            {
                CanavasController.canvasController.CloseHighPanel();
            }
            //окно инвентаря
            if (Input.GetKeyUp(KeyCode.I))
            {
                ShowInventoryWindow();
            }

            //окно квестов
            if (Input.GetKeyDown(KeyCode.Q))
            {
                ShowQuestWindow();
            }
            if (Input.GetKeyDown(KeyCode.M))
            {
                ShowMapWindow();
            }
        }

    }


    public void UseGrenade()
    {
        AudioManager.AudioIntanse.SetUseSpellSound();
        ItemSlot temp = SlotsController.SlotsInstanse.GetSlotType(ItemTypes.ItemType.Grenade);
        if (temp != null)
            temp.UseItem();
    }
    public void UseHeal()
    {
        AudioManager.AudioIntanse.SetUseSpellSound();
        if (!player.HealthFull())
        {
            ItemSlot temp = SlotsController.SlotsInstanse.GetSlotType(ItemTypes.ItemType.Medicine);
            if (temp != null)
            {
                InvertoryController.invertoryController.UseMedicine(temp.ItemID);
            }
        }
    }

    public void ShowQuestWindow()
    {
        QuestUIManager.uIManager.QuestPlayerPanelActive = !QuestUIManager.uIManager.QuestPlayerPanelActive;
        QuestUIManager.uIManager.ShowQuestPlayerPanel();
    }

    public void ShowInventoryWindow()
    {
        InvertoryUIController.invertoryUIController.InvertoryPanelActive = !InvertoryUIController.invertoryUIController.InvertoryPanelActive;
        InvertoryUIController.invertoryUIController.ShowInvertoryWindow(InvertoryUIController.invertoryUIController.InvertoryPanelActive);

    }

    public void ShowMapWindow()
    {
        MapUIController.UIController.IsMapActive = !MapUIController.UIController.IsMapActive;
        MapUIController.UIController.SetMapActive(MapUIController.UIController.IsMapActive);
    }

    //метод для смены контроля
    public void SwitchControl()
    {
        if (PlayerControlType == TypeControl.KeyboardControl)
        {
            PlayerControlType = TypeControl.MouseControl;
            player.ActiveMove(false);
        }
        else if (PlayerControlType == TypeControl.MouseControl)
        {
            PlayerControlType = TypeControl.KeyboardControl;
            player.ActiveMove(false);
        }
        player.ResetWeaponRotation();
        UIManager.UIManagerInstance.SwitchСursor();
    }

    
    //двиаемся по клику мыши
    private void PlayerMouseMove()
    {
        if (Physics.Raycast(Camera.main.ScreenPointToRay(Input.mousePosition), out hitToPlayerMove, Mathf.Infinity, 1))
        {
            UIManager.UIManagerInstance.SetCursorAnimation(hitToPlayerMove.point);
        }
        if (Physics.Raycast(Camera.main.ScreenPointToRay(Input.mousePosition), out hitToPlayerMove))
        {

            if (hitToPlayerMove.collider.tag == "Portal")
            {
                InteructPortalClick = true;
                portalObject = hitToPlayerMove.collider.GetComponent<Portal>();
                if (portalObject != null)
                    portalObject.ChangeSprite(false);
            }
            else
            {
                InteructPortalClick = false;
                if (portalObject != null)
                    portalObject.ChangeSprite(true);
            }
            player.SetDestination(hitToPlayerMove.point);
        }
    }


}
