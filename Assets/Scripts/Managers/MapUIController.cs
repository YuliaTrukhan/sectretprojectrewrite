﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MapUIController : BaseUIManager
{
    public static MapUIController UIController;
    public GameObject Map;
    public bool IsMapActive;

    public override void Awake()
    {
        base.Awake();
        if (UIController == null)
        {
            UIController = this;
        }
        else if (UIController != this)
        {
            Destroy(gameObject);
        }
        DontDestroyOnLoad(gameObject);
    }

    public void Start()
    {
        IsMapActive = false;
        SetMapActive(IsMapActive);
    }


    public override void ClosePanel()
    {
        IsMapActive = false;
        SetMapActive(IsMapActive);
    }

    public void SetMapActive(bool _isActive)
    {
        
        Map.SetActive(_isActive);
        //если открыли
        if (_isActive)
        {
            //обновляем инфу 
            CanavasController.canvasController.SetCanvasSortOrder(CanvasID);
            //WindowIconButton.SetHover();
        }
        else
        {
            //если закрыли, уменьшаем SortOrder у канваса
            SetSortOrder(0);
            CanavasController.canvasController.SortOrder--;
            CanavasController.canvasController.CheckSortOrder();
            //WindowIconButton.SetNormal();
        }
    }
}
