﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class CanavasController : MonoBehaviour {

    public static CanavasController canvasController;
    public List<BaseUIManager> UIManagers = new List<BaseUIManager>(); //содержит все окна
    public int SortOrder;
    public Text UIWindowName;

    private void Awake()
    {
        if (canvasController == null)
        {
            canvasController = this;
        }
        else if (canvasController != this)
        {
            Destroy(gameObject);
        }
        DontDestroyOnLoad(gameObject);
    }
    
    public int GetHigtSortOrder()
    {
        int sortOrder = 0;
        for (int i = 0; i < UIManagers.Count; i++)
        {
            if (UIManagers[i].GetSortOrder() > sortOrder)
            {
                sortOrder = UIManagers[i].GetSortOrder();

            }
        }
        return sortOrder;
    }

    public void CloseHighPanel()
    {
        int tempSortOrder = GetHigtSortOrder();
        for (int i = 0; i < UIManagers.Count; i++)
        {
            //находим верхнее окно
            if (UIManagers[i].GetSortOrder() == tempSortOrder)
            {
                //закрываем его
                UIManagers[i].ClosePanel();
                //проверяем, открыты ли еще окна
                CheckSortOrder();
                return;
            }
        }
    }

    public void SetCanvasSortOrder(int _canvasID)
    {
        int tempSortOrder = GetHigtSortOrder();
        for (int i=0; i<UIManagers.Count; i++)
        {
            if(UIManagers[i].CanvasID == _canvasID)
            {
                SortOrder++;
                tempSortOrder++;
                UIManagers[i].SetSortOrder(tempSortOrder);
                CheckSortOrder();
            }
        }
    }

    public void CheckSortOrder()
    {
        if (SortOrder == 0)
        {
            UIManager.UIManagerInstance.SelectInterface = false;
            UIWindowName.text = "";
        }
        else
        {
            UIManager.UIManagerInstance.SelectInterface = true;
            SetWindowName();
        }
    }

    public void SetWindowName()
    {
        int tempSortOrder = GetHigtSortOrder();
        for (int i = 0; i < UIManagers.Count; i++)
        {
            if (UIManagers[i].GetSortOrder() == tempSortOrder)
            {
                UIWindowName.text = UIManagers[i].WindowName.ToUpper();
            }
        }
    }
}
