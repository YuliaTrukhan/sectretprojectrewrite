﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//базовый класс ui менеджера
//необходим для того, чтобы контролировать окна 
public class BaseUIManager : MonoBehaviour {

    [Header ("Info")]
    public int CanvasID;
    public string WindowName;

    [Header("UI")]
    public ButtonUI WindowIconButton;
    private Canvas canvas;

    public virtual void Awake()
    {
        canvas = GetComponent<Canvas>();
    }

    public virtual void SetSortOrder(int _sortOrder)
    {
        canvas.sortingOrder = _sortOrder;
    }

    public virtual int GetSortOrder()
    {
        return canvas.sortingOrder;
    }

    public virtual void ClosePanel()
    {
       
    }
}
