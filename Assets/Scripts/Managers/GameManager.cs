﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour {

    public static GameManager GameInstance=null;
    public  bool IsGameOver;
    public  bool Pause = false;
    public bool Restart = true;

    private Player player;
    void Awake()
    {
        if (GameInstance == null)
        {
            GameInstance = this;
        }
        else if(GameInstance!=this)
        {
            Destroy(gameObject);
        }
        DontDestroyOnLoad(gameObject);
        player = FindObjectOfType<Player>();
    }

    private void Update()
    {
        if (Restart && !IsGameOver)
        {
            RestartGame();
            UIManager.UIManagerInstance.GameOverCanvas.ResetParametres();
            UIManager.UIManagerInstance.CursorAnim.gameObject.SetActive(false);
            Restart = false;
        }
    }

    public void RestartGame()
    {
        player.ResetParametres();
    }

}
