﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class MenuManager : MonoBehaviour
{

    //перезапуск текущего уровня
    public void ReloadCurrentLevel()
    {
        UIManager.UIManagerInstance.SetGameOverCanvas(false);
        SceneManager.LoadScene("MainScene");
    }
}
