﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioManager : MonoBehaviour {

    public static AudioManager AudioIntanse;
    [Header("Player")]
    public AudioClip[] PlayerDamageShot;
    public AudioClip[] PlayerDeath;
    public AudioClip[] PlayerWalkSound;

    [Header("Enemy")]
    public AudioClip[] EnemyDamageShot;

    [Header("Weapon")]
    public AudioClip[] BulletDownSound;
    public AudioClip[] BulletObstacleSound;
    public AudioClip[] GrenadeExplosionSound;

    public AudioSource GrenadeExplosionObject;

    [Header("UI")]
    public AudioClip ButtonHoverSound;
    public AudioClip ButtonClickSound;
    public AudioClip UseSpellSound;
    private AudioSource uiSoundSource;


    public AudioSource UISoundSource
    {
        get
        {
            if(uiSoundSource==null)
                uiSoundSource = FindObjectOfType<CameraControl>().GetComponent<AudioSource>();
            return uiSoundSource;
        }
    }
    private void Awake()
    {
        if (AudioIntanse == null)
        {
            AudioIntanse = this;
        }
        else if (AudioIntanse != this)
        {
            Destroy(gameObject);
        }
        DontDestroyOnLoad(gameObject);
    }



    public AudioClip GetPlayerDamageShot()
    {
        return PlayerDamageShot[Random.Range(0, PlayerDamageShot.Length)];
    }
    public AudioClip GetEnemyDamageShot()
    {
        return EnemyDamageShot[Random.Range(0, EnemyDamageShot.Length)];
    }

    public AudioClip GetPlayerDeathSound()
    {
        return PlayerDeath[Random.Range(0, PlayerDeath.Length)];
    }

    public AudioClip GetBulletDownSound()
    {
        return BulletDownSound[Random.Range(0, BulletDownSound.Length)];
    }

    public AudioClip GetPlayerWalkSound()
    {
        return PlayerWalkSound[Random.Range(0, PlayerWalkSound.Length)];
    }

    public AudioClip GetBulletObstacleSound()
    {
        return BulletObstacleSound[Random.Range(0, BulletObstacleSound.Length)];
    }

    public void GetGrenadeExplosionSound(Vector3 _explosionPosition)
    {
        GrenadeExplosionObject.gameObject.SetActive(true);
        GrenadeExplosionObject.clip =  GrenadeExplosionSound[Random.Range(0, GrenadeExplosionSound.Length)];
        GrenadeExplosionObject.Play();
        GrenadeExplosionObject.gameObject.transform.position = _explosionPosition;
        
    }
    public void SetUseSpellSound()
    {
        UISoundSource.clip = UseSpellSound;
        UISoundSource.Play();
    }
}
