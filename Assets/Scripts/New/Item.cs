﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Item : MonoBehaviour {

    public Sprite ObjectIcon;

    public int ObjectID; //не повторяющееся ID
    public ItemTypes.ItemType ObjectType;
    public string ObjectName;
    public int ObjectPrice;
    public bool Stackable;
    public float DropChance;
    public int Count;

    public virtual void GetCount()
    {

    }
}
