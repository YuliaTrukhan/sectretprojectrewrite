﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class BulletPool : MonoBehaviour {

    public static BulletPool Current;
    private List<BaseBullet> pooledBullets;
    private Transform ObjectStorage;

    void Awake()
    {
        Current = this;
    }

    void Start()
    {
        pooledBullets = new List<BaseBullet>();
    }

    //метод для запроса объекта из пула
    public BaseBullet GetPooledBullet(BaseBullet _bullet)
    {
        foreach (BaseBullet _thisObject in pooledBullets)
        {
            if (!_thisObject.gameObject.activeSelf && _thisObject.ID == _bullet.ID)
            {
                _thisObject.transform.parent = ObjectStorage;
                return _thisObject;
            }
        }

        //если неактивных объектов нет, добавляем еще один
        BaseBullet _obj = Instantiate(_bullet);
        _obj.transform.parent = ObjectStorage;
        pooledBullets.Add(_obj);
        return _obj;
    }
}
