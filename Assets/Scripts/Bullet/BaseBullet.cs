﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class BaseBullet : MonoBehaviour
{

    protected int damage;
    [SerializeField]
    protected float StartingSpeed;

    protected Vector3 target;
    protected Player player;
    protected float currentSoundTimer;
    protected Enemy enemy;
    protected float distanseToLife;
    public int ID;

    private Bullet bulletFX;

    public enum TypeOfDamage
    {
        Point = 1,
        Area = 2
    }

    [SerializeField]
    protected TypeOfDamage TypeDamage;
    [SerializeField]
    protected float RadiusOfDamage;
    [SerializeField]
    protected int StartingBreakOut;

    protected Rigidbody rb;
    protected AudioSource audioSource;
    protected Vector3 startPosition;
    protected int currentBreakOut;
    protected bool isTakeDamage = true;
    private Weapon.WeaponType weaponType;

    //метод для получения необходимых параметров для пули
    public virtual void SetBulletParametres(Vector3 _startPosition, Quaternion _scatter,int _damage, float _distanseToLife, Weapon.WeaponType _type)
    {
        transform.rotation = _scatter;
        startPosition = _startPosition;
        transform.position = _startPosition;
        damage = _damage;
        distanseToLife = _distanseToLife;
        weaponType = _type;

        if(bulletFX == null)
        {
            bulletFX = GetComponent<Bullet>();
        }
        bulletFX.Init();
    }

    public virtual void Awake()
    {
        rb = GetComponent<Rigidbody>();
        player = FindObjectOfType<Player>();
        audioSource = GetComponent<AudioSource>();
    }

    public virtual void Start()
    {
        currentBreakOut = StartingBreakOut;
        bulletFX = GetComponent<Bullet>();
    }

    public virtual void Update()
    {
        if (Vector3.Distance(startPosition, transform.position) > distanseToLife)
        {
            DestroyBullet();
        }
        
    }

    //уничтожение объекта
    public virtual void DestroyBullet()
    {
        gameObject.SetActive(false);
        currentBreakOut = StartingBreakOut;
        isTakeDamage = true;
    }
    private IEnumerator ObstacleSound()
    {
        audioSource.clip = AudioManager.AudioIntanse.GetBulletObstacleSound();
        audioSource.Play();
        yield return new WaitForSeconds(0.05f);
        DestroyBullet();
    }
    
    public virtual void OnCollisionEnter(Collision collision)
    {
        switch (collision.collider.gameObject.tag)
        {
            case "Enemy":
                if (weaponType != Weapon.WeaponType.Enemy)
                {
                    enemy = collision.collider.GetComponent<Enemy>();
                    if (TypeDamage == TypeOfDamage.Point)
                    {
                        enemy.SetSoundEffect();
                        enemy.TakeDamage(damage);
                        bulletFX.MakeBloodFX(collision);
                    }
                    else
                    {
                        AreaDamage();
                    }
                    currentBreakOut--;
                    if (currentBreakOut == 0)
                    {
                        DestroyBullet();
                    }
                   
                }
                    break;
            case "obstacle":
                DestructibleObstacle _obstacle = collision.collider.GetComponent<DestructibleObstacle>();
                if(_obstacle!=null)
                {
                    _obstacle.TakeDamage(damage);
                }
                if (TypeDamage == TypeOfDamage.Area)
                {
                    AreaDamage();
                }
                else
                {
                    bulletFX.MakeHitFX(collision);
                }
                StartCoroutine(ObstacleSound());
                   
                //DestroyBullet();
                break;
            case "Ground":
                    DestroyBullet();
                    break;
            case "Player":
                    if (weaponType == Weapon.WeaponType.Enemy)
                    {
                        if (TypeDamage == TypeOfDamage.Point)
                            player.TakeDamage(damage);
                        else
                            AreaDamage();
                        currentBreakOut--;
                        if (currentBreakOut == 0)
                            DestroyBullet();
                    UIManager.UIManagerInstance.SetCameraDamageShake();
                    player.DamageSoundEffect();
                    bulletFX.MakeBloodFX(collision);
                }
                    break;
            case "NPC":
                if (weaponType == Weapon.WeaponType.Enemy)
                {
                    NPC npc = collision.collider.GetComponent<NPC>();
                    if (TypeDamage == TypeOfDamage.Point)
                        npc.TakeDamage(damage);
                    else
                        AreaDamage();
                    currentBreakOut--;
                    if (currentBreakOut == 0)
                        DestroyBullet();
                }
                break;
        }
    }

    //если у пули тип урона по площади
    public virtual void AreaDamage()
    {
        if (isTakeDamage)
        {
            Enemy enemy;
            // ищем все врагов в пределе радиуса поражения и наносим им урон
            for (int i = 0; i < EnemyManager.EnemyInstance.GetCountOfEnemies(); i++)
            {
                enemy = EnemyManager.EnemyInstance.GetEnemy(i);
                if (Vector3.Distance(enemy.transform.position, transform.position) <= RadiusOfDamage)
                {
                    enemy.TakeDamage(damage);
                }

                if (Vector3.Distance(player.transform.position, transform.position) <= RadiusOfDamage)
                {
                    player.TakeDamage(damage);
                 
                }
            }
            isTakeDamage = false;
        }
    }

    public virtual void SetVelocity()
    {
        currentBreakOut = StartingBreakOut;
        rb.velocity = rb.transform.forward* StartingSpeed;
    }

    public virtual Vector3 Target
    {
        get
        {
            return target;
        }
        set
        {
            target = value;
        }
    }

    public virtual int Damage
    {
        get
        {
            return damage;
        }
        set
        {
            damage = value;
        }
    }

    public virtual float RadiusDamage
    {
        get
        {
            return RadiusOfDamage;
        }
        set
        {
            RadiusOfDamage = value;
        }
    }

    public virtual float DistanseToLife
    {
        get
        {
            return distanseToLife;
        }
        set
        {
            distanseToLife = value;
        }
    }

    public virtual Vector3 StartingPosition
    {
        get
        {
            return startPosition;
        }
        set
        {
            startPosition = value;
        }
    }
}
