﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FirethrowerBullet : BaseBullet {

    public float LifeTime = 4f;
    private float currentLifeTime;
    
    public override void Start()
    {
        base.Start();
        currentLifeTime = LifeTime;
    }

    public override void Update()
    {
        currentLifeTime -= Time.deltaTime;
        if (currentLifeTime < 0 || Vector3.Distance(startPosition, transform.position) > distanseToLife)
        {
            DestroyBullet();
        }
    }

    public override void SetVelocity()
    {
        currentLifeTime = LifeTime;
        base.SetVelocity();
    }
}
