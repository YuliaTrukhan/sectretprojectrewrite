﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ButtonSound : MonoBehaviour {

    public AudioClip HoverSound;
    public AudioClip ClickSound;

    private AudioSource audioSource;

    private void Awake()
    {
        audioSource = GetComponent<AudioSource>();
    }

    public void SetHoverSound()
    {
        audioSource.PlayOneShot(HoverSound);
    }

    public void SetClickSound()
    {
        audioSource.PlayOneShot(ClickSound);
    }

    public void SetNormalColor()
    {

    }
}
