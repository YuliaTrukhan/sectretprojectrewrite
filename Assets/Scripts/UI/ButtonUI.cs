﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ButtonUI : MonoBehaviour {

    public GameObject HoverIcon;
    public GameObject NormalIcon;

    public void SetHover()
    {
        HoverIcon.SetActive(true);
        NormalIcon.SetActive(false);
    }

    public void SetNormal()
    {
        HoverIcon.SetActive(false);
        NormalIcon.SetActive(true);
    }
}
