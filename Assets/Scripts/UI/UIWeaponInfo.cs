﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIWeaponInfo : MonoBehaviour {
    
    [Header("UI")]
    public Image ProgressImage;
    public Text WeaponName;

    [Header("Info")]
    public int WeaponID;
    public int WeaponUnicID;
    public string ButtonName;

    [Header("Color")]
    public Color NormalColor;
    public Color LowColor;

    public float AnimationTimer = 0.01f;
    private bool animate = false;
    private float difference;
    private float currentValue;

    public void SetParametres(int _ID, string _name)
    {
        WeaponUnicID = _ID;
        WeaponName.text = _name.ToUpper();
    }

    public void SetProgress(float _currentHolderAmount, float _maxBullets)
    {
        currentValue = _currentHolderAmount / _maxBullets;
        difference = currentValue - ProgressImage.fillAmount;
        animate = true;
    }

    public void SetNoPatronsProgress()
    {
        animate = false;
        currentValue = 0;
        difference = 0;
        ProgressImage.fillAmount = 1f;
        ProgressImage.color = LowColor;
        WeaponName.color = LowColor;
    }

    private void Update()
    {
        if (animate)
        {
            ProgressImage.fillAmount+= difference * AnimationTimer;
            if (ProgressImage.fillAmount < 0.2f)
            {
                ProgressImage.color = LowColor;
                WeaponName.color = LowColor;
            }
            else
            {
                ProgressImage.color = NormalColor;
                WeaponName.color = NormalColor;
            }

            if ((difference >0 && ProgressImage.fillAmount >= currentValue) || (difference < 0 && ProgressImage.fillAmount <= currentValue))
            {
                ProgressImage.fillAmount = currentValue;
                animate = false;
            }
        }
    }
}
