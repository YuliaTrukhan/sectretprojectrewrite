﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SelectButton : MonoBehaviour {


    public InvertoryUIController.Window WindowType;
    [Header ("ButtonColor")]
    public Color SelectColor;
    public Color HoverColor;
    public Color NormalColor;

    [Header("ButtonText")]
    public Text ButtonText;
    public Color SelectColorText;
    public Color NormalColorText;

    private Image buttonImage;
    public bool IsSelect;

    private void Start()
    {
        buttonImage = GetComponent<Image>();
        if (IsSelect)
        {
            SetSelectColor();
        }
        else SetNormalColor();
    }

    public void SetSelectColor()
    {
        InvertoryUIController.invertoryUIController.CurrentOpenWinow = WindowType;
        InvertoryUIController.invertoryUIController.OpenWindowItems();
        buttonImage.color = SelectColor;
        ButtonText.color = SelectColorText;
        IsSelect = true;
    }

    public void ResetSelectColor()
    {
        IsSelect = false;
        ButtonText.color = NormalColorText;
        SetNormalColor();
    }

    public void SetNormalColor()
    {
        if (!IsSelect)
            buttonImage.color = NormalColor;
    }

    public void SetHoverColor()
    {
        if(!IsSelect)
            buttonImage.color = HoverColor;
    }
}
