﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameOverUI : MonoBehaviour {

    private Animator anim;
    private bool once=false;

    private void Awake()
    {
        anim = GetComponent<Animator>();
    }

    public void ResetParametres()
    {
        once = false;
        
    }

    public void SetGameOverPanel()
    {
        if (!once)
        {
            gameObject.SetActive(true);
            anim.SetTrigger("GameOver");
            once = true;
        }
        
        
    }
}
