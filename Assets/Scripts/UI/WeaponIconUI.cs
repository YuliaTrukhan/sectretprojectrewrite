﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class WeaponIconUI : MonoBehaviour {

    public Image WeaponIcon;
    public Image WeaponProgress;

    public Color ReloadColor;
    public Color NormalColor; 


    public void SetWeaponProgress(float _startingAmount, float _currentAmount)
    {
        WeaponProgress.fillAmount = _currentAmount / _startingAmount;
        if (WeaponProgress.fillAmount >= 1)
        {
            WeaponProgress.color = NormalColor;
        }
        else
        {
            WeaponProgress.color = ReloadColor;
        }
    }
}
