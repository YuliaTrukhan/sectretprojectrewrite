﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HealthBar : MonoBehaviour {

    [Header ("UI")]
    public Image BigHundler;
    public RectTransform Hundler;
    public Image WhiteLine;
    private Slider slider;

    [Header("Info")]
    private bool isDamaged=false;
    public float AnimationTimer=0.01f;
    private float difference;
    private float currentValue;

    private void Awake()
    {
        slider = GetComponent<Slider>();
    }

    public void ResetParametres()
    {
        BigHundler.gameObject.SetActive(false);
        difference = 0;
        currentValue = 0;
        isDamaged = false;
    }




    public void SetValue(float _currentValue, float _startingValue)
    {
        BigHundler.gameObject.SetActive(true);
        currentValue = _currentValue / _startingValue;
        difference = currentValue - slider.value;
        isDamaged = true;
    }

    public void SetStartValues(float _currentValue, float _startingValue)
    {
        slider.value = _currentValue / _startingValue;
        WhiteLine.fillAmount = slider.value;
        BigHundler.gameObject.SetActive(false);
        
    }

    private void Update()
    {
        if (isDamaged)
        {
            slider.value += difference * AnimationTimer;
            BigHundler.rectTransform.position = Hundler.position;
            if ((difference<0 && slider.value <= currentValue))
            {
                slider.value = currentValue;
                BigHundler.gameObject.SetActive(false);
                WhiteLine.fillAmount += difference * AnimationTimer;
                if (WhiteLine.fillAmount <= slider.value)
                {
                    WhiteLine.fillAmount = slider.value;
                    isDamaged = false;
                }
            }
            else if((difference >= 0 && slider.value >= currentValue))
            {
                slider.value = currentValue;
                BigHundler.gameObject.SetActive(false);
                WhiteLine.fillAmount = slider.value;
                isDamaged = false;
            }
        }
    }
}
