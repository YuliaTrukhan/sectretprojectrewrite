﻿using UnityEngine;
using System.Collections;
using UnityStandardAssets.Cameras;

public class CameraControl : MonoBehaviour {

    private Transform player;
    public float Speed;
    public float Movement = 3.5f;
    public Transform Target;

    [Header("HandHeld")]
    public float ShootShakeAmount;
    public float DamageShakeAmount;
    public float StartingShakeDuration = 0f;

    private float currentShakeDuration;
    private bool damagedShake;
    private bool shootShake;
    private float shakeAmount = 0.1f;
    private Vector3 originalPos;

    private Vector3 moveTemp;
    private float xDifferense;
    private float zDifferense;
    private bool initialization = false;
    private bool cameraShake;

    public LayerMask Mask;
    private Camera mainCamera;
    private Vector3 mousePos;
    private Vector3 delta;

    private void Awake()
    {
        player = FindObjectOfType<Player>().transform;
        currentShakeDuration = StartingShakeDuration;
        damagedShake = false;
        shootShake = false;

        mainCamera = Camera.main;
    }

    private void Start()
    {
        transform.position = Target.position;
        originalPos = Camera.main.transform.localPosition;
    }

    public void SetShakeShootCamera()
    {
        shakeAmount = ShootShakeAmount;
        shootShake = true;
    }
    
    public void SetShakeDamageCamera()
    {
        shakeAmount = DamageShakeAmount;
        shootShake = false;
        damagedShake = true;
    }

  
    public void ShakeCamera(float _shakeAmount)
    {
        currentShakeDuration -= Time.deltaTime;
        if (currentShakeDuration > 0f)
        {
            Camera.main.transform.localPosition = originalPos + Random.insideUnitSphere * shakeAmount;
            UIManager.UIManagerInstance.SetShakeUI(shakeAmount);

        }
        else
        {
            damagedShake = false;
            shootShake = false;
            currentShakeDuration = StartingShakeDuration;
            UIManager.UIManagerInstance.SetShakeOriginalPos();
        }
    }

    //движение камеры за объектом 
    private void Update()
    {
        if (!initialization && player != null)
        {
            transform.position = player.position;
            Target.position = player.position;
            initialization = true;
        }

        if (!GameManager.GameInstance.IsGameOver)
        {
            if (damagedShake)
            {
                ShakeCamera(DamageShakeAmount);
            }
            else if (shootShake)
            {
                ShakeCamera(ShootShakeAmount);
            }

            /*if (player.position.x > transform.position.x)
            {
                xDifferense = player.position.x - transform.position.x;
            }
            else
            {
                xDifferense = transform.position.x - player.position.x;
            }

            if (player.position.z > transform.position.z)
            {
                zDifferense = player.position.z - transform.position.z;
            }
            else
            {
                zDifferense = transform.position.z - player.position.z;
            }

            if (xDifferense >= Movement || zDifferense >= Movement)
            {
                moveTemp = player.position;
                Target.position = Vector3.MoveTowards(transform.position, moveTemp, 10f * Time.deltaTime);
            }
            
            transform.position = Vector3.Lerp(transform.position, moveTemp, Time.deltaTime * Speed);*/
            //Vector3.MoveTowards(transform.position, Target.position, Time.deltaTime*Speed);

            Ray ray = mainCamera.ScreenPointToRay(Input.mousePosition);
            RaycastHit hit;


            if (Physics.Raycast(ray, out hit, 100f, Mask))
            {
                mousePos = new Vector3(hit.point.x, transform.position.y, hit.point.z);
                delta = Vector3.ClampMagnitude(mousePos - transform.position, 1f);
            }
            else
            {
                delta = Vector3.zero;
            }
            transform.position = player.position + delta;
        }
    }
}
