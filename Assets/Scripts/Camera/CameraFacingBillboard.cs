﻿using UnityEngine;
using System.Collections;

public class CameraFacingBillboard : MonoBehaviour
{
    private Camera cameraHash;

    void Start()
    {
        cameraHash = Camera.main;
    }

    void Update()
    {
        //полоска жизни объекта всегда смотрит на камеру 
        transform.LookAt(transform.position + cameraHash.transform.rotation * Vector3.forward, cameraHash.transform.rotation * Vector3.up);
    }
}
