﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Vignette : MonoBehaviour {

    public float AnimationTimer;
    private bool animate;
    private bool reduce;
    private Image vignette;
    private Color color;

    private void Awake()
    {
        animate = false;
        reduce = false;
        vignette = GetComponent<Image>();
        color = vignette.color;
    }

    public void SetDamage()
    {
        animate = true;
    }

    private void Update()
    {
        if (animate)
        {
            color.a += AnimationTimer * Time.deltaTime;
            vignette.color = color;
            if(color.a >= 1)
            {
                animate = false;
                reduce = true;
            }
        }
        if (reduce && !animate)
        {
            color.a -= AnimationTimer * Time.deltaTime;
            vignette.color = color;
            if (color.a <=0)
            {
                reduce = false;
            }
        }
    }

    public void SetDieLowHPVignette(float _currentAmount, float _startAmount)
    {
        color.a = 1- _currentAmount/_startAmount;
        vignette.color = color;
    }
}
