﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObstacleController : MonoBehaviour {

    public static ObstacleController Instanse;
    public GameObject ObstaclePrefab;
    public float StartingTimerToSpawn = 10f;
    public int MaxActiveObstacles=9;

    private float currentTimerToSpawn;
    private int currentAcriveObstacles; 
    private List<GameObject> obstacleList;
    private Vector3 genericPosition;
    private List<Transform> allObstacles;

    public Transform[] planes;


    private void Awake()
    {
        Instanse = this;
        obstacleList = new List<GameObject>();
        allObstacles= new List<Transform>();
    }

    private void Start()
    {
        currentAcriveObstacles = 0;
        currentTimerToSpawn = StartingTimerToSpawn;
       SetActiveObstacles();
    }

    private void Update()
    {
        if (!GameManager.GameInstance.IsGameOver && !GameManager.GameInstance.Pause)
        {
            currentTimerToSpawn -= Time.deltaTime;
            //есличисло препятсвий меньше максимально дозволенных
            if (currentTimerToSpawn < 0 && currentAcriveObstacles < MaxActiveObstacles)
            {
                int indexPlane = Random.Range(0, planes.Length);

                //случайно генерируем позицию на уровне, на котором сейчас находится игрок
                genericPosition = new Vector3(
                    Random.Range(planes[indexPlane].position.x - planes[indexPlane].localScale.x / 2f * 8f, planes[indexPlane].position.x + planes[indexPlane].localScale.x / 2f * 8f),
                    ObstaclePrefab.transform.localScale.y / 2f,
                    Random.Range(planes[indexPlane].position.z - planes[indexPlane].localScale.z / 2f * 8f, planes[indexPlane].position.z + planes[indexPlane].localScale.z / 2f * 8f)
                    );

                //проверяем наличие других объектов вокруг указанной позиции
                //если все ок, то генерируем объект
                if (CheckPosition(genericPosition))
                {
                    //берем из пула и генерируем объект
                    GameObject genericObstacle = GetObstacle();
                    genericObstacle.transform.position = genericPosition;
                    genericObstacle.SetActive(true);
                    SetActiveObstacles();
                    currentTimerToSpawn = StartingTimerToSpawn;
                }
            }
        }
    }

    public bool CheckPosition(Vector3 _position)
    {
        for (int i = 0; i < allObstacles.Count; i++)
        {
            if (Vector3.Distance(allObstacles[i].position, _position) < 13f)
            {
                return false;
            }
        }
        return true;
    }

    //для пула препятсвий
    public GameObject GetObstacle()
    {
        //если в массиве есть неактивный объект, возвращаем его
        foreach (GameObject _obstacle in obstacleList)
        {
            if (!_obstacle.gameObject.activeSelf)
                return _obstacle;
        }

        //в ином случае генерируем новый
        GameObject obstacle = Instantiate(ObstaclePrefab);
        return obstacle;
    }

    //получаем число активных уничтожаемых препятствий на сцене
    public void SetActiveObstacles()
    {
        currentAcriveObstacles = 0;
        for (int i = 0; i < obstacleList.Count; i++)
        {
            if (obstacleList[i].activeSelf)
                currentAcriveObstacles++;
        }
    }

    //добавление объекта в пул
    public void AddObstacle(GameObject _obstacle)
    {
        obstacleList.Add(_obstacle);
    }

    //добавление в общий массив пепятсвий (для проверки, чтобы объекты не генерировались в других объектах)
    public void AddToAllObstacle(Transform _obstacle)
    {
        allObstacles.Add(_obstacle);
    }

    public void RemoveOfAllObstacke(Transform _obstacle)
    {
        allObstacles.Remove(_obstacle);
    }
}
