﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestructibleObstacle : MonoBehaviour {

    //прочность объекта
    public int StartingStrength = 5;
    private int currentStrength;

    private void Start()
    {
        ObstacleController.Instanse.AddObstacle(gameObject);
        ObstacleController.Instanse.AddToAllObstacle(transform);
        currentStrength = StartingStrength;
    }
    
    public void TakeDamage(int _damage)
    {
        currentStrength -= _damage;
        if (currentStrength <= 0)
        {
            DestroyObstacle();
        }
    }

    //уничтожеие объекта
    public void DestroyObstacle()
    {
        ObstacleController.Instanse.RemoveOfAllObstacke(transform);
        transform.gameObject.SetActive(false);
        currentStrength = StartingStrength;
        ObstacleController.Instanse.SetActiveObstacles();        
    }
    
}
