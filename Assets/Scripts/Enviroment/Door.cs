﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Door : MonoBehaviour {

    //сущность двери
    
    private Vector3 startingPosition;

    private void Start()
    {
        startingPosition = transform.position;
    }

    //дверь открывается, когда игрок подходит к ней (триггер стоит со стороны коридора)
    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player" || other.tag == "NPC")
        {
            OpenDoor();
        }
    }

    //дверь закрывается, когда игрок покидает триггер (триггер стоит со стороны коридора)
    private void OnTriggerExit(Collider other)
    {
        {
            if (other.tag == "Player" || other.tag == "NPC")
            {
                CloseDoor();
            }
        }
    }

    //для открытия двери
    public void OpenDoor()
    {
        transform.position = new Vector3(transform.position.x, startingPosition.y + 5f, transform.position.z);
    }

    //для закрытия двери
    public void CloseDoor()
    {
        transform.position = startingPosition;
    }
}
