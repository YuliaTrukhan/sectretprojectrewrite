﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CoridorController : MonoBehaviour
{
    //для проверки, находится ли игрок в коридоре между уровнями
    private Player player;

    private void Start()
    {
        player = FindObjectOfType<Player>();
    }

    //если игрок в коридоре
    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            player.inOnLevel = false;
        }
    }

    //если игрок вышел из коридора
    private void OnTriggerExit(Collider other)
    {
        if (other.tag == "Player")
        {
            player.inOnLevel = true;
        }
    }
}
