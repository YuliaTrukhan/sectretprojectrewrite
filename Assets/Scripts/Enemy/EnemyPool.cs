﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class EnemyPool : MonoBehaviour
{
    public static EnemyPool Current;
    private List<Enemy> pooledEnemies;

    void Awake()
    {
        Current = this;
    }

    void Start()
    {
        pooledEnemies = new List<Enemy>();
    }


    //метод для запроса объекта из пула 
    public Enemy GetPooledEnemy(Enemy _enemy, bool _isSingle)
    {
        foreach (Enemy _thisObject in pooledEnemies)
        {
            if (!_thisObject.gameObject.activeSelf && _thisObject.name == _enemy.name + "(Clone)")
            {
                _thisObject.Single = _isSingle;
                return _thisObject;
            }
        }

        //если неактивных объектов нет, добавляем еще один
        Enemy _obj = Instantiate(_enemy);
        _obj.Single = _isSingle;
        pooledEnemies.Add(_obj);
        return _obj;
    }
}
