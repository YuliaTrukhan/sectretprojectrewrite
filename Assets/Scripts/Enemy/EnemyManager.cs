﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class EnemyManager : MonoBehaviour
{
    //EnemyManager отвечает за генерацию врагов
    
    public static EnemyManager EnemyInstance;

    public int MaxActiveEnemiesPerZone = 5;
    public int  MaxActiveSingleEnemies=3;
    public int AlarmChance = 30;
    public float StartingSpawnTimer = 7f;
    public float StartingAlarmTimer=60f;
    public float StartingAlarmSpawnTimer = 1f;

    private int enemiesCountInCatacomb;
    private int currentActiveSingleEnemies;
    private float currentAlarmTimer;
    private float currentSpawnTimer;
    private float currentAlarmSpawnTimer;
    private int indexOfSpawns;
    private bool alarm = false;
    private int[] enemiesCountPerZone;
    private bool isSpawn = true;
    private List<Enemy> enemiesList;
    public Transform[] SpawnersEnemy;
    public Transform[] SpawnersSingleEnemy;
    private Transform[] mainPlanes;
    public Enemy[] Enemies;
    private EnemyColors enemyColors;
    private Color enemyColor;
    private bool checkDestination = false;
    private Vector3 tempDestination;
    private bool initialization;
    private bool spawnDungeon;

    private void Awake()
    {
        EnemyInstance = this;
        if(DungeonController.DungeonMode && !DungeonController.dungeonController.GetCurrentDungeon().IsClear)
        {
            spawnDungeon = true;
        }
        else
        {
            spawnDungeon = false;
        }
    }

    private void Start()
    {
        initialization = false;
        currentSpawnTimer = 0;
        currentAlarmSpawnTimer = 0;
        enemiesList = new List<Enemy>();
        currentAlarmTimer = StartingAlarmTimer;
        enemiesCountPerZone = new int[SpawnersEnemy.Length];
        indexOfSpawns = 0;
        mainPlanes = ObstacleController.Instanse.planes;
        if (DungeonController.DungeonMode)
        {
            enemiesCountInCatacomb = DungeonController.dungeonController.GetCurrentDungeon().EnemiesCount;
        }
        enemyColors = FindObjectOfType<EnemyColors>();
        alarm = false;
        UIManager.UIManagerInstance.SetAlarmText(false);
        currentActiveSingleEnemies = 0;
    }



    //метод для спавна врага
    private void SpawnEnemy(int _indexOfSpawn, bool _isSingle)
    {
        //генерируем и проверяем начальную позицию врага
        checkDestination = true;
        while (checkDestination)
        {
            tempDestination = GetRandomDestination(_indexOfSpawn);
            if (!ObstacleController.Instanse.CheckPosition(tempDestination))
            {
                continue;
            }
            else
            {
                checkDestination = false;
            }
        }
        
        //обращаемся к пулу врагов, получаем объект
        Enemy _enemy = EnemyPool.Current.GetPooledEnemy(Enemies[Random.Range(0, Enemies.Length)], _isSingle);
        //задаем изначальную позицию
        _enemy.isDied = false;
        _enemy.transform.position = tempDestination;
        if (_indexOfSpawn == -1)
        {
            _enemy.transform.position = GetSingleSpawner();
        }
        //id врага равно индексу его спавнера. Если враг-одиночка, его id -1 
        _enemy.EnemyID = _indexOfSpawn;
        _enemy.SpawnNumber = _indexOfSpawn;

        //если на главной сцене 
        if (!DungeonController.DungeonMode && _indexOfSpawn != -1)
        {
            //увеличиваем счетчик врагов на спав-зоне
            enemiesCountPerZone[_indexOfSpawn]++;
            //выбираем и меняем цвет
            enemyColor = enemyColors.enemyColors[_indexOfSpawn];
            _enemy.ChangeColor(enemyColor);
        }
        //если в катакомбах, то id врага будет равно id подземелья
        if (DungeonController.DungeonMode)
        {
            _enemy.EnemyID = DungeonController.dungeonController.GetCurrentDungeon().DungeonID;
            _enemy.ChangeColor(Color.white);
        }

        _enemy.gameObject.SetActive(true);
        //добавляем в массив
        enemiesList.Add(_enemy);
    }
    
    //метод для проверки количества врагов на спавн-зоне
    public void CheckZone(int _spawnNumber)
    {
        if (_spawnNumber == -1)
        {
            currentActiveSingleEnemies--;
        }
        else
        {
            enemiesCountPerZone[_spawnNumber]--;
            //если на спавн-зоне не осталось врагов, то генерируем новую партию
            if (enemiesCountPerZone[_spawnNumber] == 0)
            {
                isSpawn = true;
                indexOfSpawns = 0;
                currentSpawnTimer = StartingSpawnTimer;
            }
        }
    }
    
    //для спавна врагов в катакомбах
    private void DungeonSpawner()
    {
        for (int i = 0; i < enemiesCountInCatacomb; i++)
        {
            SpawnEnemy(indexOfSpawns, false);
        }
        isSpawn = false;
    }


    public void SetEnemyMaterial(Enemy _enemy)
    {
        for (int i = 0; i < enemiesList.Count; i++)
        {
            enemiesList[i].SetNormalMaterial();
            enemiesList[i].isClosest = false;
            if (enemiesList[i] == _enemy)
            {
                enemiesList[i].SetHoverMaterial();
                enemiesList[i].isClosest = true;
            }
        }
    }
    

    private void Update()
    {
        if(!GameManager.GameInstance.IsGameOver && !GameManager.GameInstance.Pause)
        {
            if (!initialization && SpawnersSingleEnemy.Length != 0 && SpawnersEnemy.Length != 0)
                initialization = true;
            //обычный режим
            if (!alarm && !DungeonController.DungeonMode && initialization)
            {
                currentSpawnTimer -= Time.deltaTime;
                if (isSpawn && currentSpawnTimer < 0)
                {
                    MainSpawner();
                }

                //спавн врагов-одиночек (определенное количество)
                if (currentActiveSingleEnemies < MaxActiveSingleEnemies)
                {
                    SpawnEnemy(-1, true);
                    currentActiveSingleEnemies++;
                }
            }
            //если в катакомбах
            if(DungeonController.DungeonMode && isSpawn && spawnDungeon)
            {
                DungeonSpawner();
            }
            //паника
            if (alarm)
            {
                //спавним врагов через промежуток времени
                currentAlarmSpawnTimer -= Time.deltaTime;
                currentAlarmTimer -= Time.deltaTime;
                if (currentAlarmTimer > 0 && currentAlarmSpawnTimer < 0)
                {
                    AlarmSpawn();
                }

                //по истечении времени, паника заканчивается
                if (currentAlarmTimer < 0)
                {
                    UIManager.UIManagerInstance.SetAlarmText(false);
                    alarm = false;
                    currentAlarmSpawnTimer = StartingAlarmSpawnTimer;
                }
            }
            if (DungeonController.DungeonMode)
            {
                alarm = false;
                UIManager.UIManagerInstance.SetAlarmText(false);
            }
        }
    }
    
    public Vector3 GetSingleSpawner()
    {
        //возвращаем спавнер для одиночного врага(ошибка)
        return SpawnersSingleEnemy[Random.Range(0, SpawnersSingleEnemy.Length)].position;
    }

    //для получения случайной точки на спавнзоне/карте
    public Vector3 GetRandomDestination(int _spawnIndex)
    {
        if (DungeonController.DungeonMode || _spawnIndex == -1)
        {
            //если основных полей несколько, случайно выбраем один из них
            int index = Random.Range(0, mainPlanes.Length);
            return new Vector3(
                Random.Range(mainPlanes[index].position.x - mainPlanes[index].localScale.x / 2f * 9.5f, mainPlanes[index].position.x + mainPlanes[index].localScale.x / 2f * 9.5f),
                0f,
                Random.Range(mainPlanes[index].position.z - mainPlanes[index].localScale.z / 2f * 9.5f, mainPlanes[index].position.z + mainPlanes[index].localScale.z / 2f * 9.5f)
                );
        }
        else
        {
            return new Vector3(
                Random.Range(SpawnersEnemy[_spawnIndex].position.x - SpawnersEnemy[_spawnIndex].localScale.x / 2f * 10f, SpawnersEnemy[_spawnIndex].position.x + SpawnersEnemy[_spawnIndex].localScale.x / 2f * 10f),
                0f,
                Random.Range(SpawnersEnemy[_spawnIndex].position.z - SpawnersEnemy[_spawnIndex].localScale.z / 2f * 10f, SpawnersEnemy[_spawnIndex].position.z + SpawnersEnemy[_spawnIndex].localScale.z / 2f * 10f)
                );
        }
    }

    //для спавна врагов в основной сцене
    private void MainSpawner()
    {
        //спавним врагов в спавн-зоне до нужного количества
        if (enemiesCountPerZone[indexOfSpawns] < MaxActiveEnemiesPerZone)
        {
            SpawnEnemy(indexOfSpawns, false);
        }
        else
        {
            //когда нужно количество врагов набралось, переходим к следующей спавн-зоне
            if (indexOfSpawns < enemiesCountPerZone.Length - 1)
            {
                indexOfSpawns++;
            }
            //когда каждая зона набрала нужное кол-во врагов, останавливаем спавн
            else
            {
                isSpawn = false;
            }

        }
    }

    //спавн врагов при Панике
    private void AlarmSpawn()
    {
        //спавним врагов сразу со всех спавнов
        for (int i = 0; i < SpawnersEnemy.Length; i++)
        {
            SpawnEnemy(i, false);
        }

        currentAlarmSpawnTimer = StartingAlarmSpawnTimer;
    }


    //возвращается длина массива врагов
    public int GetCountOfEnemies()
    {
        if (enemiesList != null)
            return enemiesList.Count;
        else return 0;
    }

    public void AddToList(Enemy _enemy)
    {
        enemiesList.Add(_enemy);
    }

    public void SetClosetsEnemy(Enemy _enemy)
    {
        for(int i=0; i<enemiesList.Count; i++)
        {
            enemiesList[i].isClosest = false;
            if (enemiesList[i] == _enemy)
            {
                enemiesList[i].isClosest = true;
            }
        }
    }

    //возвращается объект из массива по индексу 
    public Enemy GetEnemy(int _index)
    {
        return enemiesList[_index];
    }

    //удаляем из массива объект
    public void RemoveFromList(Enemy _enemy)
    {
        enemiesList.Remove(_enemy);
    }
    
    public bool Alarm
    {
        get
        {
            return alarm;
        }
        set
        {
            alarm = value;
        }
    }
   
}
