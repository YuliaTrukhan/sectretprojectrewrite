﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyDefenderController : MonoBehaviour {

    //класс для контроля врагов (Оборона от волн зомби \ от армии зомби )

    public enum DefenderType
    {
        Wave, //волны - враги спавнятся постоянно
        Army //армия - враги спавнятся до определенного количества
    };

    public DefenderType Type;
    public int EnemiesCount;
    public float StartingSpawnTimer;
    private int currentEnemiesCount;

    private bool isSpawn;
    private float currentSpawnTimer;
    
    public List<Transform> EnemySpawners = new List<Transform>();


    private void Start()
    {
        currentEnemiesCount = 0;
        currentSpawnTimer = StartingSpawnTimer;
        isSpawn = false;
    }

    private void Update()
    {
        if (isSpawn)
        {
            currentSpawnTimer -= Time.deltaTime;
            if (currentSpawnTimer < 0)
            {
                SpawnEnemy();
            }

            if(Type == DefenderType.Army && currentEnemiesCount >= EnemiesCount)
            {
                isSpawn = false;
            }
        }
    }

    //метод для спавна врагов
    public void SpawnEnemy()
    {
        for(int i=0; i<EnemySpawners.Count; i++)
        {
            Enemy _enemy = EnemyPool.Current.GetPooledEnemy(EnemyManager.EnemyInstance.Enemies[Random.Range(0, EnemyManager.EnemyInstance.Enemies.Length)], false);
            _enemy.transform.position = EnemySpawners[i].position;
            _enemy.EnemyID = DungeonController.dungeonController.GetCurrentDungeon().DungeonID;
            EnemyManager.EnemyInstance.AddToList(_enemy);
            _enemy.gameObject.SetActive(true);
            currentEnemiesCount++;
        }

        currentSpawnTimer = StartingSpawnTimer;
    }

    //когда игрок входит в триггер, начинают спавниться враги
    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            isSpawn = true;
        }
    }
}
