﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyColors : MonoBehaviour {

    public Color[] enemyColors;
    private Color enemyColor;
    public string[] colorName;

    public string GetColor(int _index)
    {
        return colorName[_index];
    }
}
