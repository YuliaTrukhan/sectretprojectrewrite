﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyManager1 : MonoBehaviour {

    public static EnemyManager1 EnemyInstance;

    public List<EnemySpawner> EnemiesSpawners = new List<EnemySpawner>();
    public List<Transform> Planes = new List<Transform>();

    public int SingleEnemiesCount;
    public bool Alarm=false;
    public float StartingAlarmTimer;

    private int currentSingleEnemiesCount;
    private float currentAlarmTimer;
    
}
