﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemySpawner : MonoBehaviour {

    public int ID; 
    public int EnemiesCount;
    public float StartingSpawnTimer;

    private float currentSpawnTimer;
    private int currentEnemiesCount;
    private Transform thisTransform;
    private bool isEmpty;

    private void Awake()
    {
        currentSpawnTimer = StartingSpawnTimer;
        thisTransform = transform;
        isEmpty=true;
    }

    private void Update()
    {
        if (isEmpty)
        {

        }
    }

    public Vector3 GetRandomDestination()
    {
        return new Vector3(
            Random.Range(thisTransform.position.x-thisTransform.localScale.x/2f*10f, thisTransform.position.x + thisTransform.localScale.x / 2f * 10f),
            0f,
            Random.Range(thisTransform.position.z - thisTransform.localScale.z / 2f * 10f, thisTransform.position.z + thisTransform.localScale.z / 2f * 10f)
            );
    }

    public void SpawnEnemy()
    {

    }

    public void CheckEmptySpawner()
    {

    }
}
