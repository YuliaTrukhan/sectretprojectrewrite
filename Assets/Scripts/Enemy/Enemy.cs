﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.AI;
using System.Collections.Generic;

public class Enemy : MonoBehaviour
{
    [Header("PoolInfo")]
    public float PooledObjectLifeTime;
    public bool isDied;
    public bool isClosest = false;
    //сущность врага
    [Header("EnemyInformaiton")]
    public string EnemyName;
    public int EnemyID;
    public int StartingHealth = 10;
    public int StartingDamage = 20;
    public float Speed = 5f;
    public float DistanceToAttack = 2.5f;
    public float TimeBetweenAttack = 1f;
    public float RadiusOfHear=30f;
    public float RadiusOfWatch=15f;
    public float StartingTimerToCheckNoise = 5f;
    public List<int> DropItemsID = new List<int>();
    public int MaxCoinCount;
    public bool Single = false;

    [Header("Sprites and UI")]
    public Image EnemyHealthBar;
    public GameObject FieldOfView;
    public GameObject FieldOfHear;
    public SpriteRenderer MiniMapIcon;

    [Header("Material")]
    public Material NormalMaterial;
    public Material HoverMaterial;

    [Header("For shooting enemy")]
    public bool CanShoot = false;
    public Weapon[] Weapons;

    private int indexOfCurrentWeapon;
    private NavMeshAgent agent;
    private Player player;
    private Transform thisTransform;
    private Weapon currentWeapon;
    private RaycastHit hit;
    private Vector3 RandomDestination;
    private Vector3 positionToCheck;
    private Collider collidr;
    private Color skinColor;
    private AudioSource audioSource;
    private int currentHealth;
    private int currentDamage;
    private float currentTimeBetweenAttack;
    private float currentTimerToCheckNoise;
    private int spawnNumber; //для того, чтобы знать, к какому спаву приадлежит враг
    private bool isMoving = false;
    private bool checkNoise = false;
    private bool takeAlarm = true;
    private float wanderStopDistance = 4f;

    

    private void Start()
    {
        EnemyName = Random.Range(0, 10000).ToString();
        isDied = false;
        SetNormalMaterial();
        thisTransform = transform;
        //FieldOfView.transform.localScale = new Vector3(RadiusOfWatch, RadiusOfWatch, RadiusOfWatch)/5f;
        //FieldOfHear.transform.localScale = new Vector3(RadiusOfHear, RadiusOfHear, RadiusOfHear)/6f;

        if (CanShoot)
        {
            ChooseWeapon();
        }
        audioSource = GetComponent<AudioSource>();
        agent = GetComponent<NavMeshAgent>();
        collidr = GetComponent<Collider>();
        agent.speed = Speed;
        agent.stoppingDistance = DistanceToAttack;
        agent.enabled = false;
        player = FindObjectOfType<Player>();
        currentHealth = StartingHealth;
        currentDamage = StartingDamage;
        currentTimeBetweenAttack = TimeBetweenAttack;
        //SetEnemyHealthBar();
        currentTimerToCheckNoise = StartingTimerToCheckNoise;
        takeAlarm = true;
    }

    private void Update()
    {
        if (!GameManager.GameInstance.IsGameOver && !GameManager.GameInstance.Pause && !isDied)
        {
            //если игрок попал в зону видимости врага или поднята тревога, то преследуем игрока
            if ((Vector3.Distance(thisTransform.position, player.transform.position) <= RadiusOfWatch && player.inOnLevel) 
                || (EnemyManager.EnemyInstance.Alarm && player.inOnLevel) || (DungeonController.DungeonMode && player.inOnLevel))
            {
                checkNoise = false;
                isMoving = false;
                //пробуем поднять тревогу
                if (!DungeonController.DungeonMode)
                {
                    TakeAlarm();
                }
                //преследование игрока
                Chasing();
                takeAlarm = false;
            }
            else
            {
                //патрулирование
                Wander();
                takeAlarm = true;
            }

            //если услышали выстрел, идем к месту шума
            if (player.PositionOfShoot != Vector3.zero && Vector3.Distance(thisTransform.position, player.PositionOfShoot) <= RadiusOfHear && player.inOnLevel)
            {
                checkNoise = true;
                positionToCheck = player.PositionOfShoot;
            }

            if (checkNoise && player.inOnLevel)
            {
                CheckNoise();
            }
        }
        else if (isDied)
        {
            PooledObjectLifeTime -= Time.deltaTime;
            if (PooledObjectLifeTime < 0)
                ResetParametres();

        }
    }

    public void SetSoundEffect()
    {
        audioSource.clip = AudioManager.AudioIntanse.GetEnemyDamageShot();
        audioSource.Play();
    }

    public void SetHealthBar()
    {
        UIManager.UIManagerInstance.SetStartEnemyHealthValues(EnemyName, StartingHealth, currentHealth);
    }

    //метод для поднятия паники
    private void TakeAlarm()
    {
        if (!EnemyManager.EnemyInstance.Alarm && takeAlarm)
        {
            //генерируем шанс
            int chanceAlarm = Random.Range(0, 100);
            //проверяем его
            if (chanceAlarm <= EnemyManager.EnemyInstance.AlarmChance)
            {
                //если шанс попадает, то поднимаем панику
                EnemyManager.EnemyInstance.Alarm = true;
                UIManager.UIManagerInstance.SetAlarmText(true);
            }
        }
    }

    //для проверки места шума
    private void CheckNoise()
    {
        //идем к месту шума        
        Move(positionToCheck);
        thisTransform.LookAt(positionToCheck);
        //когда подходим к нему, начинаем осматриваться
        if (Vector3.Distance(thisTransform.position, positionToCheck) < 5f)
        {
            currentTimerToCheckNoise -= Time.deltaTime;
            //если никого не заметили, то возвращаемся обратно к патрулированию
            if (currentTimerToCheckNoise < 0)
            {
                checkNoise = false;
                currentTimerToCheckNoise = StartingTimerToCheckNoise;
                isMoving = false;
                Wander();
            }
        }
    }

    //преследование игрока
    public void Chasing()
    {
        transform.LookAt(new Vector3(player.transform.position.x, thisTransform.position.y, player.transform.position.z));
        //если враг не стреляющий
        if (!CanShoot)
        {
            //если расстояние больше, чем расстояние для атаки, ведем объект к цели
            if (Vector3.Distance(player.transform.position, thisTransform.position) > DistanceToAttack)
            {
                Move(player.transform.position);
            }
            else
            {
                Attack();
            }
        }
        //если враг стреляющий
        if (CanShoot)
        {
            currentWeapon.transform.LookAt(player.transform.position);
            Physics.Raycast(thisTransform.position, thisTransform.forward, out hit);
            if (Vector3.Distance(player.transform.position, currentWeapon.BulletSpawn.position) <= currentWeapon.DistanseToShoot
                && hit.collider.tag != "obstacle")
            {
                agent.enabled = false;
                //если на линии стрельбы есть союзник, то немного сдвигаемся в сторону
                if (hit.collider.tag == "CheckShot")
                {
                    thisTransform.RotateAround(player.transform.position, new Vector3(0, Random.Range(-1, 1), 0), Speed * Time.deltaTime * 10f);
                }
                else
                {
                    currentWeapon.Shoot();
                }
            }
            else
            {
                Move(player.transform.position);
            }
        }
    }

    //движение объекта
    private void Move(Vector3 _destination)
    {
        agent.enabled = true;
        agent.destination=_destination;
    }

    //атака через определенные интеравалы
    private void Attack()
    {
        agent.enabled = false;
        currentTimeBetweenAttack -= Time.deltaTime;
        if (currentTimeBetweenAttack < 0)
        {
            player.TakeDamage(currentDamage);
            currentTimeBetweenAttack = TimeBetweenAttack;
        }
    }
    
    //патрулирование
    private void Wander()
    {
        agent.enabled = true;
        if (!isMoving)
        {
            //генерируем случайную точку на спавн-зоне, идем к ней
            RandomDestination = EnemyManager.EnemyInstance.GetRandomDestination(spawnNumber);
            thisTransform.LookAt(RandomDestination);
            agent.destination = RandomDestination;
            isMoving = true;
        }
        //на подходе к точке останавливаемся, чтобы сменить путь
        if (Vector3.Distance(transform.position, RandomDestination) < wanderStopDistance)
        {
            isMoving = false;
        }
    }

    //метод для получения урона
    public void TakeDamage(int _damage)
    {
        if (!isDied)
        {
            if (_damage > currentHealth)
                currentHealth = 0;
            else
                currentHealth -= _damage;
            //SetEnemyHealthBar();
            if(isClosest)
                UIManager.UIManagerInstance.SetDamagedEnemyHealthBar(EnemyName, StartingHealth, currentHealth);
            if (currentHealth <= 0)
            {
                Die();
            }
        }
    }

    //установка полоски жизни
    //private void SetEnemyHealthBar()
    //{
    //    EnemyHealthBar.fillAmount = (float)currentHealth / (float)StartingHealth;
    //}

    //метод для выпада дропа
    private void CheckDrop()
    {
        if (DropItemsID.Count > 0)
        {
            int tempID = DropItemsID[Random.Range(0, DropItemsID.Count)];
            ItemController.itemController.CheckDropItem(transform.position, tempID, MaxCoinCount);
        }
    }

    public void ResetParametres()
    {
        currentHealth = StartingHealth;
       // SetEnemyHealthBar();
        if (CanShoot)
        {
            ChooseWeapon();
        }
        takeAlarm = true;
        Single = false;
        checkNoise = false;
        ChangeColor(Color.white);
        //FieldOfView.SetActive(true);
        //FieldOfHear.SetActive(true);
        collidr.enabled = true;
        gameObject.SetActive(false);
    }



    //смерть объекта
    private void Die()
    {

        if (!DungeonController.DungeonMode)
        {
            EnemyManager.EnemyInstance.CheckZone(spawnNumber);
        }
        else
        {
            DungeonController.dungeonController.CheckDungeonClear(EnemyID);
        }
        EnemyManager.EnemyInstance.RemoveFromList(this);
        SetNormalMaterial();
        collidr.enabled = false;
        CheckDrop();
        agent.enabled = false;
        QuestsController.questController.CheckProgress(EnemyID);
        //FieldOfView.SetActive(false);
        //FieldOfHear.SetActive(false); 
        isClosest = false;
        isDied = true;
       
        //ResetParametres();
    }

    //метод для генерации случайного оружия
    public void ChooseWeapon()
    {
        indexOfCurrentWeapon = Random.Range(0, Weapons.Length);
        SetActiveWeapon();
        currentWeapon = Weapons[indexOfCurrentWeapon];
    }

    //метод для переключения оружия
    private void SetActiveWeapon()
    {
        for (int i = 0; i < Weapons.Length; i++)
        {
            if (i == indexOfCurrentWeapon)
            {
                Weapons[i].gameObject.SetActive(true);
                indexOfCurrentWeapon = i;
            }
            else
            {
                Weapons[i].gameObject.SetActive(false);
            }
        }
    }

    public void SetHoverMaterial()
    {
        GetComponent<Renderer>().material = HoverMaterial;
        if (spawnNumber != -1)
            GetComponent<Renderer>().material.color = skinColor;
        else GetComponent<Renderer>().material.color = Color.white;
    }

    public void SetNormalMaterial()
    {
        GetComponent<Renderer>().material = NormalMaterial;
        if (spawnNumber != -1)
            GetComponent<Renderer>().material.color = skinColor;
        else GetComponent<Renderer>().material.color = Color.white;
    }

    public void ChangeColor(Color _color)
    {
        skinColor = _color;
        GetComponent<Renderer>().material.color = _color;
        MiniMapIcon.color = _color;
    }

    public int SpawnNumber
    {
        set
        {
            spawnNumber = value;
        }
        get
        {
            return spawnNumber;
        }
    }


    private void OnMouseEnter()
    {
        if (InputManager.InputManagerInstance.PlayerControlType == InputManager.TypeControl.KeyboardControl)
        {
            isClosest = true;
            SetHoverMaterial();
        }
    }

    private void OnMouseExit()
    {
        if (InputManager.InputManagerInstance.PlayerControlType == InputManager.TypeControl.KeyboardControl)
        {
            isClosest = false;
            SetNormalMaterial();
        }
    }
}
