﻿using UnityEngine;
using System.Collections;

public class Pistol : Weapon {

    public override void SetBulletText()
    {
        
        UIManager.UIManagerInstance.SetBulletText(currentHolderAmount, StartingHolderAmount, 999);
    }

    public override void Reload()
    {
        currentHolderAmount = StartingHolderAmount;

        //перезарядка закончилась, обнуляем таймер
        if (Type == WeaponType.Player)
        {
            UIManager.UIManagerInstance.SetReloadTextActve(false);
            UIManager.UIManagerInstance.SetBulletText(currentHolderAmount, StartingHolderAmount, 999);
        }

        ifReloadHolder = false;
        currentHolderReloadTimer = 0f;
        SetReloadText();
    }

    public override void SetWeaponParametres()
    {
        StartingTimerBetweenShoot = 1 / WeaponCooldown;
        ifReloadHolder = false;
        currentTimerBetweenShoot = 0;
        currentHolderReloadTimer = 0f;
        currentHolderAmount = StartingHolderAmount;
        currentBullets = 999;
    }
}
