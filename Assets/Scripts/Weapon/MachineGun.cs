﻿using UnityEngine;
using System.Collections;

public class MachineGun : Weapon
{
    private int musicChecker = 0;
    public override void Shoot()
    {
        if (canFire && !ifReloadHolder && currentHolderAmount > 0)
        {
            //разброс пуль
            Quaternion rotationX = Quaternion.AngleAxis(Random.Range(-Scatter, Scatter), BulletSpawn.up);
            //currentBullets--;
            currentHolderAmount--;
            musicChecker++;
            if (musicChecker == 3)
            {
                musicChecker = 0;
                ShootSound();
            }
            if (Type == WeaponType.Player)
            {
                UIManager.UIManagerInstance.SetBulletText(currentHolderAmount, StartingHolderAmount, currentBullets);
                SetWeaponUIInfo();
                UIManager.UIManagerInstance.SetCameraShootShake();
            }
            //обращение в пул
            currentBullet = BulletPool.Current.GetPooledBullet(BulletPrefab);
            currentBullet.SetBulletParametres(BulletSpawn.position, BulletSpawn.rotation * rotationX, Damage, DistanseToShoot, Type);
            currentBullet.SetVelocity();
            currentBullet.gameObject.SetActive(true);
            currentBullet.GetComponent<TimeDisable>().Init();

            //если пули в обойме закончились, то включаем перезарядку
            if (currentHolderAmount == 0 && currentBullets > 0)
            {
                if (Type == WeaponType.Player)
                    UIManager.UIManagerInstance.SetReloadTextActve(true);
                ifReloadHolder = true;
            }
            else if (currentHolderAmount == 0 && currentBullets == 0 && Type == WeaponType.Player)
            {
                InvertoryController.invertoryController.SetFullWeapon();
                UIManager.UIManagerInstance.GetWeaponUI(ObjectID).SetNoPatronsProgress();
            }
            currentTimerBetweenShoot = StartingTimerBetweenShoot;
            canFire = false;
        }
    }
}


    
