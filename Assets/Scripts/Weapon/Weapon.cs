﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public abstract class Weapon : Item
{
    public enum WeaponType { Player, Enemy, NPC};
    //родительский базовый класс для оружия

    [Header("Weapon Icon")]
    public Sprite WeaponIcon;
    
    [Header ("Weapon Info")]
    public ItemTypes.ItemType IType;
    public int WeaponID;
    public WeaponType Type;
    public int Level;
    public string Description;

    [Header("Weapon Stats")]
    public float WeaponCooldown;
    public float DistanseToShoot;
    public int Damage;
    public float Scatter;
    public int StartingHolderAmount;
    public float HolderReloadTimer;
    protected float StartingTimerBetweenShoot;
    public int StartingBullets;
    public int MaxBullets;

    [Header("Material")]
    public Material NormalMaterial;
    public Material HoverMaterial;

    protected int currentBullets;
    protected float currentTimerBetweenShoot;
    protected float currentHolderReloadTimer;
    protected bool ifReloadHolder = false;
    protected int currentHolderAmount;
    protected bool canFire = true;

    public BaseBullet BulletPrefab;
    protected RaycastHit shootHit;
    public Transform BulletSpawn;
    protected BaseBullet currentBullet;
    protected AudioSource audioSource1;
    protected AudioSource audioSource2;
    protected AudioSource [] audioSources;

    [SerializeField]
    protected bool isDropt=false;

    public virtual void Awake()
    {
        SetWeaponParametres();
        audioSources = GetComponents<AudioSource>();
        
    }
    


    public virtual bool CheckBullets()
    {
        if (currentHolderAmount == StartingHolderAmount)
            return false;
        else return true;
    }

    public virtual void SetWeaponParametres()
    {
        BulletSpawn = GameObject.FindGameObjectWithTag("BulletSpawn").transform;

        StartingTimerBetweenShoot = 1 / WeaponCooldown;
        ifReloadHolder = false;
        currentTimerBetweenShoot = 0;
        currentHolderReloadTimer = 0f;
        currentHolderAmount = 0;
        if (Type == WeaponType.Player && !isDropt)
        {
            currentBullets = InvertoryController.invertoryController.GetAmmoAmount(WeaponID);
            if (currentBullets > StartingHolderAmount)
            {
                currentHolderAmount = StartingHolderAmount;
                currentBullets = StartingHolderAmount;
            }
            else
            {
                currentHolderAmount = currentBullets;
                currentBullets = 0;
            }
        }

    }

    public virtual void SetBulletText()
    {
        if (Type == WeaponType.Player && InvertoryController.invertoryController.CurrentWeapon == this)
            UIManager.UIManagerInstance.SetBulletText(currentHolderAmount, StartingHolderAmount, currentBullets);
    }

    public virtual bool ReloadHolder
    {
        get
        {
            return ifReloadHolder;
        }
        set
        {
            ifReloadHolder = value;
            ReloadSound();
        }
    }

    public virtual void SetReloadText()
    {
        if (Type == WeaponType.Player && InvertoryController.invertoryController.CurrentWeapon == this)
            UIManager.UIManagerInstance.SetRealodProgress(HolderReloadTimer, currentHolderReloadTimer);
    }

    public virtual void Update()
    { 
        if (ifReloadHolder)
        {
            currentHolderReloadTimer += Time.deltaTime;
            SetReloadText();
            if (currentHolderReloadTimer >=HolderReloadTimer)
            {
                Reload();
            }
        }
        if (!canFire)
        {
            currentTimerBetweenShoot -= Time.deltaTime;
            if (currentTimerBetweenShoot < 0)
            {
                canFire = true;
            }

        }
    }

    public virtual void ReloadSound()
    {
        if (audioSources.Length > 0 && audioSources[2]!=null)
        {
            audioSources[2].Play();
        }
    }

    public virtual void ShootSound()
    {
        if (audioSources.Length>0)
        {
            audioSources[0].Play();
            audioSources[1].clip = AudioManager.AudioIntanse.GetBulletDownSound();
            audioSources[1].PlayDelayed(0.5f);
        }
    }

    //стрельба
    public virtual void Shoot()
    {
        if (canFire && !ifReloadHolder && currentHolderAmount > 0)
        {
            //разброс пуль
            Quaternion rotationX = Quaternion.AngleAxis(Random.Range(-Scatter, Scatter), BulletSpawn.up);
            //currentBullets--;
            currentHolderAmount--;
            if (Type == WeaponType.Player)
            {
                SetBulletText();
                SetWeaponUIInfo();
                UIManager.UIManagerInstance.SetCameraShootShake();
            }
            ShootSound();
            //обращение в пул
            currentBullet = BulletPool.Current.GetPooledBullet(BulletPrefab);
            currentBullet.SetBulletParametres(BulletSpawn.position, BulletSpawn.rotation * rotationX, Damage, DistanseToShoot, Type);
            currentBullet.SetVelocity();
            currentBullet.gameObject.SetActive(true);
            currentBullet.GetComponent<TimeDisable>().Init();

            //если пули в обойме закончились, то включаем перезарядку
            if (currentHolderAmount == 0 && currentBullets>0)
            {
                if (Type == WeaponType.Player)
                    UIManager.UIManagerInstance.SetReloadTextActve(true);
                ReloadHolder = true;
            }
            else if(currentHolderAmount == 0 && currentBullets == 0 && Type == WeaponType.Player)
            {
                UIManager.UIManagerInstance.GetWeaponUI(ObjectID).SetNoPatronsProgress();
                InvertoryController.invertoryController.SetFullWeapon();
            }
            currentTimerBetweenShoot = StartingTimerBetweenShoot;
            canFire = false;
        }
    }

    public virtual void CheckNoPatrons()
    {
        if (currentHolderAmount == 0 && currentBullets == 0)
        {
            UIManager.UIManagerInstance.GetWeaponUI(ObjectID).SetNoPatronsProgress();
            InvertoryController.invertoryController.SetFullWeapon();
        }
    }

    //перезарядка обоймы
    public virtual void Reload()
    {
        //если пуль больше, чем нужно обойме, то присваиваем обойме ее максимальное количество пуль
        
        int difference = StartingHolderAmount - currentHolderAmount;
        if (difference < currentBullets)
        {
            currentHolderAmount = StartingHolderAmount;
            InvertoryController.invertoryController.DeleteAmmo(WeaponID, difference);
        }
        else
        {
            currentHolderAmount += currentBullets;
            InvertoryController.invertoryController.DeleteAmmo(WeaponID, currentBullets);
        }
        //в обратном случае присваиваем обойме оставшееся количество пуль
        //else
        //{
        //    currentHolderAmount = currentBullets;
        //    InvertoryController.invertoryController.DeleteAmmo(WeaponID, currentBullets);
        //    //currentBullets = 0;
        //}
        //InvertoryController.invertoryController.DeleteAmmo(WeaponID);

        //перезарядка закончилась, обнуляем таймер
        if (Type == WeaponType.Player)
        {
            UIManager.UIManagerInstance.SetReloadTextActve(false);
            UIManager.UIManagerInstance.SetBulletText(currentHolderAmount, StartingHolderAmount, currentBullets);
        }

        ifReloadHolder = false;
        currentHolderReloadTimer = 0f;
        SetReloadText();
    }
    
    public virtual void AddBullets(int _persentRecovery)
    {
        if (currentBullets < MaxBullets)
        {
            currentBullets += _persentRecovery;
            if (currentBullets > MaxBullets)
            {
                currentBullets = MaxBullets;
            }
            if (currentHolderAmount == 0)
            {
                currentHolderAmount = StartingHolderAmount;
                currentBullets -= StartingHolderAmount;
            }
            if (UIManager.UIManagerInstance.GetWeaponUI(ObjectID)!=null)
                UIManager.UIManagerInstance.GetWeaponUI(ObjectID).SetProgress(currentHolderAmount+currentBullets, MaxBullets);
            if(InvertoryController.invertoryController.CurrentWeapon == this)
                SetBulletText();
        }
    }
    
    public virtual void DropWeapon()
    {
        isDropt = true;
        transform.parent = null;
        transform.localScale = new Vector3(3f, 3f, 3f);
        gameObject.SetActive(true);
        GetComponentInChildren<MeshRenderer>().material = NormalMaterial;
    }

    public virtual void PickUpWeapon()
    {
        isDropt = false;
        GetComponentInChildren<MeshRenderer>().material = NormalMaterial;
        transform.SetParent(InvertoryController.invertoryController.WeaponPosition);
        transform.rotation = new Quaternion(0f,0f,0f,0f);
        transform.localScale = new Vector3(1f, 1f, 1f);
        transform.localPosition = Vector3.zero;
        //transform.localPosition = InvertoryController.invertoryController.WeaponPosition.localPosition;
        InvertoryController.invertoryController.AddWeapon(this);
        CurrentBullets = InvertoryController.invertoryController.GetAmmoAmount(WeaponID);
        
        UIManager.UIManagerInstance.DeactivateHoverInformation();
    }

    
    public virtual void OnTriggerEnter(Collider other)
    {
        if(other.tag == "Player" && isDropt )
        {
            PickUpWeapon();
        }
    }

    private void OnMouseEnter()
    {
        if (isDropt)
        {
            UIManager.UIManagerInstance.SetHoverInformation(ObjectName);
            GetComponentInChildren<MeshRenderer>().material = HoverMaterial;
        }
    }

    private void OnMouseExit()
    {
        if (isDropt)
        {
            UIManager.UIManagerInstance.DeactivateHoverInformation();
            GetComponentInChildren<MeshRenderer>().material = NormalMaterial;
        }
    }

    public virtual void SetWeaponUIInfo()
    {
        if (currentBullets == 0 && currentHolderAmount == 0)
            UIManager.UIManagerInstance.GetWeaponUI(ObjectID).SetNoPatronsProgress();
        else
            UIManager.UIManagerInstance.GetWeaponUI(ObjectID).SetProgress(currentHolderAmount+currentBullets, MaxBullets);
    }

    public virtual int CurrentBullets
    {
        get
        {
            return currentBullets;
        }
        set
        {
            currentBullets = value;
            if (currentHolderAmount == 0)
            {
                if (currentBullets > StartingHolderAmount)
                {
                    currentHolderAmount = StartingHolderAmount;
                    currentBullets -= StartingHolderAmount;
                    InvertoryController.invertoryController.DeleteAmmo(WeaponID, StartingHolderAmount);
                }
                else
                {
                    currentHolderAmount = currentBullets;
                    InvertoryController.invertoryController.DeleteAmmo(WeaponID, currentBullets);
                    currentBullets =0;
                }
            }
            SetWeaponUIInfo();
            SetBulletText();
        }
    }

    public virtual bool IsEmpty()
    {
        if (currentHolderAmount>0)
        {
            return false;
        }
        else return true;
    }

}
