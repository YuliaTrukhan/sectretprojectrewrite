﻿using UnityEngine;
using System.Collections;

public class Shotgun : Weapon {

    public int BulletsPerShot;

    public override void Shoot()
    {
        if (canFire && !ifReloadHolder && currentHolderAmount>0)
        {
            ShootSound();
            //за выстрел пускаем сразу несколько пуль
            for (int i = 0; i < BulletsPerShot; i++)
            {
                //разброс пуль
                //Quaternion rotationX = Quaternion.AngleAxis(Random.Range(-Scatter, Scatter), BulletSpawn.right);
                Quaternion rotationY = Quaternion.AngleAxis(Random.Range(-Scatter, Scatter), BulletSpawn.up);
                
                //currentBullets--;
                currentHolderAmount--;
                if (Type == WeaponType.Player)
                {
                    UIManager.UIManagerInstance.SetBulletText(currentHolderAmount, StartingHolderAmount, currentBullets);
                    SetWeaponUIInfo();
                    UIManager.UIManagerInstance.SetCameraShootShake();
                }
               

                //обращение в пул
                currentBullet = BulletPool.Current.GetPooledBullet(BulletPrefab);
                currentBullet.SetBulletParametres(BulletSpawn.position, BulletSpawn.rotation * rotationY, Damage, DistanseToShoot, Type);
                currentBullet.SetVelocity();
                currentBullet.gameObject.SetActive(true);
                currentBullet.GetComponent<TimeDisable>().Init();

                //если пули в обойме закончились, то включаем перезарядку
                if (currentHolderAmount == 0)
                {
                    if (Type == WeaponType.Player)
                        UIManager.UIManagerInstance.SetReloadTextActve(true);
                    ReloadSound();
                    ifReloadHolder = true;
                }
                else if (currentHolderAmount == 0 && currentBullets == 0 && Type == WeaponType.Player)
                {
                    InvertoryController.invertoryController.SetFullWeapon();
                    UIManager.UIManagerInstance.GetWeaponUI(ObjectID).SetNoPatronsProgress();
                }
            }
            currentTimerBetweenShoot = StartingTimerBetweenShoot;
            canFire = false;
        }
    }
}
