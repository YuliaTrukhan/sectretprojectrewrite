﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class AllInventoryItems
{
    public int ObjectID;
    public int ItemID;
    public int Count;
}

public class ItemsDataBase : MonoBehaviour {
   // public List<Weapon> Weapons = new List<Weapon>();
    public List<Item> Items = new List<Item>();
    public List<AllInventoryItems> Inventory = new List<AllInventoryItems>();
    //здесь хранятся все итемы, которые могут быть в игре
   
}
