﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class InventoryItem{

    public Sprite ObjectIcon; //иконка итема

    public int ObjectID; //уникальный id предмета, не повторяется. 
                        //Различает несколько одинаковых предметов в инвентаре
    public int OwnObjectID; //собственный id предмета
    public ItemTypes.ItemType ObjectType; //тип предмета
    public string ObjectName; //имя предмета
    public int ObjectPrice; //цена предмета
    public bool Stackable; //стакается предмет в инвентаре или нет
    public float DropChance; //шанс дропа 
    public int Count; //количество 

}
