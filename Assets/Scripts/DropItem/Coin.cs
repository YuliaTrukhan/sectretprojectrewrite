﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Coin : MonoBehaviour {

    public float RotationTimer;
    public int AmountRecovery;
    public float Speed;
    private Transform thisTranform;
    private Player player;

    private bool isMoving;
    private bool outEnemy;
    private Vector3 startPosition;
    private Vector3 endPosition;

    public void SetStartParametres(Vector3 _start, Vector3 _end)
    {
        thisTranform.position = _start;
        endPosition = _end;
        outEnemy = true;
    }

    private void Awake()
    {
        thisTranform = transform;
        player = FindObjectOfType<Player>();
        isMoving = false;
    }

    private void Update()
    {
        if (outEnemy)
        {
            thisTranform.position = Vector3.MoveTowards(thisTranform.position, endPosition, Speed * Time.deltaTime);
            if(thisTranform.position == endPosition)
            {
                outEnemy = false;
            }

        }
        if (!isMoving)
        {
            transform.Rotate(Vector3.forward * RotationTimer);
        }
        else
        {
            thisTranform.position = Vector3.MoveTowards(thisTranform.position, player.transform.position, Speed * Time.deltaTime);
        }
       
    }

    private void DestroyCoin()
    {
        gameObject.SetActive(false);
        isMoving = false;
        outEnemy = false;
    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.tag == "Player")
        {
            player.SetMoney(AmountRecovery);
            DestroyCoin();
        }
        if(other.tag == "PickUp")
        {
            //transform(player.transform.position);
            outEnemy = false;
            isMoving = true;
            
        }
    }

  
}
