﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DropItemPool : MonoBehaviour
{
    public static DropItemPool ItemInstanse;

    public Coin [] CoinPrefab;
    private DropItem item;
    private List<DropItem> ItemPool = new List<DropItem>();
    private List<Coin> coinPool = new List<Coin>();

    private void Awake()
    {
        ItemInstanse = this; 
    }
    
    public DropItem Item
    {
        get
        {
            return item;
        }
        set
        {
            item = value;
        }
    }

    public Coin GetPooledCoin()
    {
        Coin tempCoin = CoinPrefab[Random.Range(0, CoinPrefab.Length)];
        foreach (Coin _thisObject in coinPool)
        {
            if ( _thisObject.name == tempCoin.name + "(Clone)" && !_thisObject.gameObject.activeSelf)
            {
                _thisObject.gameObject.SetActive(true);
                return _thisObject;
            }
        }
        Coin _obj = Instantiate(tempCoin);
        coinPool.Add(_obj);
        return _obj;
    }

    public DropItem GetPooledItem(DropItem _item)
    {
        foreach(DropItem _thisObject in ItemPool)
        {
            if(!_thisObject.gameObject.activeSelf && _thisObject.name==_item.name+"(Clone)")
            {
                _thisObject.gameObject.SetActive(true);
                return _thisObject;
            }
        }
        DropItem _obj = Instantiate(_item);
        ItemPool.Add(_obj);
        return _obj;
    }

}
