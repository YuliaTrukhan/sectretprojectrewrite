﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[System.Serializable]
public class Chests {

    public int ID;
    public string Name;
    public int Reward;
    public bool Closed;
    public bool Empty;
}
