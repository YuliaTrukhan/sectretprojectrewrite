﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class DropItem : Item
{
    [Header ("ItemInfo")]
    public Sprite Icon;
    public int ID; //повторяющееся ID
    public int WeaponID;
    public int Price;
    public ItemTypes.ItemType Type;
    public bool OnceDroped;
    public int PercentRecovery;
    public float LifeTime;
    public float Speed;
    public float StartingArmorValue;

    private float currentArmorValue;
    public float CurrentArmorValue
    {
        get
        {
            return currentArmorValue;
        }
        set
        {
            currentArmorValue = value;
        }
    }
    private float currentLifeTime;
    private bool isMoving;
    private bool outEnemy;
    private Transform thisTranform;
    private Vector3 startPosition;
    private Vector3 endPosition;
    private Transform player;

    private void Awake()
    {
        player = FindObjectOfType<Player>().transform;
        currentArmorValue = StartingArmorValue;

    }
    private void Start()
    {
        currentLifeTime = LifeTime;
        thisTranform = transform;
    }

    private void Update()
    {
        currentLifeTime -= Time.deltaTime;
        if (currentLifeTime < 0)
        {
            DestroyItem();
        }

        if (outEnemy)
        {
            thisTranform.position = Vector3.MoveTowards(thisTranform.position, endPosition, Speed * Time.deltaTime);
            if (thisTranform.position == endPosition)
            {
                outEnemy = false;
            }

        }
        if (isMoving)
        {
            thisTranform.position = Vector3.MoveTowards(thisTranform.position, player.position, Speed * Time.deltaTime);
        }
    }

    public void DestroyItem()
    {
        gameObject.SetActive(false);
        currentLifeTime = LifeTime;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "PickUp")
        {
            outEnemy = false;
            isMoving = true;

        }
        if (other.tag == "Player")
        {
            InvertoryController.invertoryController.AddDropItem(this);
            DestroyItem();
        }

    }
    

}
