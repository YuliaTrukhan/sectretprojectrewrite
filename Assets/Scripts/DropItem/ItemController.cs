﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemController : MonoBehaviour {

    public static ItemController itemController;

    public List<DropItem> DropItems = new List<DropItem>();
    public List<Chests> ChestList = new List<Chests>();

    private Player player;
    private Coin tempCoin;

    private void Awake()
    {
        if (itemController == null)
        {
            itemController = this;
        }
        else if (itemController != this)
        {
            Destroy(gameObject);
        }
        DontDestroyOnLoad(gameObject);
        player = FindObjectOfType<Player>();
    }

    public DropItem GetItem(int _itemID)
    {
        for(int i=0; i<DropItems.Count; i++)
        {
            if(DropItems[i].ID == _itemID)
            {
                return DropItems[i];
            }
        }
        return null;
    }




    public Chests GetChest(int _id)
    {
        for(int i=0; i<ChestList.Count; i++)
        {
            if (ChestList[i].ID == _id)
                return ChestList[i];
        }
        return null;
    }

    public DropItem GetAmmoItem(int _weaponID)
    {
        for (int i = 0; i < DropItems.Count; i++)
        {
            if (DropItems[i].WeaponID == _weaponID && DropItems[i].Type == ItemTypes.ItemType.Ammo)
            {
                return DropItems[i];
            }
        }
        return null;
    }
    
    public void CheckDropItem(Vector3 _position, int _itemID, int _maxCoins)
    {
        if (DropItems.Count > 0)
        {
            float chance = Random.Range(0f, 100f);
            //берем случайный дроп из массива
            DropItem tempItem = GetItem(_itemID);
            //проверяем шанс выпадения
            if(tempItem == null)
            {
                return;
            }
            if (tempItem.DropChance >= chance)
            {
                if (tempItem.OnceDroped)
                    tempItem.DropChance = 0;
                DropItem _item = DropItemPool.ItemInstanse.GetPooledItem(tempItem);
                _item.transform.position = new Vector3(_position.x, _item.transform.localScale.y / 2, _position.z);
                
            }
        }

        int coinChance = Random.Range(1, _maxCoins);
        for(int i=0; i<coinChance; i++)
        {
            tempCoin = DropItemPool.ItemInstanse.GetPooledCoin();
            tempCoin.SetStartParametres(_position, new Vector3(Random.Range(_position.x - 5f, _position.x + 5f), 0.3f, Random.Range(_position.z - 5f, _position.z + 5f)));
            //tempCoin.transform.position= new Vector3(Random.Range(_position.x-5f, _position.x + 5f), 0.3f , Random.Range(_position.z - 5f, _position.z + 5f));

        }
    }

    public void GetChestReward(int _chestID)
    {
        player.Money += GetChest(_chestID).Reward;
        UIManager.UIManagerInstance.SetCoins(player.Money);
    }
}
