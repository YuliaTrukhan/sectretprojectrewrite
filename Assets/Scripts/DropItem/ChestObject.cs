﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ChestObject : MonoBehaviour {

    public int ID;
    [Header("UI")]
    public Text Name;
    public Text MessageText;
    [Header("Material")]
    public Material NormalMaterial;
    public Material HoverMaterial;

    private bool inZone;
    private bool isMouseClick;


    private void Start()
    {
        inZone = false;
        Name.text = ItemController.itemController.GetChest(ID).Name.ToUpper();
        SetMessage();
        MessageText.enabled = false;
        isMouseClick = false;
    }

    private void OnMouseEnter()
    {
        SetHoverMaterial();
    }

    private void OnMouseExit()
    {
        SetNormalMaterial();
    }

   

    private void OnMouseDown()
    {
        isMouseClick = true;
        if (inZone && InputManager.InputManagerInstance.PlayerControlType == InputManager.TypeControl.MouseControl)
        {
            Open();
        }
    }

    public void SetNormalMaterial()
    {
        GetComponent<Renderer>().material = NormalMaterial;
    }

    public void SetHoverMaterial()
    {
        GetComponent<Renderer>().material = HoverMaterial;
    }

    private void SetMessage()
    {
        MessageText.enabled = true;
        if (ItemController.itemController.GetChest(ID).Closed)
        {
            MessageText.text = "Закрыто".ToUpper();
        }
        else
        {
            MessageText.text = "Открыто".ToUpper();
        }
    }

    private void Update()
    {
        if(Input.GetKey(KeyCode.E) && inZone)
        {
            Open();
        }
    }


    private void Open()
    {
        if (InvertoryController.invertoryController.DropItemRequest(ID))
        {
            MessageText.text = "Открыто".ToUpper();
            InvertoryUIController.invertoryUIController.DeleteDropItem(ID);
            InvertoryController.invertoryController.RemoveItem(ID);
            ItemController.itemController.GetChest(ID).Closed = false;
            GiveReward();
        }
    }

    public void GiveReward()
    {
        ItemController.itemController.GetChest(ID).Empty = true;
        ItemController.itemController.GetChestReward(ID);
    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.tag == "Player")
        {
            SetMessage();
            inZone = true;
            if (isMouseClick)
            {
                Open();
                isMouseClick = false;
            }
        }
        
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.tag == "Player")
        {
            MessageText.enabled = false;
            inZone = false;
        }
        
    }
}
