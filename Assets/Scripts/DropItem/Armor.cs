﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Armor{

    public int ObjectID;
    public int ID;
    public float ArmorAmount;
    private float currentArmorAmount;

    public float CurrentArmorValue
    {
        get
        {
            return currentArmorAmount;
        }
        set
        {
            currentArmorAmount = value;
        }
    }
}
