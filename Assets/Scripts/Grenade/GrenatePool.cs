﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GrenatePool : MonoBehaviour {

    public static GrenatePool Instanse;

    private List<Grenade> objectsPool;
    private Grenade thisObject;

    private void Awake()
    {
        Instanse = this;
        objectsPool = new List<Grenade>();
    }

    public Grenade GetPooledObject(Grenade _object)
    {
        foreach (Grenade _thisObject in objectsPool)
        {
            if (!_thisObject.gameObject.activeSelf && _thisObject.name == _object.name + "(Clone)")
            {
                return _thisObject;
            }
        }
        Grenade _obj = Instantiate(_object);
        objectsPool.Add(_obj);
        return _obj;
    }
}
