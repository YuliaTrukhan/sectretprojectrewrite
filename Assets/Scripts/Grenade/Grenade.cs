﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Grenade : Item
{
    public Sprite Icon;
    public int ItemID;
    public int Price;
    public ItemTypes.ItemType Type;
    public float ShotRadius;
    public float ExplosoinRadius;
    public int Damage;
    private Player player;
    private bool isMoving = false;
    private Vector3 target;

    private void Awake ()
    {
        player = FindObjectOfType<Player>();
    }



    public void SetForse(Vector3 _position)
    {
        target = _position;
        isMoving = true;
        
    }
    private void Update()
    {
        if (isMoving)
        {
            transform.position = Vector3.MoveTowards(transform.position, target, 12f * Time.deltaTime);
        }
    }

    private void GrenadeDamage()
    {
        Enemy enemy;
        int count = EnemyManager.EnemyInstance.GetCountOfEnemies();
        //при соприкосновении находит врагов на дистанции взрыва и наносит им урон
        for (int i = 0; i < count; i++)
        {
            enemy = EnemyManager.EnemyInstance.GetEnemy(i);
            if (Vector3.Distance(enemy.transform.position, transform.position) <= ExplosoinRadius)
            {
                enemy.TakeDamage(Damage);
            }
        }
        //если игрок оказался в пределах радиуса взрыва, ему тоже наносится урон
        if (Vector3.Distance(player.transform.position, transform.position) <= ExplosoinRadius)
        {
            player.TakeDamage(Damage);
        }
        
    }

    private void OnCollisionEnter(Collision collision)
    {
        AudioManager.AudioIntanse.GetGrenadeExplosionSound(transform.position);
        GrenadeDamage();
        MakeExplosionFX();
        DestroyGrenade();
    }

    //уничтожение объекта
    public void DestroyGrenade()
    {
        UIManager.UIManagerInstance.GrenadeUI.SetActive(false);
        gameObject.SetActive(false);
        isMoving = false;
    }

    private void MakeExplosionFX()
    {
        GameObject exp = FXManager.Instance.GetExplosion();
        exp.SetActive(true);
        exp.transform.position = transform.position;
        exp.GetComponent<TimeDisable>().Init();
        exp.GetComponent<Explosion>().Explode();
    }
}
