﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class QuestTrigger : MonoBehaviour {

    public int QuestID;
    private NPC npc;

    private void OnTriggerEnter(Collider other)
    {
        if(other.tag == "NPC")
        {
            npc = other.GetComponent<NPC>();
            if (npc != null)
            {
                npc.CurrentState = NPC.State.Stay;
                QuestsController.questController.CheckProgress(QuestID);
            }
        }
    }
}
