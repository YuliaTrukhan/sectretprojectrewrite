﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class QuestButton : MonoBehaviour {

    //сущеность кнопки, по нажатию которой в окне квеста нпс отображается информация квеста
    public int QuestID;
    public Text QuestName;
    

    public void ShowQuestInfo()
    {
        QuestUIManager.uIManager.AcceptButton.gameObject.SetActive(false);
        QuestUIManager.uIManager.GiveUpButton.gameObject.SetActive(false);
        QuestUIManager.uIManager.CompleteButton.gameObject.SetActive(false);

        QuestUIManager.uIManager.AcceptButton.QuestID = QuestID;
        QuestUIManager.uIManager.GiveUpButton.QuestID = QuestID;
        QuestUIManager.uIManager.CompleteButton.QuestID = QuestID;

        QuestUIManager.uIManager.ShowSelectedQuest(QuestID);
        

        if (QuestsController.questController.RequestAviableQuest(QuestID))
            QuestUIManager.uIManager.AcceptButton.gameObject.SetActive(true);
        else
            QuestUIManager.uIManager.AcceptButton.gameObject.SetActive(false);

        if (QuestsController.questController.RequestInProggreseQuest(QuestID))
            QuestUIManager.uIManager.GiveUpButton.gameObject.SetActive(true);
        else
            QuestUIManager.uIManager.GiveUpButton.gameObject.SetActive(false);

        if (QuestsController.questController.RequestCompletedQuest(QuestID))
            QuestUIManager.uIManager.CompleteButton.gameObject.SetActive(true);
        else
            QuestUIManager.uIManager.CompleteButton.gameObject.SetActive(false);
    }

    public void StartQuest()
    {
        QuestsController.questController.StartQuest(QuestID);
        QuestUIManager.uIManager.HideQuestPanel();
        QuestUIManager.uIManager.SetQuestNotification(QuestID);
    }

    public void GiveUPQuest()
    {
        QuestsController.questController.GiveUpQuest(QuestID);
        QuestUIManager.uIManager.HideQuestPanel();
    }
    
    public void CompleteQuest()
    {
        QuestsController.questController.CompleteQuest(QuestID);
        QuestUIManager.uIManager.HideQuestPanel();
    }

    public void ClosePanel()
    {
        QuestUIManager.uIManager.HideQuestPanel();
    }
    
}
