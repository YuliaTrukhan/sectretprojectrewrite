﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.UI;

public class NPC : MonoBehaviour {

    public enum State
    {
        Stay,
        Chase
    };

    public enum TypeOfNPC
    {
        Quest,
        Shop
    };


    public int Health;
    private int currentHealth;
    public Slider HealthBar;
    public Text TextName;

    public string NpcName;
    public int NpcID;
    public List<int> AvailableQuestID = new List<int>(); //квесты, которые можно взять у нпс
    public List<int> ReceivableQuestID = new List<int>(); //квесты, которые нпс принимает после выполнения

    public GameObject IconQuestAviable;
    public GameObject IconQuestComplete;
    public GameObject IconShop;

    public State CurrentState;
    public TypeOfNPC NPCType;
    
    private Player player;
    private NavMeshAgent agent;
    public Vector3 StartPosition;

    [Header ("Chasing")]
    private Enemy closestEnemy;
    public float DistanceToLook;
    public Weapon weapon;
    private RaycastHit hitToCheck;

    [Header("Material")]
    public Material NormalMaterial;
    public Material HoverMaterial;

    private bool inZone;

    private void OnMouseEnter()
    {
        SetHoverMaterial();
        UIManager.UIManagerInstance.SetHoverInformation(NpcName);
    }

    private void OnMouseExit()
    {
        SetNormalMaterial();
        UIManager.UIManagerInstance.DeactivateHoverInformation();
    }

    private void OnMouseDown()
    {
        InputManager.InputManagerInstance.tempNPC = this;
        if (inZone && InputManager.InputManagerInstance.PlayerControlType == InputManager.TypeControl.MouseControl && !UIManager.UIManagerInstance.SelectInterface)
        {
            OpenWindow();
        }
    }
  

    public void SetNormalMaterial()
    {
        GetComponent<Renderer>().material = NormalMaterial;
    }

    public void SetHoverMaterial()
    {
        GetComponent<Renderer>().material = HoverMaterial;
    }

    private void Awake()
    {
        CurrentState = State.Stay;
        player = FindObjectOfType<Player>();
        agent = GetComponent<NavMeshAgent>();
        StartPosition = transform.position;
        currentHealth = Health;
        SetHealthBar();
        TextName.text = NpcName.ToUpper();
        //ChangeSprite(true);
    }

    private void Start()
    {
        ShowQuestIcon();
        QuestsController.questController.AddNPCList(this);
        if(NPCType == TypeOfNPC.Shop)
        {
            IconShop.SetActive(true);
        }
        else
        {
            IconShop.SetActive(false);
        }
    }

    public void ReturnToStart()
    {
        CurrentState = State.Stay;
        agent.enabled = false;
        transform.position = StartPosition;
    }

    public void TakeDamage(int _damage)
    {
        if (CurrentState == State.Chase)
        {
            currentHealth -= _damage;
            if (currentHealth <= 0)
            {
                Die();
            }
        }
    }



    private void Die()
    {
        transform.position = StartPosition;
        CurrentState = State.Stay;
        for(int i=0; i<ReceivableQuestID.Count; i++)
        {
            if(QuestsController.questController.GetQuest(ReceivableQuestID[i]).QuestType == BaseQuest.TypeOfQuest.Escort)
            {
                QuestsController.questController.GetQuest(ReceivableQuestID[i]).GiveUp();
            }
        }
    }

    //установка полоски жизни
    private void SetHealthBar()
    {
        HealthBar.value = (float)currentHealth / (float)Health;
    }
    
    public void OpenWindow()
    {
        if (NPCType == TypeOfNPC.Quest)
        {
            QuestUIManager.uIManager.QuestPanelActive = !QuestUIManager.uIManager.QuestPanelActive;
            QuestUIManager.uIManager.ShowQuestPanel(this);
            //InputManager.InputManagerInstance.InteructNpcClick = false;
            
        }
        else if (NPCType == TypeOfNPC.Shop)
        {
            ShopController.Controller.IsPanelActive = !ShopController.Controller.IsPanelActive;
            ShopController.Controller.OpenShopWindow();
            //InputManager.InputManagerInstance.InteructNpcClick = false;
        }
        //ChangeSprite(true);
    }
    
    public void SetQuestEscortSettings()
    {
        agent.enabled = true;
        CurrentState = State.Chase;
    }

    private void Update()
    {
        if (CurrentState == State.Chase)
        {
            agent.destination = player.transform.position;
            transform.LookAt(player.transform.position);

            if (EnemyManager.EnemyInstance.GetCountOfEnemies() > 0 && Vector3.Distance(transform.position, FindClosestEnemy().position) <= DistanceToLook)
            {
                transform.LookAt(new Vector3(FindClosestEnemy().position.x, transform.position.y, FindClosestEnemy().position.z));
                weapon.transform.LookAt(FindClosestEnemy().position);
                //Physics.Raycast(InvertoryController.invertoryController.CurrentWeapon.BulletSpawn.position, InvertoryController.invertoryController.CurrentWeapon.BulletSpawn.forward, out hitToCheck);
                //стреляем если между игроком и целью нет препятствий
                //if (hitToCheck.collider.tag != "obstacle" && Vector3.Distance(InvertoryController.invertoryController.CurrentWeapon.BulletSpawn.position, FindClosestEnemy().position) <= InvertoryController.invertoryController.CurrentWeapon.DistanseToShoot)
                    weapon.Shoot();
            }
        }
    }

    //метод отвечающий за переключение иконок
    public void ShowQuestIcon()
    {
        IconQuestComplete.SetActive(false);
        IconQuestAviable.SetActive(false);
       
        if (QuestsController.questController.CheckCompleteQuest(this))
            IconQuestComplete.SetActive(true);
        else if (QuestsController.questController.CheckAvailableQuest(this))
            IconQuestAviable.SetActive(true);

    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            if (InputManager.InputManagerInstance.PlayerControlType == InputManager.TypeControl.KeyboardControl)
            {
                InputManager.InputManagerInstance.tempNPC = this;
            }
            else if (InputManager.InputManagerInstance.PlayerControlType == InputManager.TypeControl.MouseControl && InputManager.InputManagerInstance.tempNPC==this)
            {
                OpenWindow();
            }
            inZone = true;
        }
    }
    
    private void OnTriggerExit(Collider other)
    {
        if (other.tag == "Player")
        {
            if (InputManager.InputManagerInstance.tempNPC == this)
                InputManager.InputManagerInstance.tempNPC = null;
            inZone = false;
        }
    }

    //возвращает ближайшего врага
    private Transform FindClosestEnemy()
    {
        closestEnemy = EnemyManager.EnemyInstance.GetEnemy(0);
        Enemy enemy;
        for (int i = 0; i < EnemyManager.EnemyInstance.GetCountOfEnemies(); i++)
        {
            enemy = EnemyManager.EnemyInstance.GetEnemy(i);
            if (Vector3.Distance(transform.position, enemy.transform.position) < Vector3.Distance(transform.position, closestEnemy.transform.position))
            {
                closestEnemy = enemy;
            }
        }
        return closestEnemy.transform;
    }
}
