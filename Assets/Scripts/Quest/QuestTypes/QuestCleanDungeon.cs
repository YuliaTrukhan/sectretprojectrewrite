﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class QuestCleanDungeon : BaseQuest {

    public int DungeonID;
    public int StartingObjectiveCount { get; set; }
    private int currentObjectiveCount;

    public string DungeonName { get; set; }

    public override void Start()
    {
        DungeonName = DungeonController.dungeonController.GetDungeon(DungeonID).DungeonName;
        StartingObjectiveCount = DungeonController.dungeonController.GetDungeon(DungeonID).EnemiesCount;
        currentObjectiveCount = 0;
        SetQuestNotification();
        QuestType = TypeOfQuest.CleanDungeon;

    }
    

    public override void SetQuestNotification()
    {
        PointText = "Зачистить подземелье: " + DungeonName;
        RewardText = "Награда: " + Reward;
        NotificationText = "Зачистить подземелье: " + currentObjectiveCount + "/" + StartingObjectiveCount;
    }

    public override void GiveUp()
    {
        currentObjectiveCount = 0;
    }

    public override void CheckProgress(int _ID)
    {
        if (_ID == DungeonID)
        {
            currentObjectiveCount++;
            if (currentObjectiveCount == StartingObjectiveCount)
            {
                Progress = QuestProgress.Complete;
                QuestsController.questController.ChangeNPCIcon();
                QuestUIManager.uIManager.SetNewChangeIcon();
            }
            SetQuestNotification();
            QuestUIManager.uIManager.UpdateQuestNotification(QuestID);
        }
    }

}
