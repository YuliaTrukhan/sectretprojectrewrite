﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class QuestTalk : BaseQuest {

    public int NPCID;
    private bool checkInicialization = false;



    private void Update()
    {
        if (!checkInicialization)
        {
            if (QuestsController.questController.GetNPC(NPCID) != null)
            {
                SetQuestNotification();
                checkInicialization = true;
            }
        }
    }
    public override void SetQuestNotification()
    {
        PointText = "Поговорить с: " + QuestsController.questController.GetNPC(NPCID).NpcName;
        RewardText = "Награда: " + Reward;
        NotificationText = "Поговорить с: " + QuestsController.questController.GetNPC(NPCID).NpcName;
    }

    public override void StartQuest()
    {
        Progress = QuestProgress.Complete;
        QuestsController.questController.ChangeNPCIcon();
        QuestUIManager.uIManager.SetNewChangeIcon();
    }
    

}
