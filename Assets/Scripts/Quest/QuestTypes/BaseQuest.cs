﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BaseQuest:MonoBehaviour {

    public enum TypeOfQuest { CleanDungeon, ToKill, TalkToNPC, Save, Escort, FindSomething }
    public enum QuestProgress { NotAvailable, Available, InProgress, Complete, Done }

    public int QuestID;
    public string Title;
    public int NextQuestID;
    public QuestProgress Progress;
    public TypeOfQuest QuestType;
    public int Reward;

    [Multiline]
    public string QuestDescriptionText;
    [Multiline]
    public string QuestCompleteText;

    public string PointText;
    public string RewardText;
    public string NotificationText;


    public virtual void Start()
    {

    }
    
    public virtual void SetQuestNotification()
    {

    }

    public virtual void StartQuest()
    {
        Progress = QuestProgress.InProgress;
    }

    public virtual void GiveUp()
    {
        Progress = QuestProgress.Available;
    }

    public virtual void Complete()
    {
        Progress = QuestProgress.Done;

    }

    public virtual void CheckProgress(int _ID)
    {
        
    }

}
