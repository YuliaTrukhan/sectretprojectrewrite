﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class QuestItems
{
    public int ID;
    public string Name;
    public int ObjectiveCount;
    public int CurrentCount;
    public bool Done=false;

    public void SetItemsCount()
    {
        CurrentCount++;
        if(CurrentCount == ObjectiveCount)
        {
            Done = true;
        }
    }
}

public class QuestFindItem : BaseQuest {

    public List<QuestItems> Items = new List<QuestItems>();
    private int objectiveCount;
    private EnemyColors enemyColors;

    public override void Start()
    {
        enemyColors = FindObjectOfType<EnemyColors>();
        objectiveCount = 0;
        SetQuestNotification();
        QuestType = TypeOfQuest.FindSomething;
    }

    public override void SetQuestNotification()
    {
        PointText = "Найти предметы: ";
        for (int i = 0; i < Items.Count; i++)
        {
            PointText += Items[i].Name + " у " + enemyColors.colorName[Items[i].ID] + " чуваков. ";
        }

        RewardText = "Награда: " + Reward;
        NotificationText = "Найти: ";
        for (int i = 0; i < Items.Count; i++)
        {
            NotificationText += Items[i].Name + ": " + Items[i].CurrentCount + "/" + Items[i].ObjectiveCount + ". ";
        }
    }

    public override void CheckProgress(int _ID)
    {
        objectiveCount = 0;
        for (int i = 0; i < Items.Count; i++)
        {
            if (_ID == Items[i].ID && Items[i].CurrentCount < Items[i].ObjectiveCount)
            {
                Items[i].SetItemsCount();
            }
        }

        for (int i = 0; i < Items.Count; i++)
        {
            if (Items[i].Done)
            {
                objectiveCount++;
            }
            if (objectiveCount == Items.Count)
            {
                Progress = QuestProgress.Complete;
                QuestUIManager.uIManager.SetNewChangeIcon();
                QuestsController.questController.ChangeNPCIcon();
            }
        }
        SetQuestNotification();
        QuestUIManager.uIManager.UpdateQuestNotification(QuestID);
        
    }

    public override void GiveUp()
    {
        base.GiveUp();
    }

    public override void Complete()
    {
        base.Complete();
        
    }

}
