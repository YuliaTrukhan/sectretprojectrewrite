﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class QuestEscort : BaseQuest {

    private bool checkInicialization = false;

    public override void Start()
    {
        QuestType = TypeOfQuest.Escort;
    }

    private void Update()
    {
        if (!checkInicialization)
        {
            if (QuestsController.questController.GetReceivableQuestNPC(QuestID) != null)
            {
                SetQuestNotification();
                checkInicialization = true;
            }
        }
    }

    public override void StartQuest()
    {
        base.StartQuest();
        QuestsController.questController.GetAvailableQuestNPC(QuestID).SetQuestEscortSettings();
    }

    public override void GiveUp()
    {
        QuestsController.questController.GetAvailableQuestNPC(QuestID).ReturnToStart();

    }

    public override void Complete()
    {
        QuestsController.questController.GetAvailableQuestNPC(QuestID).ReturnToStart();
    }

    public override void CheckProgress(int _ID)
    {
        if(_ID == QuestID)
        {
            Progress = QuestProgress.Complete;
            QuestsController.questController.ChangeNPCIcon();
            QuestUIManager.uIManager.SetNewChangeIcon();
        }
    }



    public override void SetQuestNotification()
    {
        PointText = "Провести: " + QuestsController.questController.GetReceivableQuestNPC(QuestID).NpcName;
        RewardText = "Награда: " + Reward;
        NotificationText = "Провести: " + QuestsController.questController.GetReceivableQuestNPC(QuestID).NpcName;
    }
}
