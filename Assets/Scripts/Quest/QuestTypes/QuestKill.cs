﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class QuestKill : BaseQuest {

    public int EnemyID;
    public int ObjectiveCount;
    private int currentObjectiveCount;
    private EnemyColors enemyColors;

    public override void Start()
    {
        enemyColors = FindObjectOfType<EnemyColors>();
        currentObjectiveCount = 0;
        SetQuestNotification();
        QuestType = TypeOfQuest.ToKill;
    }

    public override void CheckProgress(int _ID)
    {
        if (_ID == EnemyID)
        {
            currentObjectiveCount++;
            if (currentObjectiveCount == ObjectiveCount)
            {
                Progress = QuestProgress.Complete;
                QuestsController.questController.ChangeNPCIcon();
                QuestUIManager.uIManager.SetNewChangeIcon();
            }
            SetQuestNotification();
            QuestUIManager.uIManager.UpdateQuestNotification(QuestID);
        }
    }

    public override void SetQuestNotification()
    {
        if (EnemyID == -1)
        {
            PointText = "Убить: " + ObjectiveCount + "чуваков. У них БЕЛЫЙ цвет и они бродят по всей карте.";
        }
        else
        {
            PointText = "Убить: " + ObjectiveCount + "чуваков. У них " + enemyColors.colorName[EnemyID] + " цвет.";
        }
        RewardText = "Награда: " + Reward;
        NotificationText = "Убить: " + currentObjectiveCount + "/" + ObjectiveCount;
    }

    public override void GiveUp()
    {
        currentObjectiveCount = 0;
    }

    public override void Complete()
    {
        currentObjectiveCount = 0;
    }
    
}
