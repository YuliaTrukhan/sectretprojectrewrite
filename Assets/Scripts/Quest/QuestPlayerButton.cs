﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class QuestPlayerButton : MonoBehaviour
{

    public int QuestID;
    public Text QuestName;
    
    private void SetButtonsActive(bool _isActive)
    {
        QuestUIManager.uIManager.OpenNotification.gameObject.SetActive(_isActive);
        QuestUIManager.uIManager.CloseNotification.gameObject.SetActive(_isActive);
        QuestUIManager.uIManager.GiveUpLogButton.gameObject.SetActive(_isActive);
    }
    public void ShowQuestInfo()
    {
        SetButtonsActive(false);
        QuestUIManager.uIManager.ShowSelectedPlayerQuest(QuestID);

        if (QuestsController.questController.RequestInProggreseQuest(QuestID) || QuestsController.questController.RequestCompletedQuest(QuestID))
            SetButtonsActive(true);
    }

    public void GiveUp()
    {
        QuestsController.questController.GiveUpQuest(QuestID);
        QuestUIManager.uIManager.HideQuestPlayerPanel();
    }

    public void ClosePanel()
    {
        QuestUIManager.uIManager.HideQuestPlayerPanel();
    }

    public void ShowNotification()
    {
        QuestUIManager.uIManager.ActivateQuestNotification(QuestID, true);
    }

    public void CloseNotification()
    {
        QuestUIManager.uIManager.ActivateQuestNotification(QuestID, false);
    }
}
