﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class QuestNotification : MonoBehaviour {

    public int QuestID;
    public Text QuestName;
    public Text QuestPoint;
    public bool isActive;
}
