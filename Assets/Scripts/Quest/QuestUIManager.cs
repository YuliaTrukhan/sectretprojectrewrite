﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class QuestUIManager : BaseUIManager
{

    public static QuestUIManager uIManager;

    public bool QuestPanelActive = false;
    public bool QuestPlayerPanelActive = false;

    public GameObject QuestPanel;
    public GameObject QuestPlayerPanel;
    private List<BaseQuest> NPCQuestList = new List<BaseQuest>();

    public QuestButton qButton;
    public QuestPlayerButton qPlayerButton;
    private List<GameObject> questButtons = new List<GameObject>();

    public QuestButton AcceptButton;
    public QuestButton GiveUpButton;
    public QuestButton CompleteButton;
    public QuestPlayerButton OpenNotification;
    public QuestPlayerButton CloseNotification;
    public QuestPlayerButton GiveUpLogButton;

    public Transform ButtonSpacer1;
    public Transform ButtonSpacer2;
    public Transform PlayerButtonSpacer;
    public Transform QuestSpacer;

    private List<QuestNotification> questNotifications = new List<QuestNotification>();
    private List<QuestNotification> PoolQuestTables = new List<QuestNotification>();
    public QuestNotification QuestNotification;

    public Text QuestTitle;
    public Text QuestDescription;
    public Text QuestPoint;
    public Text QuestReward;
    public Text QuestPlayerTitle;
    public Text QuestPlayerDescription;
    public Text QuestPlayerPoint;
    public Text QuestPlayerReward;


   public override void Awake()
    {
        base.Awake();
        if (uIManager == null)
            uIManager = this;
        else if (uIManager != this)
            Destroy(gameObject);
        DontDestroyOnLoad(gameObject);
    }


    private void Start()
    {
        HideQuestPanel();
    }
    
    //установка окна уведомления о квесте
    public void ActivateQuestNotification(int _questID, bool _isActive)
    {
        for (int i = 0; i < questNotifications.Count; i++)
            if (questNotifications[i].QuestID == _questID)
            {
                questNotifications[i].gameObject.SetActive(_isActive);
                questNotifications[i].isActive = _isActive;
            }
    }

   // удаление уведомления о квесте по его завершению
    public void DestroyQuestNotification(int _questID)
    {
        for (int i = 0; i < questNotifications.Count; i++)
            if (questNotifications[i].QuestID == _questID)
            {
                //Destroy(questNotifications[i].gameObject);
                PoolQuestTables.Add(questNotifications[i]);
                questNotifications[i].gameObject.SetActive(false);
                questNotifications.Remove(questNotifications[i]);
            }
    }

    public void AddToNPCQuestList(BaseQuest _quest)
    {
        NPCQuestList.Add(_quest);
    }

    //методя для изменения данных в уведомлении о квесте
    public void UpdateQuestNotification(int _questID)
    {
        for (int j = 0; j < questNotifications.Count; j++)
        {
            if (_questID == questNotifications[j].QuestID)
            {
                questNotifications[j].QuestName.text = QuestsController.questController.GetQuest(_questID).Title;
                questNotifications[j].QuestPoint.text = QuestsController.questController.GetQuest(_questID).NotificationText;
            }
        }
    }

    //для пула уведомлений о квесте
    private QuestNotification GetQuestTable()
    {
        for (int i = 0; i < PoolQuestTables.Count; i++)
        {
            if (!PoolQuestTables[i].gameObject.activeSelf)
            {
                return PoolQuestTables[i];
            }
        }

        QuestNotification _obj = Instantiate(QuestNotification);
        _obj.gameObject.SetActive(false);
        return _obj;
    }

    //уведомление о квесте
    public void SetQuestNotification(int _questID)
    {
        QuestNotification _questT = GetQuestTable();
        _questT.transform.SetParent(QuestSpacer, false);
        _questT.QuestID = _questID;
        _questT.isActive = true;
        _questT.gameObject.SetActive(true);
        questNotifications.Add(_questT);
        UpdateQuestNotification(_questID);
    }

    public override void ClosePanel()
    {
        base.ClosePanel();
        if (QuestPanelActive)
        {
            HideQuestPanel();
        }
        else if (QuestPlayerPanelActive)
        {
            HideQuestPlayerPanel();
        }
    }

    public void ShowQuestPanel(NPC _npcObject)
    {
       
        if (QuestPlayerPanelActive)
            HideQuestPlayerPanel();
        if (!QuestPanelActive)
        {
            HideQuestPanel();
        }
        else
        {
            CanavasController.canvasController.UIWindowName.text = WindowName.ToUpper();
            //WindowIconButton.SetHover();
            CanavasController.canvasController.SetCanvasSortOrder(CanvasID);
            QuestPanel.SetActive(QuestPanelActive);
            QuestsController.questController.QuestRequest(_npcObject);
            // установить все квесты
            FillQuestButtons();
        }
    }

    public void SetNewChangeIcon()
    {
        WindowIconButton.SetHover();
    }

    //показать окно квеста от игрока
    public void ShowQuestPlayerPanel()
    {
        if(QuestPanelActive)
            HideQuestPanel();
        if (!QuestPlayerPanelActive)
        {
            HideQuestPlayerPanel();
        }
        else if (QuestPlayerPanelActive && !QuestPanelActive)
        {
            WindowIconButton.SetNormal();
            CanavasController.canvasController.UIWindowName.text = WindowName.ToUpper();
            //WindowIconButton.SetHover();
            CanavasController.canvasController.SetCanvasSortOrder(CanvasID);
            QuestPlayerPanel.SetActive(QuestPlayerPanelActive);
            for (int i = 0; i < QuestsController.questController.GetCurrentQuestCount(); i++)
            {
                //создаем кнопку
                QuestPlayerButton qButton = Instantiate(qPlayerButton);
                BaseQuest _quest = QuestsController.questController.GetCurrentQuest(i);
                //записываем в нее id и имя квеста
                qButton.QuestID = _quest.QuestID;
                qButton.QuestName.text = _quest.Title;
                qButton.transform.SetParent(PlayerButtonSpacer, false);
                questButtons.Add(qButton.gameObject);
            }
        }
    }

   // информация о квесте в окне квестов игрока
    public void ShowSelectedPlayerQuest(int _questID)
    {
        OpenNotification.QuestID = _questID;
        CloseNotification.QuestID = _questID;
        GiveUpLogButton.QuestID = _questID;

        BaseQuest _tempQuest;
        //берем все текущие квесты
        for (int i = 0; i < QuestsController.questController.GetCurrentQuestCount(); i++)
        {
            _tempQuest = QuestsController.questController.GetCurrentQuest(i);
           // находим нужный по id
            if (QuestsController.questController.GetCurrentQuest(i).QuestID == _questID)
            {
               // записываем информацию
                QuestPlayerTitle.text = _tempQuest.Title;
               // в зависимости от прогресса квеста записываем в описание соответствующие сообщения
                if (_tempQuest.Progress == BaseQuest.QuestProgress.InProgress)
                {
                    QuestPlayerDescription.text = _tempQuest.QuestDescriptionText;
                }
                else if (_tempQuest.Progress == BaseQuest.QuestProgress.Complete)
                {
                    QuestPlayerDescription.text = _tempQuest.QuestCompleteText;
                }

                QuestPlayerPoint.text = _tempQuest.PointText;
                QuestPlayerReward.text = _tempQuest.RewardText;

            }
        }
    }

    //установка квестов дя окна квестов нпс
    private void FillQuestButtons()
    {
        //устанавливаем кнопки для квестов
        foreach (BaseQuest _quest in NPCQuestList)
        {
            QuestButton _qButton = Instantiate(qButton);
            _qButton.QuestID = _quest.QuestID;
            _qButton.QuestName.text = _quest.Title;

            if (_quest.Progress == BaseQuest.QuestProgress.Available)
            {
                _qButton.transform.SetParent(ButtonSpacer1, false);
            }
            else if (_quest.Progress == BaseQuest.QuestProgress.InProgress || _quest.Progress == BaseQuest.QuestProgress.Complete)
            {
                _qButton.transform.SetParent(ButtonSpacer2, false);
            }

            questButtons.Add(_qButton.gameObject);
        }
    }

    //информация о конкретном квесте в окне нпс
    public void ShowSelectedQuest(int _questID)
    {
        //находим необходимый квест у нпс и записываем его информацию в окно
        for (int i = 0; i < NPCQuestList.Count; i++)
        {
            if (NPCQuestList[i].QuestID == _questID)
            {
                QuestTitle.text = NPCQuestList[i].Title;
                if (NPCQuestList[i].Progress == BaseQuest.QuestProgress.InProgress || NPCQuestList[i].Progress == BaseQuest.QuestProgress.Available)
                {
                    QuestDescription.text = NPCQuestList[i].QuestDescriptionText;
                }
                else if (NPCQuestList[i].Progress == BaseQuest.QuestProgress.Complete)
                {
                    QuestDescription.text = NPCQuestList[i].QuestCompleteText;
                }
                QuestPoint.text = NPCQuestList[i].PointText;
                QuestReward.text = NPCQuestList[i].RewardText;
            }
        }
    }

   // закрыть окно квеста для игрока
    public void HideQuestPlayerPanel()
    {
        SetSortOrder(0);
        CanavasController.canvasController.SetWindowName();
        //WindowIconButton.SetNormal();
        CanavasController.canvasController.SortOrder--;
        CanavasController.canvasController.CheckSortOrder();
        QuestPlayerPanelActive = false;

        QuestPlayerTitle.text = "";
        QuestPlayerPoint.text = "";
        QuestPlayerDescription.text = "";
        QuestPlayerReward.text = "";

        OpenNotification.gameObject.SetActive(false);
        CloseNotification.gameObject.SetActive(false);
        GiveUpLogButton.gameObject.SetActive(false);

        for (int i = 0; i < questButtons.Count; i++)
        {
            Destroy(questButtons[i]);
        }

        questButtons.Clear();
        QuestPlayerPanel.SetActive(false);
    }

    //закрыть окно квеста для нпс
    public void HideQuestPanel()
    {
        CanavasController.canvasController.SetWindowName();
        //WindowIconButton.SetNormal();
        SetSortOrder(0);
        CanavasController.canvasController.SortOrder--;
        CanavasController.canvasController.CheckSortOrder();

        QuestPanelActive = false;

        QuestTitle.text = "";
        QuestDescription.text = "";
        QuestPoint.text = "";
        QuestReward.text = "";

        NPCQuestList.Clear();
        for (int i = 0; i < questButtons.Count; i++)
        {
            Destroy(questButtons[i]);
        }

        questButtons.Clear();

        AcceptButton.gameObject.SetActive(false);
        GiveUpButton.gameObject.SetActive(false);
        CompleteButton.gameObject.SetActive(false);
        QuestPanel.SetActive(false);
    }

}
