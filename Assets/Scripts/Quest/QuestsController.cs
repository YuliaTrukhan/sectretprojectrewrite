﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class QuestsController : MonoBehaviour {

    public static QuestsController questController;

    public List<BaseQuest> AllQuests = new List<BaseQuest>();

    private List<BaseQuest> currentQuests = new List<BaseQuest>();
    private List<NPC> npcList = new List<NPC>();
    private Player player;
    private int indexNPC;

    private void Awake()
    {
        if (questController == null)
        {
            questController = this;
        }
        else if (questController != this)
        {
            Destroy(gameObject);
        }
        DontDestroyOnLoad(gameObject);
        player = FindObjectOfType<Player>();
    }


    public int GetNPCCount()
    {
        return npcList.Count;
    }

    public int GetCurrentQuestCount()
    {
        return currentQuests.Count;
    }

    public void AddNPCList(NPC _npc)
    {
        npcList.Add(_npc);
    }
    
    public void ClearNPC()
    {
        npcList.Clear();
    }

    public void CheckProgress(int _ID)
    {
        for(int i=0; i<currentQuests.Count; i++)
        {
            currentQuests[i].CheckProgress(_ID);
        }
    }

    public void StartQuest(int _questID)
    {
        for (int i = 0; i < AllQuests.Count; i++)
        {
            if (AllQuests[i].QuestID == _questID && AllQuests[i].Progress == BaseQuest.QuestProgress.Available)
            {
                currentQuests.Add(AllQuests[i]);
                AllQuests[i].StartQuest();
                QuestUIManager.uIManager.SetNewChangeIcon();
            }
        }
        ChangeNPCIcon();
    }

    public void ChangeNPCIcon()
    {
        foreach (NPC _npc in npcList)
        {
            _npc.ShowQuestIcon();
        }
    }

    public void GiveUpQuest(int _questID)
    {
        for (int i = 0; i < AllQuests.Count; i++)
        {
            if (AllQuests[i].QuestID == _questID && (AllQuests[i].Progress == BaseQuest.QuestProgress.InProgress || AllQuests[i].Progress == BaseQuest.QuestProgress.Complete))
            {
                AllQuests[i].Progress = BaseQuest.QuestProgress.Available;
                AllQuests[i].GiveUp();
                currentQuests.Remove(AllQuests[i]);
                QuestUIManager.uIManager.SetNewChangeIcon();
            }
        }
        QuestUIManager.uIManager.DestroyQuestNotification(_questID);
        ChangeNPCIcon();
    }

    public void CompleteQuest(int _questID)
    {
        for (int i = 0; i < AllQuests.Count; i++)
        {
            if (AllQuests[i].QuestID == _questID && AllQuests[i].Progress == BaseQuest.QuestProgress.Complete)
            {
                AllQuests[i].Progress = BaseQuest.QuestProgress.Done;
                AllQuests[i].Complete();
                GiveReward(AllQuests[i]);
                currentQuests.Remove(AllQuests[i]);
            }
        }
        QuestUIManager.uIManager.DestroyQuestNotification(_questID);
        CheckChainQuest(_questID);
        ChangeNPCIcon();
    }


    //запрос всех квестов, которые есть у заданного нпс
    public void QuestRequest(NPC _npcObject)
    {
        //проверяем все доступные квесты
        if (_npcObject.AvailableQuestID.Count > 0)
        {
            for (int i = 0; i < AllQuests.Count; i++)
                for (int j = 0; j < _npcObject.AvailableQuestID.Count; j++)
                {
                    if (AllQuests[i].QuestID == _npcObject.AvailableQuestID[j] && AllQuests[i].Progress == BaseQuest.QuestProgress.Available)
                    {
                        QuestUIManager.uIManager.AddToNPCQuestList(AllQuests[i]);
                    }
                }
        }
        //проверяем все текущие квесты(в процессе выполнения)
        for (int i = 0; i < currentQuests.Count; i++)
            for (int j = 0; j < _npcObject.ReceivableQuestID.Count; j++)
            {
                if (currentQuests[i].QuestID == _npcObject.ReceivableQuestID[j] && (currentQuests[i].Progress == BaseQuest.QuestProgress.InProgress || currentQuests[i].Progress == BaseQuest.QuestProgress.Complete))
                {
                    QuestUIManager.uIManager.AddToNPCQuestList(currentQuests[i]);
                }
            }
    }



    //метод для цепочки квестов
    private void CheckChainQuest(int _questID)
    {
        int tempID = 0;
        for (int i = 0; i < AllQuests.Count; i++)
        {
            //если id следующего квеста не 0 (если 0, то квест не является частью цепи)
            if (AllQuests[i].QuestID == _questID && AllQuests[i].NextQuestID > 0)
                //записываем id следующего квеста
                tempID = AllQuests[i].NextQuestID;
        }
        if (tempID > 0)
        {
            for (int i = 0; i < AllQuests.Count; i++)
            {
                //находим следующий в цепи квест и меняем его прогресс с (не доступен) на (доступен)
                if (AllQuests[i].QuestID == tempID && AllQuests[i].Progress == BaseQuest.QuestProgress.NotAvailable)
                {
                    AllQuests[i].Progress = BaseQuest.QuestProgress.Available;
                }
            }
        }
    }
    //награда
    public void GiveReward(BaseQuest _quest)
    {
        player.Money += _quest.Reward;
        UIManager.UIManagerInstance.SetCoins(player.Money);
    }

    public BaseQuest GetCurrentQuest(int _index)
    {
        return currentQuests[_index];
    }

    //возвращает квест по ID
    public BaseQuest GetQuest(int _questID)
    {
        for (int i = 0; i < AllQuests.Count; i++)
            if (AllQuests[i].QuestID == _questID)
                return AllQuests[i];
        return null;
    }
    //метод для проверки, доступен ли квест
    public bool RequestAviableQuest(int _questID)
    {
        for (int i = 0; i < AllQuests.Count; i++)
        {
            if (AllQuests[i].QuestID== _questID && AllQuests[i].Progress == BaseQuest.QuestProgress.Available)
                return true;
        }
        return false;
    }
    //метод для проверки, в процессе ли квест
    public bool RequestInProggreseQuest(int _questID)
    {
        for (int i = 0; i < AllQuests.Count; i++)
        {
            if (AllQuests[i].QuestID == _questID && AllQuests[i].Progress == BaseQuest.QuestProgress.InProgress)
                return true;
        }
        return false;
    }
    //метод для проверки, выполнен (не завершен) ли квест
    public bool RequestCompletedQuest(int _questID)
    {
        for (int i = 0; i < AllQuests.Count; i++)
        {
            if (AllQuests[i].QuestID == _questID && AllQuests[i].Progress == BaseQuest.QuestProgress.Complete)
                return true;
        }
        return false;
    }

    //метод для проерки, есть ли у нужного нпс доступные квесты
    public bool CheckAvailableQuest(NPC _npcObject)
    {
        for (int i = 0; i < AllQuests.Count; i++)
            for (int j = 0; j < _npcObject.AvailableQuestID.Count; j++)
            {
                if (AllQuests[i].QuestID == _npcObject.AvailableQuestID[j] && AllQuests[i].Progress == BaseQuest.QuestProgress.Available)
                    return true;
            }
        return false;
    }
    //метод для проерки, есть ли у нужного нпс квесты в процессе
    public bool CheckInProgressQuest(NPC _npcObject)
    {
        for (int i = 0; i < AllQuests.Count; i++)
            for (int j = 0; j < _npcObject.ReceivableQuestID.Count; j++)
            {
                if (AllQuests[i].QuestID == _npcObject.ReceivableQuestID[j] && AllQuests[i].Progress == BaseQuest.QuestProgress.InProgress)
                    return true;
            }
        return false;
    }
    //метод для проерки, есть ли у нужного нпс выполненные квесты(не завершенные)
    public bool CheckCompleteQuest(NPC _npcObject)
    {
        for (int i = 0; i < AllQuests.Count; i++)
            for (int j = 0; j < _npcObject.ReceivableQuestID.Count; j++)
            {
                if (AllQuests[i].QuestID == _npcObject.ReceivableQuestID[j] && AllQuests[i].Progress == BaseQuest.QuestProgress.Complete)
                    return true;
            }
        return false;
    }

    public NPC GetReceivableQuestNPC(int _questID)
    {
        for (int i = 0; i < npcList.Count; i++)
            for (int j = 0; j < npcList[i].ReceivableQuestID.Count; j++)
            {
                if (npcList[i].ReceivableQuestID[j] == _questID)
                    indexNPC = i;
            }
        return npcList[indexNPC];
    }

    public NPC GetAvailableQuestNPC(int _questID)
    {
        for (int i = 0; i < npcList.Count; i++)
            for (int j = 0; j < npcList[i].AvailableQuestID.Count; j++)
            {
                if (npcList[i].AvailableQuestID[j] == _questID)
                    indexNPC = i;
            }
        return npcList[indexNPC];
    }

    public NPC GetNPC(int _ID)
    {
        for (int i = 0; i < npcList.Count; i++)
        {
            if (npcList[i].NpcID == _ID)
            {
                return npcList[i];
            }
        }
        return null;
    }
}
