﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DontDestroyOnLoad : MonoBehaviour
{
    private static bool isDestroy = false;

    private void Awake()
    {
        if (!isDestroy)
        {
            DontDestroyOnLoad(gameObject);
            isDestroy = true;
        }
        else
        {
            Destroy(gameObject);
        }
    }
}
