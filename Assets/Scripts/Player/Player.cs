﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.AI;
using UnityEngine.SceneManagement;
using System.IO;
using System.Runtime.Serialization;

[System.Serializable]
public class Player : MonoBehaviour
{

    [Header("PlayerInfo")]
    public float StartingHealth=100;
    public int Money;
    public float DistanceToLook=15f;
    public float Speed = 10f;
    public float StartingWalkingTimer;
    public float StartingTimerOfShootingNoise = 1f;
    public float StartingPickUpRadius;
    public bool inOnLevel=false; //для проверки, на базе ли игрок или нет
    public bool isWalking;

    private float currentWalkingTimer;
    private float currentTimerOfShootingNoise;
    private bool watchAtEnemy = false;
    private float currentHealth;
    private float currentPickUpRadius;
    public SphereCollider PickUpSphere;

    private Transform thisTransform;
    private Enemy tempEnemy;
    private RaycastHit hitToCheck;
    private NavMeshAgent agent;
    private RaycastHit hitToMove;
    private Vector3 positionOfShoot;
    private Vector3 tempDest;
    private Quaternion weaponRotation;
    private CameraControl cameraControl;
    public AudioSource DamagedAudio;
    public AudioSource DeathAudio;
    public AudioSource WalkSound;
    private bool shootNoise = false;
    private static bool isDestroy=false;
    public static Player PlayerInstance = null;
 
    private void Awake()
    {
        if (!isDestroy)
        {
            DontDestroyOnLoad(gameObject);
            isDestroy = true;
        }
        else Destroy(gameObject);
    }

    private void Start()
    {
        thisTransform = gameObject.transform;
        currentHealth = StartingHealth;
        UIManager.UIManagerInstance.SetPlayerHealthBar(currentHealth, StartingHealth);
        //DamagedAudio = GetComponent<AudioSource>();
        isWalking = false;
        agent = GetComponent<NavMeshAgent>();
        agent.speed = Speed;
        agent.angularSpeed = 0;
        agent.enabled = true;
        watchAtEnemy = false;
        currentTimerOfShootingNoise = StartingTimerOfShootingNoise;
        currentPickUpRadius = StartingPickUpRadius;
        PickUpSphere.radius = currentPickUpRadius;
        currentWalkingTimer = 0f;
        UIManager.UIManagerInstance.SetCoins(Money);
        weaponRotation = new Quaternion(0, 0, 0, 0);
    }
    
 


    public void SetMoney(int _count)
    {
        Money += _count;
        UIManager.UIManagerInstance.SetCoins(Money);
    }

    public void ActiveMove(bool _isActive)
    {
        agent.enabled = _isActive;
        watchAtEnemy = false;
    }

  
    
    public void SetDestination(Vector3 _destination)
    {
            isWalking = true;
        agent.enabled = true;
        tempDest = _destination;
        agent.destination = tempDest;
        LookAtDestination(_destination);
    }

    private void LookAtDestination(Vector3 _destination)
    {
        if (!watchAtEnemy)
        {
            _destination.y = thisTransform.position.y;
            transform.LookAt(_destination);
        }
    }
    public void ResetWeaponRotation()
    {
        /*if(InvertoryController.invertoryController.CurrentWeapon!=null)
            InvertoryController.invertoryController.CurrentWeapon.transform.localRotation = weaponRotation;*/
    }

    private void SetHandHelmCamera()
    {
        if (cameraControl == null)
        {
            cameraControl = FindObjectOfType<CameraControl>();
        }
        cameraControl.SetShakeDamageCamera();
    }


    public void PlayerWalkSound()
    {
        currentWalkingTimer -= Time.deltaTime;
        if (currentWalkingTimer < 0)
        {
            WalkSound.clip = AudioManager.AudioIntanse.GetPlayerWalkSound();
            WalkSound.Play();
            currentWalkingTimer = StartingWalkingTimer; ;
        }
    }
    public void DamageSoundEffect()
    {
        DamagedAudio.clip = AudioManager.AudioIntanse.GetPlayerDamageShot();
        DamagedAudio.Play();
    }
    //метод для получения урона
    public void TakeDamage(float _damage)
    {
        ArmorItem armor = InvertoryController.invertoryController.PlayerCurrentArmor();
        if (armor != null && armor.CurrentArmorAmount > 0f)
        {
            UIManager.UIManagerInstance.SetPlayerDamagedVignette();
            armor.CurrentArmorAmount -= _damage;
            UIManager.UIManagerInstance.SetArmorSlider(armor.StartArmorAmount, armor.CurrentArmorAmount);
            if (armor.CurrentArmorAmount <= 0f)
            {
                currentHealth += armor.CurrentArmorAmount;
                armor.CurrentArmorAmount = 0f;
                UIManager.UIManagerInstance.SetPlayerHealthBar(currentHealth, StartingHealth);
                InvertoryController.invertoryController.DestroyArmor();
            }
        }
        else
        {
            if (_damage > currentHealth)
                currentHealth = 0;
            else
                currentHealth -= _damage;

            if (currentHealth <= StartingHealth / 2f)
            {
                UIManager.UIManagerInstance.DieVignette.SetDieLowHPVignette(currentHealth, StartingHealth / 2f);
            }

            UIManager.UIManagerInstance.SetPlayerDamagedVignette();
            UIManager.UIManagerInstance.SetPlayerHealthBar(currentHealth, StartingHealth);
            if (currentHealth <= 0)
            {
                Die();
            }
        }
    }

    private bool weaponRot=false;
 
    //смотрим в сторону мыши
    private void WatchAtMouse()
    {
        agent.enabled = true;
        if (Physics.Raycast(Camera.main.ScreenPointToRay(Input.mousePosition), out hitToMove))
        {

            /*if (InvertoryController.invertoryController.CurrentWeapon != null && weaponRot)
            {
                if (hitToMove.collider.tag == "Enemy")
                    InvertoryController.invertoryController.CurrentWeapon.transform.LookAt(hitToMove.collider.transform.position);
                else InvertoryController.invertoryController.CurrentWeapon.transform.LookAt(hitToMove.point);

            }
            else ResetWeaponRotation();*/
            

            if (hitToMove.collider.CompareTag("Cursor"))
            {
                //ResetWeaponRotation();
                Enemy tempObj = hitToMove.collider.GetComponentInParent<Enemy>();
                if (tempEnemy != tempObj)
                {
                    tempEnemy = tempObj;
                    tempObj.SetHealthBar();
                }
            }
            else
            {
                UIManager.UIManagerInstance.DeactivateEnemyHealthBar();
                tempEnemy = null;
            }

            Vector3 target = hitToMove.point;
            target.y = thisTransform.position.y;
            thisTransform.rotation = Quaternion.Slerp(transform.rotation, Quaternion.LookRotation(target - thisTransform.position), Speed * Time.deltaTime);
        }
    }

    private void Update()
    {
        //если игра не окончена
        if (!GameManager.GameInstance.IsGameOver && !GameManager.GameInstance.Pause && !UIManager.UIManagerInstance.SelectInterface)
        {
            if (!InvertoryController.invertoryController.GrenadeMode)
            {
                if (InputManager.InputManagerInstance.PlayerControlType == InputManager.TypeControl.MouseControl && inOnLevel)
                {
                    LookAtClosestEnemy();
                }
                //ручное управление
                if (InputManager.InputManagerInstance.PlayerControlType == InputManager.TypeControl.KeyboardControl)
                {
                    WatchAtMouse();
                }
                //шум выстрела
                if (shootNoise)
                {
                    Noise();
                }
            }
            if (isWalking)
            {
                PlayerWalkSound();
                if (Vector3.Distance(new Vector3(thisTransform.position.x, 0f, thisTransform.position.z), new Vector3(tempDest.x, 0f, tempDest.z)) <= 0.1f
                && InputManager.InputManagerInstance.PlayerControlType == InputManager.TypeControl.MouseControl)
                {
                    ResetWalking();
                }
            }
        }
    }

    public void ResetWalking()
    {
        currentWalkingTimer = 0f;
        isWalking = false;
        WalkSound.Stop();
    }

    public void PlayerKeyBoardMove(Vector3 _direction)
    {
        isWalking = true;
        transform.position += _direction * Speed * Time.deltaTime;
    }
    

    private void LookAtClosestEnemy()
    {
        //ищем ближайшего врага в пределах видимости и смотрим на него
        if (EnemyManager.EnemyInstance.GetCountOfEnemies() > 0 
            && Vector3.Distance(thisTransform.position, FindClosestEnemy().transform.position) <= DistanceToLook)
        {
           
            watchAtEnemy = true;
            if (tempEnemy != FindClosestEnemy())
            {
                tempEnemy = FindClosestEnemy();
                EnemyManager.EnemyInstance.SetEnemyMaterial(tempEnemy);
                tempEnemy.SetHealthBar();
            }
            // смотрим на ближайшего врага
            thisTransform.LookAt(new Vector3(tempEnemy.transform.position.x, thisTransform.position.y, tempEnemy.transform.position.z));
           
            if (InvertoryController.invertoryController.CurrentWeapon != null)
            {
                //InvertoryController.invertoryController.CurrentWeapon.transform.LookAt(tempEnemy.transform.position);
                Physics.Raycast(InvertoryController.invertoryController.CurrentWeapon.BulletSpawn.position, InvertoryController.invertoryController.CurrentWeapon.BulletSpawn.forward, out hitToCheck);
                //стреляем если между игроком и целью нет препятствий
                if (hitToCheck.collider.tag != "obstacle" && Vector3.Distance(InvertoryController.invertoryController.CurrentWeapon.BulletSpawn.position, tempEnemy.transform.position) <= InvertoryController.invertoryController.CurrentWeapon.DistanseToShoot)
                    Shoot();
            }
        }
        else
        {
            watchAtEnemy = false;
            EnemyManager.EnemyInstance.SetEnemyMaterial(null);
            UIManager.UIManagerInstance.DeactivateEnemyHealthBar();
            tempEnemy = null;
        }
    }
    
    //шум
    private void Noise()
    {
        currentTimerOfShootingNoise -= Time.deltaTime;
        //шум длится определенное время, через которое пропадает
        if (currentTimerOfShootingNoise < 0)
        {
            positionOfShoot = Vector3.zero;
            shootNoise = false;
        }
    }

    //стрельба
    public void Shoot()
    {
        if(InvertoryController.invertoryController.CurrentWeapon!=null)
            InvertoryController.invertoryController.CurrentWeapon.Shoot();
        //записываем позицию выстрела
        positionOfShoot = thisTransform.position;
        currentTimerOfShootingNoise = StartingTimerOfShootingNoise;
        shootNoise = true;
    }

    //возвращает ближайшего врага
    private Enemy FindClosestEnemy()
    {
        Enemy closestEnemy = EnemyManager.EnemyInstance.GetEnemy(0);
        for (int i = 0; i < EnemyManager.EnemyInstance.GetCountOfEnemies(); i++)
        {
            Enemy enemy = EnemyManager.EnemyInstance.GetEnemy(i);
            if (Vector3.Distance(thisTransform.position, enemy.transform.position) < Vector3.Distance(thisTransform.position, closestEnemy.transform.position))
            {
                closestEnemy = enemy;
            }
        }
        return closestEnemy;
    }

    public void ResetParametres()
    {
        //закончить игру
        UIManager.UIManagerInstance.DieVignette.SetDieLowHPVignette(1f,1f);
        DungeonController.dungeonController.LastDungeonID = 0;
        currentHealth = StartingHealth;
        //InteructNpcClick = false;
        DungeonController.DungeonMode = false;
        UIManager.UIManagerInstance.SetPlayerHealthBar(currentHealth, StartingHealth);
        //InvertoryController.invertoryController.CurrentWeapon.transform.rotation = weaponRotation;
    }

    private void SetDeathSound()
    {
        DeathAudio.clip = AudioManager.AudioIntanse.GetPlayerDeathSound();
        DeathAudio.Play();
    }
    //смерть игрока 
    private void Die()
    {
        SetDeathSound();
        GameManager.GameInstance.IsGameOver = true;
        GameManager.GameInstance.Restart = true;
        inOnLevel = false;
        agent.enabled = false;
        UIManager.UIManagerInstance.SetGameOverCanvas(true);
        UIManager.UIManagerInstance.SelectInterface = true;
        
        //ResetParametres();
    }
    
    public void RecoveryHealth(DropItem _item)
    {
        if (currentHealth != StartingHealth)
        {
            currentHealth += StartingHealth / 100 * _item.PercentRecovery;
            if (currentHealth > StartingHealth)
            {
                currentHealth = StartingHealth;
            }
            UIManager.UIManagerInstance.SetPlayerHealthBar(currentHealth, StartingHealth);
            UIManager.UIManagerInstance.DieVignette.SetDieLowHPVignette(currentHealth, StartingHealth/2f);
        }
    }


    private void OnTriggerEnter(Collider other)
    {
        switch(other.tag)
        {
           
            case "Platform":
                    weaponRot = true;
                break;
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if(other.tag == "Platform")
        {
            weaponRot = false;
        }
    }

    public Vector3 PositionOfShoot
    {
        set
        {
            positionOfShoot = value;
        }
        get
        {
            return positionOfShoot;
        }
    }
    
    public bool HealthFull()
    {
        if (currentHealth == StartingHealth)
            return true;
        else return false;
    }
}
