﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerSpawner : MonoBehaviour {

    private Player player;

    private void Awake()
    {
        player = FindObjectOfType<Player>();
            player.transform.position = transform.position;
    }
}
