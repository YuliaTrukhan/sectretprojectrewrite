﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Portal : MonoBehaviour {

    public enum PortalType
    {
        ToMain, ToDungeon
    };

    [Header ("Information")]
    public int DungeonID;
    public PortalType TypeOfPortal;
    public Text Message;
    public Transform PlayerSpawner;

    [Header("Sprites")]
    public GameObject NormalSprite;
    public GameObject HoverSprite;    

    private bool inZone = false;
    private Player player;

    private void Start()
    {
        Message.text = DungeonController.dungeonController.GetDungeon(DungeonID).DungeonName;
        ObstacleController.Instanse.AddToAllObstacle(transform);
        player = FindObjectOfType<Player>();
        if(DungeonID == DungeonController.dungeonController.LastDungeonID)
        {
            player.transform.position = PlayerSpawner.position;
            DungeonController.dungeonController.LastDungeonID = DungeonController.dungeonController.GetCurrentDungeon().DungeonID;
        }
        ChangeSprite(true);
    }
    

    public void Teleport()
    {
        if (!DungeonController.dungeonController.GetDungeon(DungeonID).Closed)
        {
            InputManager.InputManagerInstance.InteructNpcClick = false;
            InputManager.InputManagerInstance.InteructPortalClick = false;
            player.ActiveMove(false);
            UIManager.UIManagerInstance.SelectInterface = false;

            //если в подземелье
            if (TypeOfPortal == PortalType.ToDungeon)
            {
                player.inOnLevel = false;
                DungeonController.DungeonMode = true;
                //устанавливаем текущее подземелье
                DungeonController.dungeonController.SetCurrentDungeon(DungeonController.dungeonController.GetDungeon(DungeonID));
                //загружаем сцену
                SceneManager.LoadScene(DungeonController.dungeonController.GetDungeon(DungeonID).LevelToGo);
                for (int i = 0; i < QuestsController.questController.AllQuests.Count; i++)
                {
                    if (QuestsController.questController.AllQuests[i].QuestType == BaseQuest.TypeOfQuest.Escort && QuestsController.questController.AllQuests[i].Progress == BaseQuest.QuestProgress.InProgress)
                    {
                        QuestsController.questController.GiveUpQuest(QuestsController.questController.AllQuests[i].QuestID);
                    }
                }
                QuestsController.questController.ClearNPC();
            }
            else
            {
                player.inOnLevel = true;
                DungeonController.DungeonMode = false;
                DungeonController.dungeonController.SetCurrentDungeon(DungeonController.dungeonController.GetDungeon(DungeonID));
                SceneManager.LoadScene(DungeonController.dungeonController.GetDungeon(DungeonID).LevelToGo);
            }
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            inZone = true;
            if (InputManager.InputManagerInstance.PlayerControlType == InputManager.TypeControl.KeyboardControl)
            {
                ChangeSprite(false);
                InputManager.InputManagerInstance.PortalObject = this;
            }
            else if (InputManager.InputManagerInstance.InteructPortalClick)
            {
                Teleport();
                InputManager.InputManagerInstance.InteructPortalClick = false;
            }
        }
    }


    private void OnTriggerExit(Collider other)
    {
        if (other.tag == "Player")
        {
            inZone = false;
            if (InputManager.InputManagerInstance.PlayerControlType == InputManager.TypeControl.KeyboardControl)
            {
                ChangeSprite(true);
                InputManager.InputManagerInstance.PortalObject=null;
            }
        }
    }

    public void ChangeSprite(bool _isNormal)
    {
        NormalSprite.SetActive(_isNormal);
        HoverSprite.SetActive(!_isNormal);
    }

    public bool PlayerInZone
    {
        get
        {
            return inZone;
        }
    }
}
