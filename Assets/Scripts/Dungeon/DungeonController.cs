﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DungeonController : MonoBehaviour
{
    public static DungeonController dungeonController;
    public List<Dungeon> Dungeons = new List<Dungeon>();
    private Dungeon currentDungeon;
    public static bool DungeonMode=false;
    public int LastDungeonID;

    private void Awake()
    {
        if (dungeonController == null)
        {
            dungeonController = this;
        }
        else if (dungeonController != this)
        {
            Destroy(gameObject);
        }
        DontDestroyOnLoad(gameObject);
        
        SetParametres();
        LastDungeonID = 0;
    }

    public void SetParametres()
    {
        for(int i=0;i<Dungeons.Count; i++)
        {
            Dungeons[i].SetEnemiesCount();
        }
    }

    public Dungeon GetCurrentDungeon()
    {
        return currentDungeon;
    }

    public Dungeon GetDungeon(int _DungeonID)
    {
        for(int i=0; i<Dungeons.Count; i++)
        {
            if (Dungeons[i].DungeonID == _DungeonID)
                return Dungeons[i];
        }
        return null;
    }

    public void SetCurrentDungeon(Dungeon _dungeon)
    {
        currentDungeon = _dungeon;
    }

    public void CheckDungeonClear(int _DungeonID)
    {
        GetDungeon(_DungeonID).CheckClear();
    }

    public void OpenDungeon(int _DungeonID)
    {
        GetDungeon(_DungeonID).Closed = false;
    }
    
}
