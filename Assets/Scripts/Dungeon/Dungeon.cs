﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Dungeon {

    [Header("Information")]
    public int DungeonID;
    public string DungeonName;
    public int EnemiesCount;
    public bool Closed;
    public int NextDungeonID;
    public bool IsClear;
    public bool RespawnAgain;

    [Header ("Scene")]
    public int LevelToGo = 0;
    
    private int currentEnemiesCount;

    
    public void CheckClear()
    {
        currentEnemiesCount--;
        if (currentEnemiesCount == 0)
        {
            if (!RespawnAgain)
                IsClear = true;
            if (NextDungeonID > 0)
            {
                DungeonController.dungeonController.OpenDungeon(NextDungeonID);
            }
        }
    }

    public void SetEnemiesCount()
    {
        currentEnemiesCount = EnemiesCount;
    }
}
