﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class InvertoryUIController : BaseUIManager
{
    public enum Window { All, Weapon, Armor, Staff}
    public static InvertoryUIController invertoryUIController;

    
    public GameObject InvertoryPanel;
    public Transform AllItemsSpacer;
    public InvertoryWeapon WeaponInfo;
    public InvertoryItem ItemInfo;

    [Header("WindowInfo")]
    public GameObject WindowInfo;
    public Image WeaponIcon;
    public Text WeaponName;
    public Text WeaponPrice;
    public Text WeaponLvl;
    public Text WeaponDamageText;
    public Text WeaponSpeedText;
    public Text WeaponReloadText;
    public Text WeaponAmmoText;
    public Text WeaponDescriptionText;
    
    public bool InvertoryPanelActive = false;
    public Window CurrentOpenWinow;

    private List<InvertoryItem> ItemsInfo = new List<InvertoryItem>();
    private List<InvertoryItem> ArmorInfo = new List<InvertoryItem>();
    private List<InvertoryWeapon> WeaponsInfo = new List<InvertoryWeapon>();

    public override void Awake()
    {
        base.Awake();
        if (invertoryUIController == null)
        {
            invertoryUIController = this;
        }
        else if (invertoryUIController != this)
        {
            Destroy(gameObject);
        }
        DontDestroyOnLoad(gameObject);
    }

    private void Start()
    {
        SetStartItems();
        ResetItemInfo();
        ShowInvertoryWindow(false);
    }



    //получить оружие в интвертаре по имени
    public InvertoryWeapon GetWeaponInfo(int _weaponUnicID)
    {
        for(int i=0; i<WeaponsInfo.Count; i++)
        {
            if (WeaponsInfo[i].WeaponUnicID == _weaponUnicID)
                return WeaponsInfo[i];
        }
        return null;
    }


    public void SetItemInformation(int _weaponUnicID)
    {
        Weapon _weapon = InvertoryController.invertoryController.GetWeaponByUnicID(_weaponUnicID);
        WindowInfo.SetActive(true);
        WeaponIcon.sprite = _weapon.WeaponIcon;
        WeaponName.text = _weapon.ObjectName;
        WeaponPrice.text = _weapon.ObjectPrice.ToString();
        WeaponLvl.text = _weapon.Level + " LVL";
        WeaponDamageText.text = _weapon.Damage.ToString();
        WeaponSpeedText.text = _weapon.WeaponCooldown + "/MIN";
        WeaponReloadText.text = _weapon.HolderReloadTimer + " SEC";
        WeaponAmmoText.text = _weapon.CurrentBullets + "/" + _weapon.MaxBullets;
        WeaponDescriptionText.text = _weapon.Description;
    }

    public void ResetItemInfo()
    {
        WindowInfo.SetActive(false);
    }

    //получить объект в инвертаре по id
    public InvertoryItem GetItemInfo(int _ID)
    {
        for (int i = 0; i < ItemsInfo.Count; i++)
        {
            if (ItemsInfo[i].ObjectID == _ID)
                return ItemsInfo[i];
        }
        return null;
    }



    //метод для установки информации о гранате в инвертаре

    public void SetGrenadeItem(Grenade _grenade, int _count)
    {
        WindowIconButton.SetHover();
        InvertoryItem invertoryItem = Instantiate(ItemInfo);
        invertoryItem.transform.SetParent(AllItemsSpacer, false);
        invertoryItem.ItemName.text = "Граната";
        invertoryItem.ItemID = _grenade.ItemID;
        invertoryItem.ObjectID = _grenade.ItemID;
        invertoryItem.ItemIcon.sprite = _grenade.Icon;
        invertoryItem.Count = _count;
        invertoryItem.ActiveSetButton = true;
        invertoryItem.Type = ItemTypes.ItemType.Grenade;
        ItemsInfo.Add(invertoryItem);
        invertoryItem.SetItemSlot();
        invertoryItem.ShowCount = true;
        ShopController.Controller.SetGrenadeSellInfo(_grenade, _count);
        SlotsController.SlotsInstanse.GetSpell(ItemTypes.ItemType.Grenade).SetProgress(InvertoryController.invertoryController.StartingGrenadeAmount, _count);
    }

    //метод для установки информации об оружии в инвертаре
    public void SetWeaponItem(Weapon _weapon)
    {
        WindowIconButton.SetHover();
        InvertoryWeapon invertoryWeapon = Instantiate(WeaponInfo);
        invertoryWeapon.transform.SetParent(AllItemsSpacer, false);
        invertoryWeapon.WeaponName.text = _weapon.ObjectName;
        //while (InvertoryController.invertoryController.GetInventoryItem(_weapon.ObjectID) != null)
        //{
        //    _weapon.ObjectID++;
        //}
        invertoryWeapon.WeaponUnicID = _weapon.ObjectID; //для того, чтобы определять, какое из одинаковых оружий мы используем сейчас
        //задаются настройки
        invertoryWeapon.Type = _weapon.IType;
        invertoryWeapon.WeaponID = _weapon.WeaponID;
        invertoryWeapon.WeaponIcon.sprite = _weapon.WeaponIcon;
        invertoryWeapon.SetWeaponStats(_weapon.Damage, _weapon.WeaponCooldown, _weapon.HolderReloadTimer, _weapon.CurrentBullets, _weapon.MaxBullets);
        WeaponsInfo.Add(invertoryWeapon);
        if(InvertoryController.invertoryController.CurrentWeapon != _weapon)
            _weapon.gameObject.SetActive(false);
        //если в слотах нет такого оружия, ставим его в слот
        if (SlotsController.SlotsInstanse.CheckWeaponInSlot(_weapon.WeaponID))
        {
            invertoryWeapon.SetWeaponSlot();
        }
    }

    //метод для для удаления итема из инвертаря (если итемов несколько, то просто обвновляется счетчик)
    public void DeleteDropItem(int _itemID)
    {
        if (GetItemInfo(_itemID) != null)
        {
            GetItemInfo(_itemID).DeleteItem();
        }
    }
    
    //установка стартовых итемов, если таковые имеются
    private void SetStartItems()
    {
        //оружие
        for (int i = 0; i < InvertoryController.invertoryController.InvertoryWeapons.Count; i++)
        {
            SetWeaponItem(InvertoryController.invertoryController.InvertoryWeapons[i]);
        }
        
    }

    //закрыть панель
    public override void ClosePanel()
    {
        InvertoryPanelActive = false;
        ShowInvertoryWindow(InvertoryPanelActive);
    }

    //открыть/закрыть окно инвертаря
    public void ShowInvertoryWindow(bool _isActive)
    {
        if (_isActive)
        {
            WindowIconButton.SetNormal();
            CanavasController.canvasController.SetCanvasSortOrder(CanvasID);
        }
        else
        {
            SetSortOrder(0);
            //WindowIconButton.SetNormal();
            CanavasController.canvasController.SortOrder--;
            CanavasController.canvasController.CheckSortOrder();
        }
        InvertoryPanel.SetActive(_isActive);
    }



    public void DestroyWeaponInfo(int _weaponUnicID)
    {
        WeaponsInfo.Remove(GetWeaponInfo(_weaponUnicID));
    }

    //удаление итема из инвертаря
    public void DestroyItem(InvertoryItem _item)
    {
        ItemsInfo.Remove(_item);
    }

    //метод для установки информации об итеме в инвертаре
    private void SetInvertoryItem(DropItem _item)
    {
        WindowIconButton.SetHover();
        //установка параметров 
        InvertoryItem invertoryItem = Instantiate(ItemInfo);
        invertoryItem.transform.SetParent(AllItemsSpacer, false);
        invertoryItem.ItemName.text = _item.ObjectName;
        invertoryItem.ItemID = _item.ID;
        invertoryItem.Count= InvertoryController.invertoryController.GetItem(_item.ObjectID).ItemCount;
        invertoryItem.ItemIcon.sprite = _item.Icon;
        if (_item.Stackable)
        {
            _item.ObjectID = _item.ID;
            invertoryItem.ObjectID = _item.ID;
            invertoryItem.ShowCount = true;
        }
        else
        {
            //InvertoryController.invertoryController.GetInventoryItem(_item.ObjectID)
            while (GetItemInfo(_item.ObjectID) != null)
            {
                _item.ObjectID++;
            }
            invertoryItem.ObjectID = _item.ObjectID;
            invertoryItem.ShowCount = false;
        }
        //если ключ, то отключаем кнопку для установки в слот
        if (_item.Type == ItemTypes.ItemType.Key)
        {
            invertoryItem.ActiveSetButton = false;
        }
        else invertoryItem.ActiveSetButton = true;
        invertoryItem.Type = _item.Type;
        //добавление в массив
        ItemsInfo.Add(invertoryItem);
        invertoryItem.SetItemSlot();
    }

    public void OpenWindowItems()
    {
        if(CurrentOpenWinow == Window.All)
        {
            SetWeaponsActive(true);
            SetItemsActive(true);
            SetArmorActive(true);
        }
        else if( CurrentOpenWinow == Window.Armor)
        {
            SetWeaponsActive(false);
            SetItemsActive(false);
            SetArmorActive(true);
        }
        else if (CurrentOpenWinow == Window.Staff)
        {
            SetWeaponsActive(false);
            SetArmorActive(false);
            SetItemsActive(true);
        }
        else if (CurrentOpenWinow == Window.Weapon)
        {
            SetWeaponsActive(true);
            SetItemsActive(false);
            SetArmorActive(false);
        }
    }

    public void SetArmorActive(bool _isActive)
    {
        for (int i = 0; i < ItemsInfo.Count; i++)
        {
            if (_isActive && !ItemsInfo[i].InSlot && ItemsInfo[i].Type == ItemTypes.ItemType.Armor && ItemsInfo[i]!=null)
            {
                ItemsInfo[i].gameObject.SetActive(_isActive);
            }
            else if (!_isActive)
            {
                ItemsInfo[i].gameObject.SetActive(_isActive);
            }
        }
    }

    public void SetWeaponsActive(bool _isActive)
    {
        for(int i=0; i< WeaponsInfo.Count; i++)
        {
            if(_isActive && !WeaponsInfo[i].InSlot)
            {
                WeaponsInfo[i].gameObject.SetActive(_isActive);
            }
            else if (!_isActive)
            {
                WeaponsInfo[i].gameObject.SetActive(_isActive);
            }
        }
    }

    public void SetItemsActive(bool _isActive)
    {
        for (int i = 0; i < ItemsInfo.Count; i++)
        {
            if (_isActive && !ItemsInfo[i].InSlot && ItemsInfo[i].Type != ItemTypes.ItemType.Armor)
            {
                ItemsInfo[i].gameObject.SetActive(_isActive);
            }
            else if (!_isActive)
            {
                ItemsInfo[i].gameObject.SetActive(_isActive);
            }
        }
    }


    //добавление итема в инвертарь
    public void AddDropItem(DropItem _item)
    {
        if (ItemsInfo.Count > 0 && GetItemInfo(_item.ID) != null && _item.Stackable)
        {
            GetItemInfo(_item.ObjectID).Count = InvertoryController.invertoryController.GetItem(_item.ObjectID).ItemCount;
            if(SlotsController.SlotsInstanse.GetSpell(GetItemInfo(_item.ID).Type)!=null)
                SlotsController.SlotsInstanse.GetSpell(GetItemInfo(_item.ID).Type).AddItemCount(1);
        }
        else
        {
            SetInvertoryItem(_item);
        }
    }


 
  
   
}
