﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class InventoryItems
{
    public int ObjectID;
    public int ItemCount;

    public InventoryItems(int _ID, int _count)
    {
        ObjectID = _ID;
        ItemCount = _count;
    }
}

[System.Serializable]
public class ArmorItem
{
    public int ObjectID;
    public float StartArmorAmount;
    public float CurrentArmorAmount;
    public bool isCurrent;
}

public class InvertoryController : MonoBehaviour {

    public static InvertoryController invertoryController;

    public Weapon CurrentWeapon { set; get; }
    public int CurrentArmorID { get; set; }
    
    public List<Weapon> InvertoryWeapons = new List<Weapon>();
    public List<InventoryItems> InventoryDropItems = new List<InventoryItems>();
    public List<ArmorItem> ArmorItems = new List<ArmorItem>();

    //для режима броска гранаты
    [Header("Grenade")]
    public bool GrenadeMode = false;
    public int StartingGrenadeAmount;
    //public float StartingGrenadeCooldown;
    public Grenade GrenadePrefab;
    public Transform GrenadeSpawn;
    public Vector3 GrenadeShootPosition { get; set; }

    private int currentGrenadeAmount;
    //private float currentGrenadeCooldown;
    private bool canShotGrenade = false;
    private RaycastHit hitToMove;
    private RaycastHit hitCursor;
    private Grenade grenade;
    private Player player;
    
    [Header("Weapon Parents")]
    public Transform PlayerSpacer;
    public Transform WeaponPosition;

    [Header("ItemReloadTimer")]
    public float StartingReloadTimer;
    public bool isReload;
    private float currentReloadTimer;
    

    private void Awake()
    {
        if (invertoryController == null)
        {
            invertoryController = this;
        }
        else if (invertoryController != this)
        {
            Destroy(gameObject);
        }
        DontDestroyOnLoad(gameObject);
        player = FindObjectOfType<Player>();
        currentReloadTimer = 0;
    }

    private void Start()
    {
        //currentGrenadeCooldown = 0;
        currentGrenadeAmount = StartingGrenadeAmount;
        GrenadeMode = false;
        InvertoryUIController.invertoryUIController.SetGrenadeItem(GrenadePrefab, StartingGrenadeAmount);
        //ShopController.Controller.SetGrenadeSellInfo(GrenadePrefab, currentGrenadeAmount);
        SetCurrentWeapon(InvertoryWeapons[0].ObjectID);
    }



    //метод для броска гранаты
    private void ShotOneGrenade()
    {
        //currentGrenadeAmount--;
        //UIManager.UIManagerInstance.SetGrenateText(currentGrenadeAmount, StartingGrenadeAmount);
        //запрашиваем гранату из пула
        grenade = GrenatePool.Instanse.GetPooledObject(GrenadePrefab);
        grenade.transform.position = GrenadeSpawn.position;
        grenade.transform.rotation = GrenadeSpawn.rotation;
        //запускаем ее 
        grenade.gameObject.SetActive(true);
        canShotGrenade = false;
        grenade.SetForse(GrenadeShootPosition);
        InvertoryUIController.invertoryUIController.DeleteDropItem(GrenadePrefab.ItemID);
        player.ResetWalking();
        SetGrenadeMode();

    }

    public ArmorItem PlayerCurrentArmor()
    {
        for (int i = 0; i < ArmorItems.Count; i++)
        {
            if (ArmorItems[i].isCurrent)
                return ArmorItems[i];
        }
        return null;
    }

    //метод для добавления патронов 
    public void RecoveryBullets(int _ID, int _percentRecovery)
    {
        for (int i = 0; i < InvertoryWeapons.Count; i++)
        {
            if (InvertoryWeapons[i].WeaponID == _ID)
            {
                InvertoryWeapons[i].AddBullets(_percentRecovery);
            }
        }
    }

    //установка режима гранаты
    public void SetGrenadeMode()
    {
        if (!GrenadeMode && currentGrenadeAmount>0)
        {
            GrenadeMode = true;
            UIManager.UIManagerInstance.CursorAnim.gameObject.SetActive(false);
            UIManager.UIManagerInstance.GrenadeCursor.SetActive(true);

            SetActiveWeapon(-1);
        }
        else if (GrenadeMode && !canShotGrenade)
        {
            GrenadeMode = false;
            player.ActiveMove(true);
            //UIManager.UIManagerInstance.GrenadeUI.SetActive(false);
            UIManager.UIManagerInstance.GrenadeCursor.SetActive(false);
            SetActiveWeapon(CurrentWeapon.ObjectID);
        }
    }
    public void ShootGrenades(Vector3 _dir)
    {
        canShotGrenade = false;
        //Physics.Raycast(Camera.main.ScreenPointToRay(Input.mousePosition), out hitToMove);
        player.transform.LookAt(new Vector3(_dir.x, player.transform.position.y, _dir.z));
        player.ActiveMove(true);
        player.SetDestination(_dir);
        canShotGrenade = true;
    }

    
    private void Update()
    {
        if (GrenadeMode)
        {
            if (Physics.Raycast(Camera.main.ScreenPointToRay(Input.mousePosition), out hitCursor))
            {
                UIManager.UIManagerInstance.SetGrenadeRadius(hitCursor.point);
                //player.transform.LookAt(new Vector3(hit.point.x, player.transform.position.y, hit.point.z));
            }

            //как только игрок подошел на расстоние, достаточное для броска
            if (Vector3.Distance(GrenadeSpawn.position, GrenadeShootPosition) <= GrenadePrefab.ShotRadius)
            {
                //останавливаем его и бросаем гранату
                player.ActiveMove(false);
                if (canShotGrenade)
                {
                    //currentGrenadeCooldown = StartingGrenadeCooldown;
                    ShotOneGrenade();
                    canShotGrenade = false;
                }
            }
        }
        if (isReload)
        {
            currentReloadTimer += Time.deltaTime;
            Spell temp = SlotsController.SlotsInstanse.GetSpell(ItemTypes.ItemType.Medicine);
            temp.SetProgress(StartingReloadTimer, currentReloadTimer);
            if (currentReloadTimer >= StartingReloadTimer)
            {
                isReload = false;
                temp.SetProgress(0f, 0f);
                currentReloadTimer = 0;
            }
        }
    }

    public void DeleteAmmo(int _weaponID)
    {
        for (int i = 0; i < ItemController.itemController.DropItems.Count; i++)
        {
            if (ItemController.itemController.DropItems[i].Type == ItemTypes.ItemType.Ammo && ItemController.itemController.DropItems[i].WeaponID == _weaponID)
            {
                if (GetItem(ItemController.itemController.DropItems[i].ID) != null)
                {
                    DeleteItem(ItemController.itemController.DropItems[i].ObjectID);
                }
            }
        }
    }

    public void DeleteAmmo(int _weaponID, int _count)
    {
        //InvertoryUIController.invertoryUIController.GetItemInfo(ItemController.itemController.GetAmmoItem(_weaponID).ID).RemoveSomeItems(_count);
        for (int i = 0; i < _count; i++)
        {
            if(InvertoryUIController.invertoryUIController.GetItemInfo(ItemController.itemController.GetAmmoItem(_weaponID).ID)!=null)
                InvertoryUIController.invertoryUIController.GetItemInfo(ItemController.itemController.GetAmmoItem(_weaponID).ID).DeleteItem();
        }
       
    }

    public void AddAmmo(int _weaponID)
    {
        for(int i=0;i<InvertoryWeapons.Count; i++)
        {
            if (InvertoryWeapons[i].WeaponID == _weaponID)
            {
                InvertoryWeapons[i].CurrentBullets += ItemController.itemController.GetAmmoItem(_weaponID).PercentRecovery;
            }
        }
    }

    public int GetAmmoAmount(int _weaponID)
    {
        for(int i=0; i<ItemController.itemController.DropItems.Count; i++)
        {
            if(ItemController.itemController.DropItems[i].Type == ItemTypes.ItemType.Ammo && ItemController.itemController.DropItems[i].WeaponID == _weaponID)
            {
                if (GetItem(ItemController.itemController.DropItems[i].ID) != null)
                {
                    return  ItemController.itemController.DropItems[i].PercentRecovery;
                }
            }
        }
        return 0;
    }

    public InventoryItems GetItem(int _ID)
    {
        for (int i = 0; i < InventoryDropItems.Count; i++)
        {
            if (InventoryDropItems[i].ObjectID == _ID)
            {
                return InventoryDropItems[i];
            }
        }
        return null;
    }

    public void SetItem(DropItem _item)
    {
        if(_item.Stackable && GetItem(_item.ObjectID) != null)
        {
            if (_item.Type == ItemTypes.ItemType.Ammo)
                GetItem(_item.ObjectID).ItemCount += ItemController.itemController.GetAmmoItem(_item.WeaponID).PercentRecovery;
            else
                GetItem(_item.ObjectID).ItemCount++;
        }
        else
        {
            InventoryItems tempItem;
            if (_item.Stackable)
            {
                if (_item.Type == ItemTypes.ItemType.Ammo)
                    tempItem = new InventoryItems(_item.ID, ItemController.itemController.GetAmmoItem(_item.WeaponID).PercentRecovery);
                else
                {
                    tempItem = new InventoryItems(_item.ID, 1);
                }
            }
            else
            {
                tempItem = new InventoryItems(_item.ObjectID, 1);
            }
            InventoryDropItems.Add(tempItem);
        }
    }

    public void RemoveItem(int _itemID)
    {
        if (GetItem(_itemID) != null)
        {
            InventoryDropItems.Remove(GetItem(_itemID));
        }
    }

    //удаление гранаты
    public void DeleteGrenade()
    {
        //уменьшаем текущее количество
        currentGrenadeAmount--;
        //обновляем информацию в магазине
        ShopController.Controller.GetSellItemInfo(GrenadePrefab.ItemID).DeleteItem();
        //обновляем информацию в ui инвертаре
        InvertoryUIController.invertoryUIController.GetItemInfo(GrenadePrefab.ItemID).RemoveSomeItems(1);
        SetGrenadeInfoUI();
    }

    public void SetGrenadeInfoUI()
    {
        SlotsController.SlotsInstanse.GetSpell(ItemTypes.ItemType.Grenade).SetAnimateProgress(StartingGrenadeAmount, currentGrenadeAmount);
    }

    //добавление гранаты
    public void AddGrenade()
    {
        //обновление инфы в магазине
        if (currentGrenadeAmount != 0)
        {
            InvertoryUIController.invertoryUIController.GetItemInfo(GrenadePrefab.ItemID).AddCount();
            SlotsController.SlotsInstanse.GetSpell(ItemTypes.ItemType.Grenade).AddItemCount(1);
            ShopController.Controller.GetSellItemInfo(GrenadePrefab.ItemID).AddCount(1);
        }
        else InvertoryUIController.invertoryUIController.SetGrenadeItem(GrenadePrefab, 1); 
        
        //увеличиваем кол-во
        currentGrenadeAmount++;
        //обновить инфу в слотах
        SetGrenadeInfoUI();
        //SlotsController.SlotsInstanse.GetInterfaceSlot(GrenadePrefab.ItemID).SetCountText(currentGrenadeAmount);
    }

    //удаление оружия
    public void DeleteWeapon(int _weaponUnicID)
    {
        for (int i = 0; i < InvertoryWeapons.Count; i++)
        {
            //находим оружие
            if (InvertoryWeapons[i].ObjectID == _weaponUnicID)
            {
                //удаляем инфу из инвертаря
                InvertoryUIController.invertoryUIController.GetWeaponInfo(_weaponUnicID).DestroyInfo();
                //удаляем из массива
                InvertoryWeapons.Remove(InvertoryWeapons[i]);
            }
        }
    }

    //метод для выброса оружия на землю
    public void DropInventoryWeapon(int _weaponUnicID)
    {
        for(int i=0; i<InvertoryWeapons.Count; i++)
        {
            //находим оружие
            if(InvertoryWeapons[i].ObjectID == _weaponUnicID)
            {
                //выбрасываем оружие
                InvertoryWeapons[i].DropWeapon();
                //присваиваем ему новую позицию
                InvertoryWeapons[i].transform.position = new Vector3(player.transform.position.x + 3f, InvertoryWeapons[i].transform.position.y / 2f, player.transform.position.z + 3f);
                //удаляем из массива
                InvertoryWeapons.Remove(InvertoryWeapons[i]);
            }
        }
        
    }

    //получить оружие по уникальному id
    public Weapon GetWeaponByUnicID(int _weaponUnicID)
    {
        for (int i = 0; i < InvertoryWeapons.Count; i++)
        {
            if (InvertoryWeapons[i].ObjectID == _weaponUnicID)
            {
                return InvertoryWeapons[i];
            }
        }
        return null;
    }

    //получить оружие по id
    public Weapon GetWeapon(int _weaponID)
    {
        for(int i=0; i<InvertoryWeapons.Count; i++)
        {
            if(InvertoryWeapons[i].WeaponID == _weaponID)
            {
                return InvertoryWeapons[i];
            }
        }
        return null;
    }

    //метод для восстановления здоровья игрока
    public void UseMedicine(int _itemID)
    {
        //проверяем итем
        if (!isReload)
        {
            //восстанавливаем здоровье
            player.RecoveryHealth(ItemController.itemController.GetItem(_itemID));
            //обновляем инфу в инвертаре
            InvertoryUIController.invertoryUIController.DeleteDropItem(_itemID);
            isReload = true;
        }
    }

    public void SetFullWeapon()
    {
        for (int i = 0; i < InvertoryWeapons.Count; i++)
        {
            if(!InvertoryWeapons[i].IsEmpty() && InvertoryUIController.invertoryUIController.GetWeaponInfo(InvertoryWeapons[i].ObjectID) != null)
            {
                SetActiveWeapon(InvertoryWeapons[i].ObjectID);
            }
        }
    }

    //установка выбранного оружия 
    public void SetActiveWeapon(int _weaponUnicID)
    {
        for(int i=0; i<InvertoryWeapons.Count; i++)
        {
            if(InvertoryWeapons[i].ObjectID == _weaponUnicID)
            {
                InvertoryWeapons[i].gameObject.SetActive(true);
                CurrentWeapon = InvertoryWeapons[i];
                UIManager.UIManagerInstance.SetWeaponName(InvertoryWeapons[i].ObjectName);
                UIManager.UIManagerInstance.SetReloadTextActve(InvertoryWeapons[i].ReloadHolder);
                UIManager.UIManagerInstance.SetWeaponImage(InvertoryWeapons[i].WeaponIcon);
                InvertoryWeapons[i].SetBulletText();
                InvertoryWeapons[i].SetReloadText();
            }
            else
            {
                InvertoryWeapons[i].gameObject.SetActive(false);
            }
            
        }
    }

    public void DeleteItem(int _itemID)
    {
        if (PlayerCurrentArmor() != null && PlayerCurrentArmor().ObjectID == _itemID)
        {
            ResetPlayerArmor();
        }
        if (ShopController.Controller.GetSellItemInfo(_itemID) != null)
            ShopController.Controller.GetSellItemInfo(_itemID).DeleteItem();
        InvertoryUIController.invertoryUIController.GetItemInfo(_itemID).RemoveSomeItems(1);
       //SlotsController.SlotsInstanse.GetItemSlot(_itemID).SetEmpty();
        GetItem(_itemID).ItemCount--;        
        if (GetItem(_itemID).ItemCount == 0)
        {
            InventoryDropItems.Remove(GetItem(_itemID));
            if (SlotsController.SlotsInstanse.GetItemSlot(_itemID)!=null)
            SlotsController.SlotsInstanse.GetItemSlot(_itemID).SetEmpty();
            //Items.Remove(GetInventoryItem(_itemID));
        }
    }



    public bool DropItemRequest(int _itemID)
    {
        for (int i = 0; i < InventoryDropItems.Count; i++)
        {
            if (InventoryDropItems[i].ObjectID == _itemID)
                return true;
        }

        return false;
    }


    
    //добавление оружия
    public void AddWeapon(Weapon _weapon)
    {
        //добавляем в массив
        InvertoryWeapons.Add(_weapon);
        //Items.Add(_weapon);
        //обновляем инфу в инвентаре
        InvertoryUIController.invertoryUIController.SetWeaponItem(_weapon);
        //обновляем инфу в магазине
        ShopController.Controller.SetWeaponSellInfo(_weapon);
    }



    //добавление итема в инвертарь
    public void AddDropItem(DropItem _item)
    {

        if (_item.Stackable)
            _item.ObjectID = _item.ID;
        if (_item.Type == ItemTypes.ItemType.Armor)
        {
            ArmorItem item = new ArmorItem();
            item.ObjectID = _item.ObjectID;
            item.CurrentArmorAmount=item.StartArmorAmount = _item.StartingArmorValue;
            ArmorItems.Add(item);
        }
        
        SetItem(_item);
        //Items.Add(_item);
        
        
        ShopController.Controller.SetItemSellInfo(_item);
        InvertoryUIController.invertoryUIController.AddDropItem(_item);
        if(_item.Type == ItemTypes.ItemType.Ammo)
        {
            AddAmmo(_item.WeaponID);
        }

    }
    

    public void SetCurrentArmor(int _ID)
    {
        for(int i=0; i<ArmorItems.Count; i++)
        {
            if(ArmorItems[i].ObjectID == _ID)
            {
                ArmorItems[i].isCurrent=true;
                CurrentArmorID = ArmorItems[i].ObjectID;
                //CurrentArmor = InventoryArmors[i];
                UIManager.UIManagerInstance.SetArmorSlider(ArmorItems[i].StartArmorAmount, ArmorItems[i].CurrentArmorAmount);

            }
            else
            {
                ArmorItems[i].isCurrent = false;
            }
        }
    }


    public void ResetPlayerArmor()
    {
        //SlotsController.SlotsInstanse.GetSlotType(ItemTypes.ItemType.Armor).SetEmpty();
        //Destroy(InvertoryUIController.invertoryUIController.GetItemInfo(PlayerCurrentArmor().ObjectID).gameObject);
        //CurrentArmor = null;
        for (int i = 0; i < ArmorItems.Count; i++)
        {
            ArmorItems[i].isCurrent = false;
        }

        UIManager.UIManagerInstance.SetArmorSlider(1f, 0f);
    }

    public void DestroyArmor()
    {
        SlotsController.SlotsInstanse.GetSlotType(ItemTypes.ItemType.Armor).SetEmpty();
        InvertoryUIController.invertoryUIController.GetItemInfo(CurrentArmorID).DeleteItem();
        UIManager.UIManagerInstance.SetArmorSlider(1f, 0f);
        ArmorItem item = PlayerCurrentArmor();
        ResetPlayerArmor();
        ArmorItems.Remove(item);
    }

    //устанавливаем текущее оружие
    public void SetCurrentWeapon(int _weaponUnicID)
    {
        SetActiveWeapon(_weaponUnicID);
    }

    public int CurrentGrenadeAmount
    {
        get
        {
            return currentGrenadeAmount;
        }
        set
        {
            currentGrenadeAmount = value;
        }
    }
}
