﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class WeaponSlot : MonoBehaviour {

    //слот для оружия в инвентаре

    [Header("UI")]
    public Image WeaponIcon;
    public Text WeaponNameText;
    public Text ButtonNameText;
    public Text LvlText;
    public Image Inking;
    public GameObject ResetButton;
    public GameObject Plus;

    [Header("Colors")]
    public Color NormalColor;
    public Color HoverColor;
    public Color EmptyColor;
    public Color BGHoverColor;
    public Color BGNormalColor;

    [Header("WeaponUIInfo")]
    public string ButtonName;
    public string WeaponName;

    [Header("WeaponInfo")]
    public KeyCode ButtonCode;
    public int WeaponUnicID;
    public int WeaponID;
    public ItemTypes.ItemType IType;
    public bool isEmpty;

    private Image BGImage;

    private void Awake()
    {
        BGImage = GetComponent<Image>();
    }

    private void Start()
    {
        SetEmptyUI();
    }

    public void SetEmptyUI()
    {
        Inking.color = EmptyColor;
        WeaponIcon.gameObject.SetActive(false);
        ResetButton.SetActive(false);
        Plus.SetActive(false);
        ButtonNameText.text = ButtonName;
        BGImage.color = BGNormalColor;
        LvlText.text = "";
        WeaponNameText.text = WeaponName;
    }

    public void SetHoverInformationUI()
    {
        if (!isEmpty)
        {
            Inking.color = NormalColor;
            InvertoryUIController.invertoryUIController.SetItemInformation(WeaponUnicID);
        }
    }

    public void SetSelectWeaponUI()
    {
        Inking.color = HoverColor;
        BGImage.color = BGHoverColor;
        if (isEmpty)
        {
            Plus.SetActive(true);
        }
    }

    public void SetNormalUI()
    {
        Inking.color = EmptyColor;
        BGImage.color = BGNormalColor;
        InvertoryUIController.invertoryUIController.ResetItemInfo();
        if (isEmpty)
        {
            Plus.SetActive(false);
        }
    }

    //для задачи параметров 
    public void SetParametres(int _weaponID, Sprite _icon, int _weaponUnicID)
    {
        WeaponIcon.sprite = _icon;
        WeaponIcon.gameObject.SetActive(true);
        WeaponNameText.text = "";
        LvlText.text = "15lvl".ToUpper();
        WeaponUnicID = _weaponUnicID;
        isEmpty = false;
        ResetButton.SetActive(true);
    }

    //установка названия слота
    public void SetSlotTitle()
    {
        //if (!isEmpty)
        //{
        //    Title.text = InvertoryController.invertoryController.GetWeapon(WeaponID).WeaponName;

        //}
        //else
        //{
        //    switch (IType)
        //    {
        //        case ItemTypes.ItemType.Weapon:
        //            Title.text = "Слот для оружия";
        //            break;
        //        case ItemTypes.ItemType.HardWeapon:
        //            Title.text = "Слот для тяжелого оружия";
        //            break;

        //    }
        //}
    }

    //удаление предмета из слота
    public void SetEmpty()
    {
        SetEmptyUI();
        UIManager.UIManagerInstance.RemoveWeaponUIIcon(WeaponUnicID);
        //убираем значение "установлено в слоте"
        InvertoryUIController.invertoryUIController.GetWeaponInfo(WeaponUnicID).InSlot = false;
        isEmpty = true;

        if (InvertoryUIController.invertoryUIController.CurrentOpenWinow == InvertoryUIController.Window.All || InvertoryUIController.invertoryUIController.CurrentOpenWinow == InvertoryUIController.Window.Weapon)
            InvertoryUIController.invertoryUIController.GetWeaponInfo(WeaponUnicID).gameObject.SetActive(true);
        //если оружие текущее
        if (InvertoryController.invertoryController.CurrentWeapon.ObjectID == WeaponUnicID)
        {
            //обнуляем инфу слота
            WeaponUnicID = -1;
            //если есть еще оружие в слотах, устанавливаем первое попавшееся текущим
            InvertoryController.invertoryController.SetCurrentWeapon(SlotsController.SlotsInstanse.GetOccupySlot());
        }
        else WeaponUnicID = -1;
       
        WeaponIcon.sprite = null;
        SetSlotTitle();
    }

    //использование слота
    public void UseItem()
    {
        if (!isEmpty)
        {
            InvertoryController.invertoryController.SetActiveWeapon(WeaponUnicID);
        }
    }
}
