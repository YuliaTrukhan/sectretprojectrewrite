﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ItemSlot : MonoBehaviour {

    //слот для итема в инвентаре
    [Header ("UI")]
    public Image Icon;
    public Image Inking;
    public Text Title;
    public Text ButtonNameText;
    public GameObject ButtonReset;
    public GameObject Plus;

    [Header("Info")]
    public string ButtonName;
    public string SlotName;
    public int ItemID; 
    public ItemTypes.ItemType SlotType;
    public bool IsEmpty;

    [Header("Color")]
    public Color NormalColor;
    public Color HoverColor;
    public Color EmptyColor;
    public Color BGHoverColor;
    public Color BGNormalColor;

    private Image BGImage;

    private void Awake()
    {
        BGImage = GetComponent<Image>();
        ButtonNameText.text = ButtonName;
        Title.text = SlotName;
        SetEmptyUI();
    }
    public void SetEmptyUI()
    {
        Inking.color = EmptyColor;
        Icon.gameObject.SetActive(false);
        ButtonReset.SetActive(false);
        Plus.SetActive(false);
        ButtonNameText.text = ButtonName;
        BGImage.color = BGNormalColor;
        Title.text = SlotName;
    }

    public void SetHoverInformationUI()
    {
        if (!IsEmpty)
        {
            Inking.color = NormalColor;
        }
    }

    public void SetSelectWeaponUI()
    {
        Inking.color = HoverColor;
        BGImage.color = BGHoverColor;
        if (IsEmpty)
        {
            Plus.SetActive(true);
        }
    }

    public void SetNormalUI()
    {
        Inking.color = EmptyColor;
        BGImage.color = BGNormalColor;
        InvertoryUIController.invertoryUIController.ResetItemInfo();
        if (IsEmpty)
        {
            Plus.SetActive(false);
        }
    }


    //для задачи параметров 
    public void SetParametres(Sprite _icon, int _ID)
    {
        Icon.gameObject.SetActive(true);
        Icon.sprite = _icon;
        ItemID = _ID;
        Title.gameObject.SetActive(false);
        IsEmpty = false;
        ButtonReset.SetActive(true);
    }


    //удаление предмета из слота
    public void SetEmpty()
    {
        if (InvertoryUIController.invertoryUIController.GetItemInfo(ItemID) != null)
        {
            //SlotsController.SlotsInstanse.GetSpell(SlotType).SetSpellEmpty();
            //обновляем информацию в инвентаре
            InvertoryUIController.invertoryUIController.GetItemInfo(ItemID).DeleteFromSlot();
        }
        if (SlotType == ItemTypes.ItemType.Grenade && InvertoryController.invertoryController.GrenadeMode)
        {
            InvertoryController.invertoryController.SetGrenadeMode();
        }
        if(SlotsController.SlotsInstanse.GetSpell(SlotType)!=null)
            SlotsController.SlotsInstanse.GetSpell(SlotType).SetSpellEmpty();
        
        Icon.gameObject.SetActive(false);
        Title.gameObject.SetActive(true);
        ButtonReset.SetActive(false);
        IsEmpty = true;
    }

    //использование слота
    public void UseItem()
    {
        switch (SlotType)
        {
            case ItemTypes.ItemType.Medicine:
                //используем аптечку
                InvertoryController.invertoryController.UseMedicine(ItemID);
                //если больше таким итемов нет, то освобождаем слот
                if (!InvertoryController.invertoryController.DropItemRequest(ItemID))
                {
                    SetEmpty();
                }
                break;
            //переход в режим гранаты и обратно
            case ItemTypes.ItemType.Grenade:
                InvertoryController.invertoryController.SetGrenadeMode();
                break;
        }
    }
}
