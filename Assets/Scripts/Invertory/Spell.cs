﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Spell : MonoBehaviour {

    [Header("UI")]
    public Image BgImage;
    public Image ProgressImage;
    public Image Icon;
    public GameObject ItemCount;
    public Text ItemCountText;

    [Header("Info")]
    public float AnimationTimer;
    public bool IsEmpty;
    public ItemTypes.ItemType SpellType;
    public int Count;
    private bool isAnimate;
    private float currentAmount;
    private float difference;

    [Header("Color")]
    public Color EmptyColor;
    public Color NormalColor;

    private void Start()
    {
        isAnimate = false;
    }

    public void SetSpellEmpty()
    {
        ProgressImage.gameObject.SetActive(false);
        BgImage.color = EmptyColor;
        Icon.color = EmptyColor;
        ItemCount.SetActive(false);
    }

    public void SetSpellCount(int _itemCount)
    {
        ItemCountText.text = _itemCount.ToString();
    }

    public void RemoveOneItem()
    {
        Count--;
        SetSpellCount(Count);
        if (Count == 0)
        {
            IsEmpty = true;
            SetSpellEmpty();
        }
    }

    public void AddItemCount(int _count)
    {
        if (IsEmpty)
        {
            IsEmpty = false;
            SetSpellFull();
        }
        Count+= _count;
        SetSpellCount(Count);
    }

    public void SetSpellFull()
    {
        ProgressImage.gameObject.SetActive(true);
        BgImage.color = NormalColor;
        Icon.color = NormalColor;
        ItemCount.SetActive(true);
        IsEmpty = false;
        //ItemCountText.text = _itemCount.ToString();
        //if (SpellType == ItemTypes.ItemType.Grenade)
        //    InvertoryController.invertoryController.SetGrenadeInfoUI();
    }

    public void SetProgress(float _startingAmount, float _currentAmount)
    {
        ProgressImage.fillAmount = _currentAmount / _startingAmount;
    }

    public void SetAnimateProgress(float _startingAmount, float _currentAmount)
    {
        currentAmount= _currentAmount / _startingAmount;
        difference = currentAmount - ProgressImage.fillAmount;
        SetSpellCount(Count);
        isAnimate = true;
       // ProgressImage.fillAmount = 
    }

    private void Update()
    {
        if (isAnimate)
        {
            ProgressImage.fillAmount += difference * AnimationTimer;
            if((difference<0 && ProgressImage.fillAmount<=currentAmount) || (difference >= 0 && ProgressImage.fillAmount >= currentAmount))
            {
                ProgressImage.fillAmount = currentAmount;
                if(currentAmount == 0)
                {
                    SetSpellEmpty();
                }
                isAnimate = false;
            }
        }
    }
}
