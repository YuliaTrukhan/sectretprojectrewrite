﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class InvertoryItem : MonoBehaviour {

    //отображение итемов в инвентаре
    [Header("Items Info")]
    public int ItemID;
    public int ObjectID;
    public ItemTypes.ItemType Type;
    public bool InSlot = false;
    public bool ActiveSetButton;
    public bool ShowCount;

    [Header ("UI")]
    public Image ItemIcon;
    public Text ItemName;
    public Text ItemCount;
    public GameObject SetButton;

    public int Count
    {
        get
        {
            return count;
        }
        set {
            count = value;
            SetItemCountText();
        }
    }

    public Color NormalColor;
    public Color HoverColor;
    public Color HoverTextColor;
    public Color NormalTextColor;

    private int count;
    private Image bgImage;

    private void Awake()
    {
        bgImage = GetComponent<Image>();
    }

    private void Start()
    {
        ItemCount.text = "";
        SetItemCountText();
        SetButton.SetActive(ActiveSetButton);
    }

    public void SetHover()
    {
        bgImage.color = HoverColor;
        ItemName.color = HoverTextColor;
        ItemCount.color = HoverTextColor;
        //WeaponStats.color = HoverTextColor;
        SlotsController.SlotsInstanse.SetSelectedItemSlot(Type);
        //SlotsController.SlotsInstanse.SetSelectedWeaponSlot(WeaponID);
        //InvertoryUIController.invertoryUIController.SetItemInformation(WeaponUnicID);
    }

    public void SetNormal()
    {
        bgImage.color = NormalColor;
        ItemName.color = NormalTextColor;
        ItemCount.color = NormalTextColor;
        //WeaponStats.color = NormalTextColor;
        SlotsController.SlotsInstanse.SetNormalItemSlot(Type);
        //InvertoryUIController.invertoryUIController.ResetItemInfo();
    }

    public void AddCount()
    {
        Count++;
        SetItemCountText();
    }

    public void SetItemCountText()
    {
        if(ShowCount)
            ItemCount.text = "x" + Count;
    }

    public void RemoveSomeItems(int _count)
    {
        Count-=_count;
        if (Type == ItemTypes.ItemType.Ammo)
        {
            InvertoryController.invertoryController.GetWeapon(ItemController.itemController.GetItem(ItemID).WeaponID).CurrentBullets--;
        }
        SetItemCountText();
        if(SlotsController.SlotsInstanse.GetSpell(Type)!=null)
            SlotsController.SlotsInstanse.GetSpell(Type).RemoveOneItem();
        //если количество равно нулю, информация о нем убирается
        if (Count == 0)
        {
            InvertoryUIController.invertoryUIController.DestroyItem(this);
            Destroy(gameObject);
        }
    }

    //удаление итема из инвентаря
    public void DeleteItem()
    {
        switch (Type)
        {
            case ItemTypes.ItemType.Medicine:
            case ItemTypes.ItemType.Key:
            case ItemTypes.ItemType.Armor:
            case ItemTypes.ItemType.Ammo:
                InvertoryController.invertoryController.DeleteItem(ObjectID);
                break;
            case ItemTypes.ItemType.Grenade:
                InvertoryController.invertoryController.DeleteGrenade();
                break;
        }
    }


    public void DeleteFromSlot()
    {
        InSlot = false;
        if(SlotsController.SlotsInstanse.GetSpell(Type)!=null)
            SlotsController.SlotsInstanse.GetSpell(Type).Count = 0;
        if (Type != ItemTypes.ItemType.Armor)
        {
            if (InvertoryUIController.invertoryUIController.CurrentOpenWinow == InvertoryUIController.Window.All || InvertoryUIController.invertoryUIController.CurrentOpenWinow == InvertoryUIController.Window.Staff)
                gameObject.SetActive(true);
        }
        else
        {
            if (InvertoryUIController.invertoryUIController.CurrentOpenWinow == InvertoryUIController.Window.All || InvertoryUIController.invertoryUIController.CurrentOpenWinow == InvertoryUIController.Window.Armor)
                gameObject.SetActive(true);
        }
        if (Type == ItemTypes.ItemType.Armor)
        {
            InvertoryController.invertoryController.ResetPlayerArmor();
        }
    }



    //установка итема на слот
    public void SetItemSlot()
    {
        ItemSlot tempSlot = SlotsController.SlotsInstanse.GetSlotType(Type);
        if (tempSlot != null && tempSlot.IsEmpty)
        {
            SetNormal();
            //установка параметров в слот в инвентаре
            tempSlot.SetParametres(ItemIcon.sprite, ObjectID);
            Spell spell = SlotsController.SlotsInstanse.GetSpell(Type);
            if (spell != null)
            {
                SlotsController.SlotsInstanse.GetSpell(Type).SetSpellFull();
                SlotsController.SlotsInstanse.GetSpell(Type).AddItemCount(Count);
            }
            if (Type == ItemTypes.ItemType.Armor)
            {
                InvertoryController.invertoryController.SetCurrentArmor(ObjectID);
                //InvertoryController.invertoryController.SetPlayerArmor(ItemController.itemController.GetItem(ItemID));
            }
            InSlot = true;
            gameObject.SetActive(false);
        }
    }
}
