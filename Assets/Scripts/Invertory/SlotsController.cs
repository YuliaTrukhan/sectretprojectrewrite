﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SlotsController: MonoBehaviour {

    //управление слотами
    public static SlotsController SlotsInstanse; 

    [Header("WeaponSlots")]
    public List<WeaponSlot> WeaponsSlots = new List<WeaponSlot>();
    [Header("ItemSlots")]
    public List<ItemSlot> ItemSlots = new List<ItemSlot>();
    [Header("Spells")]
    public List<Spell> Spells = new List<Spell>();

    private int slotIndex;

    private void Awake()
    {
        if (SlotsInstanse == null)
        {
            SlotsInstanse = this;
        }
        else if (SlotsInstanse != this)
        {
            Destroy(gameObject);
        }
        DontDestroyOnLoad(gameObject);
    }

    private void Start()
    {
        slotIndex = 0;
        ActiveWeaponSlot(slotIndex);
    }

    public Spell GetSpell(ItemTypes.ItemType _type)
    {
        for(int i=0; i<Spells.Count; i++)
        {
            if(Spells[i].SpellType == _type)
            {
                return Spells[i];
            }
        }
        return null;
    }
    
    public void SetSelectedWeaponSlot(int _weaponID)
    {
        for(int i=0; i<WeaponsSlots.Count; i++)
        {
            if(WeaponsSlots[i].WeaponID == _weaponID)
            {
                WeaponsSlots[i].SetSelectWeaponUI();
            }
        }
    }

    public void SetSelectedItemSlot(ItemTypes.ItemType _type)
    {
        for (int i = 0; i < ItemSlots.Count; i++)
        {
            if (ItemSlots[i].SlotType == _type)
            {
                ItemSlots[i].SetSelectWeaponUI();
            }
        }
    }

    public void SetNormalWeaponSlot(int _weaponID)
    {
        for (int i = 0; i < WeaponsSlots.Count; i++)
        {
            if (WeaponsSlots[i].WeaponID == _weaponID)
            {
                WeaponsSlots[i].SetNormalUI();
            }
        }
    }

    public void SetNormalItemSlot(ItemTypes.ItemType _type)
    {
        for (int i = 0; i < ItemSlots.Count; i++)
        {
            if (ItemSlots[i].SlotType == _type)
            {
                ItemSlots[i].SetNormalUI();
            }
        }
    }

    //очистить слот для оружия
    public void RefreshWeaponSlot(int _weaponUnicID)
    {
        for (int i = 0; i < WeaponsSlots.Count; i++)
        {
            if (!WeaponsSlots[i].isEmpty && WeaponsSlots[i].WeaponUnicID == _weaponUnicID)
            {
                WeaponsSlots[i].SetEmpty();
                return;
            }
        }
        ////если все слоты с оружием пусты, убираем текст с патронами
        //if (EmptyWeaponSlotsRequest(ItemTypes.ItemType.Weapon) && EmptyWeaponSlotsRequest(ItemTypes.ItemType.HardWeapon))
        //{
        //    UIManager.UIManagerInstance.SetWeaponTextActive();
        //}
    }

    public ItemSlot GetSlotType(ItemTypes.ItemType _type)
    {
        for(int i = 0; i< ItemSlots.Count; i++)
        {
            if(ItemSlots[i].SlotType == _type)
            {
                return ItemSlots[i];
            }
        }
        return null;
    }
    public int GetOccupySlot()
    {
        for(int i=0; i<WeaponsSlots.Count; i++)
        {
            if (!WeaponsSlots[i].isEmpty)
            {
                return WeaponsSlots[i].WeaponUnicID;
            }
        }
        return -1;
    }

 

    public void SetWeaponSlot(Weapon _weapon)
    {
        for(int i=0; i<WeaponsSlots.Count; i++)
        {
            if (WeaponsSlots[i].isEmpty && WeaponsSlots[i].WeaponID == _weapon.WeaponID)
            {
                WeaponsSlots[i].SetParametres(_weapon.WeaponID, _weapon.WeaponIcon, _weapon.ObjectID);
                UIManager.UIManagerInstance.SetWeaponUIIcon(_weapon.ObjectID, WeaponsSlots[i].ButtonName);
                WeaponsSlots[i].SetSlotTitle();
                return;
            }
        }
    }





    //использоват выбранный слот оружия
    public void ActiveWeaponSlot(int _index)
    {
        for(int i=0; i<WeaponsSlots.Count; i++)
        {
            if (i == _index)
            {
                WeaponsSlots[i].UseItem();
            }
        }
    }
   
    //использовать выбранный слот итема
    public void ActiveItemSlot(int _index)
    {
        for (int i = 0; i < ItemSlots.Count; i++)
        {
            if (i == _index )
            {
                ItemSlots[i].UseItem();
            }
        }
    }


    public bool CheckWeaponSlot(int _weaponID)
    {
        for(int i=0; i<WeaponsSlots.Count; i++)
        {
            if(WeaponsSlots[i].WeaponID == _weaponID)
            {
                return true;
            }
        }
        return false;
    }

    //для проверки, есть ли такое же оружие, которое мы подобрали, в слоте
    public bool CheckWeaponInSlot(int _weaponID)
    {
            for (int i = 0; i < WeaponsSlots.Count; i++)
            {
                if (WeaponsSlots[i].WeaponID == _weaponID  && WeaponsSlots[i].isEmpty)
                {
                    return true;
                }
            }
            return false;
    }

    public bool EmptyWeaponSlotsRequest(ItemTypes.ItemType _type)
    {
        for (int i = 0; i < WeaponsSlots.Count; i++)
        {
            if (WeaponsSlots[i].isEmpty && WeaponsSlots[i].IType == _type)
            {
                return true;
            }
        }
        return false;
    }
    

    public ItemSlot GetItemSlot(int _itemID)
    {
        for(int i=0; i<ItemSlots.Count; i++)
        {
            if(ItemSlots[i].ItemID == _itemID)
            {
                return ItemSlots[i];
            }
        }
        return null;
    }
}
