﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class InvertoryQuestItem : MonoBehaviour {

    public int QuestID;
    public int ItemID;
    public Image ItemIcon;
    public Text ItemName;
    public Text ItemCount;
    public int Count;

    private void Start()
    {
        SetItemCount();
    }

    public void SetItemCount()
    {
        ItemCount.text = "x" + Count;
    }

    public void DestroyItem()
    {
        gameObject.SetActive(false);
    }
}
