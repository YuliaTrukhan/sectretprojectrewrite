﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class InvertoryWeapon : MonoBehaviour {

    //оображение оружия в инвентаре

    public ItemTypes.ItemType Type;
    public int WeaponID;
    public Image WeaponIcon;
    public Text WeaponName;
    public Text WeaponStats;
    public bool InSlot = false; //отслеживает, установлен ли уже объект в слот
    
    public int WeaponUnicID { get; set; }

    public Color NormalColor;
    public Color HoverColor;
    public Color HoverTextColor;
    public Color NormalTextColor;

    private Image bgImage;

    private void Awake()
    {
        bgImage = GetComponent<Image>();
    }

    public void SetHover()
    {
        bgImage.color = HoverColor;
        WeaponName.color = HoverTextColor;
        WeaponStats.color = HoverTextColor;
        SlotsController.SlotsInstanse.SetSelectedWeaponSlot(WeaponID);
        InvertoryUIController.invertoryUIController.SetItemInformation(WeaponUnicID);
    }

    public void SetNormal()
    {
        bgImage.color = NormalColor;
        WeaponName.color = NormalTextColor;
        WeaponStats.color = NormalTextColor;
        SlotsController.SlotsInstanse.SetNormalWeaponSlot(WeaponID);
        InvertoryUIController.invertoryUIController.ResetItemInfo();
    }

    //установка параметров оружия
    public void SetWeaponStats(int _damage, float _cooldown, float _reload, int _currentBullets, int _maxBullets)
    {
        WeaponStats.text = "DMG:"+ _damage + " SP:" + _cooldown + " R:" + _reload + "A:"+ _currentBullets + "/" + _maxBullets;
    }

    //установка выбранного оружия
    public void SetCurrentWeapon()
    {
        InvertoryController.invertoryController.SetActiveWeapon(WeaponUnicID);
    }

    //установка оружия в слот
    public void SetWeaponSlot()
    {
        //если оружие еще не поставлено в слот
        if (!InSlot)
        {
            SetNormal();
            Weapon tempWeapon = InvertoryController.invertoryController.GetWeaponByUnicID(WeaponUnicID);
            SlotsController.SlotsInstanse.SetWeaponSlot(tempWeapon);
            tempWeapon.SetWeaponUIInfo();
            InvertoryUIController.invertoryUIController.GetWeaponInfo(WeaponUnicID).gameObject.SetActive(false);
            InvertoryUIController.invertoryUIController.ResetItemInfo();
            SlotsController.SlotsInstanse.SetNormalWeaponSlot(WeaponID);
            InSlot = true;

        }
    }

    //удаление оружия из инвертаря
    public void DeleteWeapon()
    {
        InSlot = false;
        SlotsController.SlotsInstanse.RefreshWeaponSlot(WeaponUnicID);
        InvertoryController.invertoryController.DropInventoryWeapon(WeaponUnicID);
        InvertoryUIController.invertoryUIController.DestroyWeaponInfo(WeaponUnicID);
        ShopController.Controller.RemoveSellWeaponInfoList(WeaponUnicID);
        DestroyInfo();
    }
    
    public void DestroyInfo()
    {
        InSlot = false;
        SlotsController.SlotsInstanse.RefreshWeaponSlot(WeaponUnicID);
        InvertoryUIController.invertoryUIController.DestroyWeaponInfo(WeaponUnicID);
        Destroy(gameObject);
    }
}
