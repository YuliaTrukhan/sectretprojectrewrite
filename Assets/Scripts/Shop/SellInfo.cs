﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

//инфо о продаже предметов
public class SellInfo : MonoBehaviour {

    public int ItemID;
    public Image ItemIcon;
    public Text ItemName;
    public int WeaponUnicID;
    public Text ItemStats;
    public Text PriceText;
    public Text ItemLvl;
    public Text CountText;
    public int Count;
    public ItemTypes.ItemType Type;

    public Color NormalColor;
    public Color HoverColor;
    public Color HoverTextColor;
    public Color NormalTextColor;

    private Image bgImage;
    private int price;


    private void Awake()
    {
        bgImage = GetComponent<Image>();
    }

    public void SetHover()
    {
        bgImage.color = HoverColor;
        ItemName.color = HoverTextColor;
        CountText.color = HoverTextColor;
        if (ItemStats != null)
            ItemStats.color = HoverTextColor;
        if (Type == ItemTypes.ItemType.Weapon || Type == ItemTypes.ItemType.HardWeapon)
            ShopController.Controller.SetSellItemInformation(WeaponUnicID);
    }

    public void SetNormal()
    {
        bgImage.color = NormalColor;
        ItemName.color = NormalTextColor;
        CountText.color = NormalTextColor;
        if (ItemStats != null)
            ItemStats.color = NormalTextColor;
        ShopController.Controller.ResetItemInfo();
    }

    //для установки параметров
    public void SetItemInfo(int _ID, Sprite _itemIcon, string _itemName, ItemTypes.ItemType _type, int _price)
    {
        ItemIcon.sprite = _itemIcon; //иконка
        ItemName.text = _itemName; //название
        ItemID = _ID;
        Type = _type;
        PriceText.text = _price.ToString();
        price = _price;
        if (Type == ItemTypes.ItemType.Weapon || Type == ItemTypes.ItemType.HardWeapon)
        {
            Weapon _weapon = InvertoryController.invertoryController.GetWeaponByUnicID(WeaponUnicID);
            SetWeaponStats(_weapon.Damage, _weapon.WeaponCooldown, _weapon.HolderReloadTimer, _weapon.CurrentBullets, _weapon.MaxBullets, _weapon.Level);
            CountText.text = "";
        }
        else
        {
            ItemStats.text = "";
            ItemLvl.text = "";
            
        }
    }

    public void SetWeaponStats(int _damage, float _cooldown, float _reload, int _currentBullets, int _maxBullets, int _lvl)
    {
        if (ItemStats != null)
        {
            ItemStats.text = "DMG:" + _damage + " SP:" + _cooldown + " R:" + _reload + "A:" + _currentBullets + "/" + _maxBullets;
            ItemLvl.text = _lvl + "LVL";
        }
    }

    public void UpdateCountText()
    {
        CountText.text = "x" + Count;
    }

    //удаление итема
    public void DeleteItem()
    {
        //уменьшаем количество
        Count--;
        UpdateCountText();
        //если количество = 0
        if (Count == 0)
        {
            //удаляем инфу из магазина
            ShopController.Controller.RemoveSellItemsInfoList(ItemID);
            Destroy(gameObject);
        }
    }

    public void AddCount(int _count)
    {
        Count += _count;
        UpdateCountText();
    }

    //продажа итема
    public void SellItem()
    {
        switch (Type)
        {
            //для стаффа
            case ItemTypes.ItemType.Medicine:
            case ItemTypes.ItemType.Key:
            case ItemTypes.ItemType.Armor:
            case ItemTypes.ItemType.Ammo:
                //удаление из инвентаря
                InvertoryController.invertoryController.DeleteItem(ItemID);
                break;
            case ItemTypes.ItemType.Grenade:
                InvertoryController.invertoryController.DeleteGrenade();
                break;
                //оружие
            case ItemTypes.ItemType.Weapon:
            case ItemTypes.ItemType.HardWeapon:
                InvertoryController.invertoryController.DeleteWeapon(WeaponUnicID);
                DeleteItem();
                break;
        }
        //плучение денег
        ShopController.Controller.EarnMoney(price);
    }

}
