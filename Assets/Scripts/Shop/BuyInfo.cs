﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

//инфа о покупке предмета
public class BuyInfo : MonoBehaviour {

    public Image ItemIcon;
    public Text ItemName;
    public Text ItemStats;
    public Text PriceText;
    public Text ItemLvl;

    public Color NormalColor;
    public Color HoverColor;
    public Color HoverTextColor;
    public Color NormalTextColor;

    private Image bgImage;
    private int itemID;
    private int price;
    private ItemTypes.ItemType type;

    private void Awake()
    {
        bgImage = GetComponent<Image>();
    }

    public void SetHover()
    {
        bgImage.color = HoverColor;
        ItemName.color = HoverTextColor;
        if(ItemStats!=null)
            ItemStats.color = HoverTextColor;
        if(type == ItemTypes.ItemType.Weapon || type == ItemTypes.ItemType.HardWeapon)
            ShopController.Controller.SetBuyItemInformation(itemID);
    }

    public void SetNormal()
    {
        bgImage.color = NormalColor;
        ItemName.color = NormalTextColor;
        if (ItemStats != null)
            ItemStats.color = NormalTextColor;
        ShopController.Controller.ResetItemInfo();
    }

    //установка параметров
    public void SetItemInfo(int _ID, Sprite _itemIcon, string _itemName, int _price, ItemTypes.ItemType _type)
    {
        itemID = _ID;
        ItemIcon.sprite = _itemIcon;
        ItemName.text = _itemName;
        PriceText.text = _price.ToString();
        price = _price;
        type = _type;
        if (type == ItemTypes.ItemType.Weapon || type == ItemTypes.ItemType.HardWeapon)
        {
            Weapon _weapon = ShopController.Controller.GetWeapon(itemID);
            SetWeaponStats(_weapon.Damage, _weapon.WeaponCooldown, _weapon.HolderReloadTimer, _weapon.StartingBullets - _weapon.StartingHolderAmount, _weapon.MaxBullets);
            ItemLvl.text = _weapon.Level + "LVL";
        }
        else
        {
            ItemStats.text = "";
            ItemLvl.text = "";
        }
    }

    public void SetWeaponStats(int _damage, float _cooldown, float _reload, int _currentBullets, int _maxBullets)
    {
        if (ItemStats != null)
        {
            ItemStats.text = "DMG:" + _damage + " SP:" + _cooldown + " R:" + _reload + "A:" + _currentBullets + "/" + _maxBullets;
            ItemLvl.text = "1 LVL";
        }
    }

    //покупка итема
    public void BuyItem()
    {
        //проверяем, если ли у нас достаточная сумма
        if (ShopController.Controller.PlayerMoneyRequest(price))
        {
            switch (type)
            {
                //стафф
                case ItemTypes.ItemType.Medicine:
                case ItemTypes.ItemType.Armor:
                    //обновляем инфу в инвентаре
                    DropItem _item = DropItemPool.ItemInstanse.GetPooledItem(ItemController.itemController.GetItem(itemID)); 
                    InvertoryController.invertoryController.AddDropItem(_item);
                    _item.DestroyItem();
                    //отнимаем деньги
                    ShopController.Controller.SpendMoney(price);
                    break;
                case ItemTypes.ItemType.Weapon:
                case ItemTypes.ItemType.HardWeapon:
                    Weapon weapon = Instantiate(ShopController.Controller.GetWeapon(itemID));
                    weapon.PickUpWeapon();
                    ShopController.Controller.SpendMoney(price);
                    ShopController.Controller.RemoveBuyItem(this);
                    Destroy(gameObject);
                    break;
                case ItemTypes.ItemType.Grenade:
                    if(InvertoryController.invertoryController.CurrentGrenadeAmount < InvertoryController.invertoryController.StartingGrenadeAmount)
                    {
                        InvertoryController.invertoryController.AddGrenade();
                        ShopController.Controller.SpendMoney(price);
                    }
                    break;
            }
           
        }
    }
}
