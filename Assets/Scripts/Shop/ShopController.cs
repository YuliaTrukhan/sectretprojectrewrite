﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ShopController : BaseUIManager
{

    public enum WindowType { Sell, Buy}
    public static ShopController Controller;
    public GameObject ShopPanel;
    public bool IsPanelActive = false;
    public WindowType CurrentType;
    public SellInfo SellInfoPrefab;
    public BuyInfo BuyInfoPrefab;
    public Transform ItemSpacer;
    
    public List<Weapon> ShopWeaponsList = new List<Weapon>();
    public List<DropItem> ShopItemsList = new List<DropItem>();

    [Header("WindowInfo")]
    public GameObject WindowInfo;
    public Image WeaponIcon;
    public Text WeaponName;
    public Text WeaponPrice;
    public Text WeaponLvl;
    public Text WeaponDamageText;
    public Text WeaponSpeedText;
    public Text WeaponReloadText;
    public Text WeaponAmmoText;
    public Text WeaponDescriptionText;

    private List<SellInfo> SellItemsInfo = new List<SellInfo>();
    private List<BuyInfo> BuyItemsInfo = new List<BuyInfo>();

    private Player player;

    public override void Awake()
    {
        base.Awake();
        if (Controller == null)
        {
            Controller = this;
        }
        else if (Controller != this)
        {
            Destroy(gameObject);
        }
        DontDestroyOnLoad(gameObject);
        player = FindObjectOfType<Player>();
    }

    private void Start()
    {
        SetBuyParametres();
        SetSellParametres();
        OpenShopWindow();
        OpenBuyItems();
    }
    
    //установка в магазин инфы о продаже при старте
    private void SetSellParametres()
    {
        for(int i=0; i<InvertoryController.invertoryController.InvertoryWeapons.Count; i++)
        {
            SetWeaponSellInfo(InvertoryController.invertoryController.InvertoryWeapons[i]);
        }
    }
    private void SetItemInfo(Weapon _weapon)
    {
        WindowInfo.SetActive(true);
        WeaponIcon.sprite = _weapon.WeaponIcon;
        WeaponName.text = _weapon.ObjectName;
        WeaponPrice.text = _weapon.ObjectPrice.ToString();
        WeaponLvl.text = _weapon.Level + " LVL";
        WeaponDamageText.text = _weapon.Damage.ToString();
        WeaponSpeedText.text = _weapon.WeaponCooldown + "/MIN";
        WeaponReloadText.text = _weapon.HolderReloadTimer + " SEC";
        WeaponAmmoText.text = _weapon.StartingBullets -_weapon.StartingHolderAmount + "/" + _weapon.MaxBullets;
        WeaponDescriptionText.text = _weapon.Description;
    }
    public void SetSellItemInformation(int _weaponUnicID)
    {
        Weapon _weapon = InvertoryController.invertoryController.GetWeaponByUnicID(_weaponUnicID);
        SetItemInfo(_weapon);
    }

    public void SetBuyItemInformation(int _weaponUnicID)
    {
        Weapon _weapon = GetWeapon(_weaponUnicID);
        SetItemInfo(_weapon);
    }

    public void ResetItemInfo()
    {
        WindowInfo.SetActive(false);
    }

    //удаление инфы о продаже
    public void RemoveSellItemInfo(int _itemID)
    {
        if (GetSellItemInfo(_itemID) != null)
        {
            GetSellItemInfo(_itemID).DeleteItem();
        }
    }
    
    //удаление инфы из массива при полной продаже объектов
    public void RemoveSellItemsInfoList(int _itemID)
    {
        if (GetSellItemInfo(_itemID) != null)
        {
            SellItemsInfo.Remove(GetSellItemInfo(_itemID));
        }
    }

    //удаление инфы о продаже оружия
    public void RemoveSellWeaponInfoList(int _weaponUnicID)
    {
        for(int i=0; i<SellItemsInfo.Count; i++)
        {
            if(SellItemsInfo[i].WeaponUnicID == _weaponUnicID)
            {
                SellItemsInfo.Remove(SellItemsInfo[i]);
            }
        }
    }

    public void OpenSellItems()
    {
        CurrentType = WindowType.Sell;
        OpenItems();
    }

    public void OpenBuyItems()
    {
        CurrentType = WindowType.Buy;
        OpenItems();
    }

    public void OpenItems()
    {
        if(CurrentType == WindowType.Buy)
        {
            for(int i=0; i<BuyItemsInfo.Count; i++)
            {
                BuyItemsInfo[i].gameObject.SetActive(true);
            }
            for (int i = 0; i < SellItemsInfo.Count; i++)
            {
                SellItemsInfo[i].gameObject.SetActive(false);
            }
        }
        else if(CurrentType == WindowType.Sell)
        {
            for (int i = 0; i < BuyItemsInfo.Count; i++)
            {
                BuyItemsInfo[i].gameObject.SetActive(false);
            }
            for (int i = 0; i < SellItemsInfo.Count; i++)
            {
                SellItemsInfo[i].gameObject.SetActive(true);
            }
        }
    }

    //установка инфы о продаже гранаты
    public void SetGrenadeSellInfo(Grenade _grenade, int _count)
    {
        SellInfo tempSell = Instantiate(SellInfoPrefab);
        tempSell.SetItemInfo(_grenade.ItemID, _grenade.Icon, "Гранаты", _grenade.Type, _grenade.Price);
        tempSell.Count = _count;
        tempSell.UpdateCountText();
        tempSell.transform.SetParent(ItemSpacer, false);
        SellItemsInfo.Add(tempSell);
    }

    //установка в магазин инфы о продаже
    public void SetItemSellInfo(DropItem _item)
    {
        //если такие итемы уже есть, то обновляется кол-во
        if (SellItemsInfo.Count > 0 && GetSellItemInfo(_item.ID) != null && _item.Stackable)
        {
            GetSellItemInfo(_item.ID).Count= InvertoryController.invertoryController.GetItem(_item.ObjectID).ItemCount;
            
            GetSellItemInfo(_item.ID).UpdateCountText();
        }
        //иначе добавляется новый
        else AddItemSellInfo(_item);
    }

 
    private void AddItemSellInfo(DropItem _item)
    {
        SellInfo tempSell = Instantiate(SellInfoPrefab);
        tempSell.SetItemInfo(_item.ObjectID, _item.Icon, _item.ObjectName, _item.Type, _item.Price);
        if (_item.Stackable)
        {
            tempSell.ItemID = _item.ID;
        }
        else
        {
            tempSell.ItemID = _item.ObjectID;
        }
        tempSell.Count = InvertoryController.invertoryController.GetItem(_item.ObjectID).ItemCount;
        tempSell.UpdateCountText();
        tempSell.transform.SetParent(ItemSpacer, false);
        SellItemsInfo.Add(tempSell);
        if(CurrentType == WindowType.Buy)
        {
            tempSell.gameObject.SetActive(false);
        }
    }

    public void SetWeaponSellInfo(Weapon _weapon)
    {
        SellInfo tempSell = Instantiate(SellInfoPrefab);
        tempSell.WeaponUnicID = _weapon.ObjectID;
        tempSell.SetItemInfo(_weapon.WeaponID, _weapon.WeaponIcon, _weapon.ObjectName, _weapon.IType, _weapon.ObjectPrice);
        tempSell.Count++;
        tempSell.transform.SetParent(ItemSpacer, false);
        if (CurrentType == WindowType.Buy)
        {
            tempSell.gameObject.SetActive(false);
        }
        SellItemsInfo.Add(tempSell);
    }

    public SellInfo GetSellItemInfo(int _itemID)
    {
        for(int i=0; i<SellItemsInfo.Count; i++)
        {
            if(SellItemsInfo[i].ItemID == _itemID)
            {
                return SellItemsInfo[i];
            }
        }
        return null;
    }

    private void SetBuyParametres()
    {
        for(int i=0; i<ShopWeaponsList.Count; i++)
        {
            SetWeaponBuyInfo(ShopWeaponsList[i]);
        }
        for(int i=0; i<ShopItemsList.Count; i++)
        {
            SetItemBuyInfo(ShopItemsList[i]);
        }
        SetGrenadeBuyInfo(InvertoryController.invertoryController.GrenadePrefab);
    }

    public void SetWeaponBuyInfo(Weapon _weapon)
    {
        BuyInfo tempBuy = Instantiate(BuyInfoPrefab);
        tempBuy.SetItemInfo(_weapon.WeaponID, _weapon.WeaponIcon, _weapon.ObjectName, _weapon.ObjectPrice, _weapon.IType);
        tempBuy.transform.SetParent(ItemSpacer, false);
        BuyItemsInfo.Add(tempBuy);
    }

    private void SetGrenadeBuyInfo(Grenade _grenade)
    {
        BuyInfo tempBuy = Instantiate(BuyInfoPrefab);
        tempBuy.SetItemInfo(_grenade.ItemID, _grenade.Icon, "Гранаты", _grenade.Price, _grenade.Type);
        tempBuy.transform.SetParent(ItemSpacer, false);
        BuyItemsInfo.Add(tempBuy);
    }

    public void SetItemBuyInfo(DropItem _item)
    {
        BuyInfo tempBuy = Instantiate(BuyInfoPrefab);
        tempBuy.SetItemInfo(_item.ID, _item.Icon, _item.ObjectName, _item.Price, _item.Type);
        tempBuy.transform.SetParent(ItemSpacer, false);
        BuyItemsInfo.Add(tempBuy);
    }

    public override void ClosePanel()
    {
        IsPanelActive = false;
        OpenShopWindow();
    }

    public void OpenShopWindow()
    {
        if (IsPanelActive)
        {
            CanavasController.canvasController.SetCanvasSortOrder(CanvasID);
        }
        else
        {
            CanavasController.canvasController.SortOrder--;
            CanavasController.canvasController.CheckSortOrder();
        }

        ShopPanel.SetActive(IsPanelActive);
    }

    //запрос на проверку наличия необходимой суммы для покупки
    public bool PlayerMoneyRequest(int _money)
    {
        if (player.Money >= _money)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    public Weapon GetWeapon(int _ID)
    {
        for(int i=0; i<ShopWeaponsList.Count; i++)
        {
            if(ShopWeaponsList[i].WeaponID == _ID)
            {
                return ShopWeaponsList[i];
            }
        }
        return null;
    }

    //потратить деньги
    public void SpendMoney(int _money)
    {
        player.Money -= _money;
        UIManager.UIManagerInstance.SetCoins(player.Money);
    }

    //получить деньги
    public void EarnMoney(int _money)
    {
        player.Money += _money;
        UIManager.UIManagerInstance.SetCoins(player.Money);
    }

    public void RemoveBuyItem(BuyInfo _info)
    {
        BuyItemsInfo.Remove(_info);
    }


}
