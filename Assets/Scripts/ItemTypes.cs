﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemTypes : MonoBehaviour {

	public enum ItemType {Weapon, HardWeapon, Armor, Medicine, Ammo, Key, QuestItem, Grenade, Other };
}
