﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Explosion : MonoBehaviour
{
    public ParticleSystem Fire;
    public ParticleSystem Debris;
    public ParticleSystem Smoke;
    public Light ExplLight;
    private bool isExploding = false;
    public float LightFadeSpeed;

    public void Explode()
    {
        Fire.Play();
        ExplLight.gameObject.SetActive(true);
        ExplLight.intensity = 5f;
        if(transform.position.y < 1f)
        {
            transform.Translate(Vector3.up*1.5f);
        }
        isExploding = true;
    }

    private void Update()
    {
        if(isExploding)
        {
            ExplLight.intensity -= Time.deltaTime * LightFadeSpeed;

            if(ExplLight.intensity <= 0f)
            {
                isExploding = false;
                ExplLight.gameObject.SetActive(false);
            }
        }

    }
}
