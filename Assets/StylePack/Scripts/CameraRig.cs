﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraRig : MonoBehaviour
{
    public Transform FollowTarget;
    public Transform target;
    public LayerMask Mask;

    private Camera mainCamera;
    private Vector3 mousePos;
    private Vector3 delta;

    void Start ()
    {
        mainCamera = Camera.main;
        target = transform.GetChild(0);
    }
	
	void Update ()
    {
        Ray ray = mainCamera.ScreenPointToRay(Input.mousePosition);
        RaycastHit hit;


        if (Physics.Raycast(ray, out hit, 100f, Mask))
        {
            mousePos = new Vector3(hit.point.x, transform.position.y, hit.point.z);
            delta = Vector3.ClampMagnitude(mousePos - transform.position, 1f);
        }
        else
        {
            delta = Vector3.zero;
        }
        
        transform.position = FollowTarget.position + delta;
    }
}
