﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TimeDisable : MonoBehaviour
{
    public float DisableTime;

    private float timer;
    
    public void Init()
    {
        timer = DisableTime;
    }

    private void Update()
    {
        if(timer > 0f)
        {
            timer -= Time.deltaTime;
        }
        else
        {
            if(gameObject.activeSelf)
            {
                gameObject.SetActive(false);
            }
        }   
    }
}
