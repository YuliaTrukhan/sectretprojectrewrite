﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FXManager : MonoBehaviour
{
    public static FXManager Instance;

    public GameObject HitParticle;
    public GameObject BloodParticle;
    public GameObject Explosion;

    private List<GameObject> hitParticles;
    private List<GameObject> bloodParticles;
    private List<GameObject> explosions;

    private void Start()
    {
        Instance = this;
        hitParticles = new List<GameObject>();
        bloodParticles = new List<GameObject>();
        explosions = new List<GameObject>();
    }

    public GameObject GetHitParticle()
    {
        for (int i = 0; i < hitParticles.Count; i++)
        {
            if (!hitParticles[i].activeSelf)
            {
                hitParticles[i].SetActive(true);
                return hitParticles[i];
            }
        }
        GameObject hp = Instantiate(HitParticle, Vector3.zero, Quaternion.identity);
        hitParticles.Add(hp);
        return hp;
    }

    public GameObject GetBloodParticle()
    {
        for (int i = 0; i < bloodParticles.Count; i++)
        {
            if (!bloodParticles[i].activeSelf)
            {
                bloodParticles[i].SetActive(true);
                return bloodParticles[i];
            }
        }
        GameObject bp = Instantiate(BloodParticle, Vector3.zero, Quaternion.identity);
        bloodParticles.Add(bp);
        return bp;
    }

    public GameObject GetExplosion()
    {
        for (int i = 0; i < explosions.Count; i++)
        {
            if (!explosions[i].activeSelf)
            {
                explosions[i].SetActive(true);
                return explosions[i];
            }
        }
        GameObject exp = Instantiate(Explosion, Vector3.zero, Quaternion.identity);
        explosions.Add(exp);
        return exp;
    }
}