﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour
{
    private Rigidbody rigidBody;
    private TrailRenderer trail;

    private void Start()
    {
        rigidBody = GetComponent<Rigidbody>();
        trail = GetComponent<TrailRenderer>();
    }

    /*void OnCollisionEnter(Collision collision)
    {
        gameObject.SetActive(false);

        GameObject hp = FXManager.Instance.GetHitParticle();
        hp.transform.position = collision.contacts[0].point;
        hp.transform.LookAt(collision.contacts[0].normal); 
        hp.GetComponent<TimeDisable>().Init();
        hp.GetComponent<ParticleSystem>().Play();

    }*/

    public void Init()
    {
        if(rigidBody == null)
        {
            rigidBody = GetComponent<Rigidbody>();
            trail = GetComponent<TrailRenderer>();
        }
        trail.Clear();
    }

    public void MakeHitFX(Collision collision)
    {
        GameObject hp = FXManager.Instance.GetHitParticle();
        hp.transform.position = collision.contacts[0].point;
        hp.transform.LookAt(collision.contacts[0].normal);
        hp.GetComponent<TimeDisable>().Init();
        hp.GetComponent<ParticleSystem>().Play();
    }

    public void MakeBloodFX(Collision collision)
    {
        GameObject bp = FXManager.Instance.GetBloodParticle();
        bp.transform.position = collision.contacts[0].point;
        bp.transform.LookAt(collision.contacts[0].normal);
        bp.GetComponent<TimeDisable>().Init();
        bp.GetComponent<ParticleSystem>().Play();
    }
}
