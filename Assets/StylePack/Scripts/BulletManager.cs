﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletManager : MonoBehaviour
{
    public GameObject Bullet;

    private List<GameObject> bullets;

    private void Start()
    {
        bullets = new List<GameObject>();
    }

    public GameObject GetBullet()
    {
        for(int i = 0; i < bullets.Count; i++)
        {
            if(!bullets[i].activeSelf)
            {
                bullets[i].SetActive(true);
                return bullets[i];
            }
        }
        GameObject bullet = Instantiate(Bullet, Vector3.zero, Quaternion.identity);
        bullets.Add(bullet);
        return bullet;
    }
}
