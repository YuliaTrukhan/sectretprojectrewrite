﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ThirdPersonController : MonoBehaviour
{
    public float RunSpeed;
    public float BackOffSpeed;
    public float SideSpeed;
    public float RotationSpeed;
    public Transform bulletPosition;
    public float ShootInterval;

    private Animator animationController;
    private Rigidbody rigidBody;
    private float vSpeed;
    private float hSpeed;
    private float aSpeed;
    private Camera mainCamera;
    private bool isShooting;
    private Vector3 lookPos;
    private bool isHeavyGun;
    private BulletManager bulletManager;

    public LookDirection lookDirection;
    private float shootTimer;

    public Explosion explosion;

    public enum LookDirection
    {
        RIGHT,
        LEFT,
        TOP,
        DOWN
    }

	void Start ()
    {
        animationController = GetComponent<Animator>();
        rigidBody = GetComponent<Rigidbody>();
        bulletManager = GetComponent<BulletManager>();
        mainCamera = Camera.main;
        isShooting = false;
        isHeavyGun = false;
        shootTimer = ShootInterval;

        SwitchGun();
    }
	
	void Update ()
    {
        ReadInput();
        ReadOrientation();
        UpdateAnimationController();
        ProcessShooting();
	}

    private void FixedUpdate()
    {
        UpdatePosition();
    }

    private void ReadInput()
    {
        ReadKeys();
        ReadMouse();
    }

    private void ReadKeys()
    {   
        vSpeed = Input.GetAxis("Vertical");
        hSpeed = Input.GetAxis("Horizontal");

        if(Input.GetKeyDown(KeyCode.G))
        {
            ThrowGrenade();
        }

        if (Input.GetKeyDown(KeyCode.Q))
        {
            SwitchGun();
        }
    }

    private void ReadMouse()
    {
        Ray ray = mainCamera.ScreenPointToRay(Input.mousePosition);
        RaycastHit hit;

        if (Physics.Raycast(ray, out hit, 100f))
        {
            lookPos = new Vector3(hit.point.x, 0f, hit.point.z);
        }
        
        if(Input.GetMouseButtonDown(0))
        {
            isShooting = true;
        }
        if (Input.GetMouseButtonUp(0))
        {
            isShooting = false;
        }
    }

    private void ReadOrientation()
    {
        float angle = transform.rotation.eulerAngles.y;

        if (angle >= 0f && angle <= 45f)
        {
            lookDirection = LookDirection.TOP;
        }
        else if (angle > 45f && angle <= 135f)
        {
            lookDirection = LookDirection.RIGHT;
        }
        else if (angle > 135f && angle <= 225f)
        {
            lookDirection = LookDirection.DOWN;
        }
        else if (angle > 225f && angle <= 315f)
        {
            lookDirection = LookDirection.LEFT;
        }
        else if (angle > 315f && angle <= 360f)
        {
            lookDirection = LookDirection.TOP;
        }
    }

    private void UpdatePosition()
    {
        float _hSpeed = RunSpeed;
        float _vSpeed = RunSpeed;
        switch(lookDirection)
        {
            case LookDirection.TOP:
                _hSpeed = SideSpeed;
                _vSpeed = vSpeed >= 0f ? RunSpeed : BackOffSpeed;
                break;
            case LookDirection.DOWN:
                _hSpeed = SideSpeed;
                _vSpeed = vSpeed < 0f ? RunSpeed : BackOffSpeed;
                break;
            case LookDirection.RIGHT:
                _hSpeed = hSpeed >= 0f ? RunSpeed : BackOffSpeed;
                _vSpeed = SideSpeed;
                break;
            case LookDirection.LEFT:
                _hSpeed = hSpeed < 0f ? RunSpeed : BackOffSpeed;
                _vSpeed = SideSpeed;
                break;
        }

        rigidBody.velocity = Vector3.forward * vSpeed * _vSpeed + Vector3.right * hSpeed * _hSpeed;

        aSpeed = GetRotationAngle();
        rigidBody.angularVelocity = new Vector3(0f, aSpeed * RotationSpeed, 0f);
    }

    private void UpdateAnimationController()
    {
        switch (lookDirection)
        {
            case LookDirection.TOP:
                animationController.SetFloat("fSpeed", vSpeed);
                animationController.SetFloat("sSpeed", hSpeed);
                break;
            case LookDirection.DOWN:
                animationController.SetFloat("fSpeed", -vSpeed);
                animationController.SetFloat("sSpeed", -hSpeed);
                break;
            case LookDirection.RIGHT:
                animationController.SetFloat("fSpeed", hSpeed);
                animationController.SetFloat("sSpeed", vSpeed);
                break;
            case LookDirection.LEFT:
                animationController.SetFloat("fSpeed", -hSpeed);
                animationController.SetFloat("sSpeed", -vSpeed);
                break;
        }

        animationController.SetFloat("aSpeed", aSpeed);
        animationController.SetBool("Shoot", isShooting);
    }

    private float GetRotationAngle()
    {
        float dx = transform.position.x - lookPos.x;
        float dy = transform.position.z - lookPos.z;

        float angle = Mathf.Atan2(dy, dx) * Mathf.Rad2Deg + 90f;

        angle = (angle + transform.rotation.eulerAngles.y) % 360f;
        if(angle > 180f)
        {
            angle -= 360f;
        }

        return -angle/180f;
    }

    private void ThrowGrenade()
    {
        animationController.SetTrigger("GrenadeThrow");
        explosion.Explode();
    }

    private void SwitchGun()
    {
        isHeavyGun = !isHeavyGun;
        animationController.SetBool("isHeavyGun", isHeavyGun);
    }

    private void ProcessShooting()
    {
        if(isShooting)
        {
            shootTimer -= Time.deltaTime;

            if(shootTimer <= 0f)
            {
                Shoot();
            }

        }
    }

    private void Shoot()
    {
        GameObject go = bulletManager.GetBullet();
        go.transform.position = bulletPosition.position;
        go.transform.rotation = transform.rotation;
        go.GetComponent<Bullet>().Init();
        shootTimer = ShootInterval;
    }
}
